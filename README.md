# CPP YACL Lib

Yet another C++ library for handling command line arguments.

This library is intended for console applications that implement a command approach (popularized by Git and others).
The library provides builders to assemble commands and their parameters.

## An abstraction for applications

In C++ I prefer to jump from the C-like main function and its `char*[]` into the C++ world as soon as possible.
Hence, this framwork comes with an abstraction for applications.
It works with a `vector<string>` for the command line arguments instead of `char*[]`.

Furthermore I like to keep the code in `main` as short as possible.
`main` is a program and cannot be linked into the tests, which are also `main`s.
I try to achieve a high line coverage.

### Configuring the application

You can use the `Application` class as is.
But, my advise is you derive from it like so (there is a corresponding component test to demonstrate this):

```cpp
class My_Application final : public Application
{
    public:
        explicit My_Application(const string &program_name) : Application{command_line_parser(program_name)}
        {
        }

    private:
        auto command_line_parser(const string &program_name) const -> Command_Line_Parser
        {
                Command_Line_Parser_Builder command_syntax{program_name};
                // 1st command
                cmd_syntax
                    .command("start")
                    .description("Starts the server.")
                    .mandatory()
						.int_param("port")
						.description("Port to listen on.")
						.min(0)
                        .max(65535)
                    .action([&](const auto &context, auto &in, auto &out, auto &err) {
                            // Start the server...
                    });
                // 2nd command
                cmd_syntax
                    .command("stop")
                    .description("Stops the server.")
                    .optional()
						.flag_param("wait")
						.description("Defer shut-down until all clients disconnected.")
						.defaults_to(false)
                    .action([&](const auto &context, auto &in, auto &out, auto &err) {
                            // Stop the server...
                    });
                // Describing the application ends the building process.
                // The result is a valid, functional command line parser.
                return command_syntax.description("My backend server.");
        }
};
```

It is a good idea to use methods instead of lambdas.
I did not do this here to keep the example short.

`My_Application` is used in `main` like shown in the most simple example below:

```cpp
auto main(int arg_count, const char *arg_list[]) -> int
{
        // Extract program name, it's the first argument
        My_Application application{yacl::program_name(arg_count, arg_list)};
        // Extract actual arguments, these are all arguments
        application.run(yacl::list_of_arguments(arg_count, arg_list), std::cin, std::cout, std::cerr);
        return 0;
}
```

## Configuring commands

The shortest command configuration looks like this:

```cpp
Command_Line_Parser_Builder builder{"exe_name"};
builder.command("command_name")
       .description("description")
       .action(some_action);
```

The example above defines a command `command_name`.
This command has no parameters.
When invoked, it will call the function or method `some_action`.

On the command line you would invoke it like so, assuming the executable is named `exe_name`:

```bash
exe_name command_name
```

## Configuring command parameters

There are three types of parameters: Integer, flag (boolean) and text (string).

Furthermore parameters can be either optional or mandatory.

### Integer parameters

| Parameters    | Mandatory     | Default | Description |
|---------------|---------------|---------|-------------|
| `description` | yes           | n/a     | Describes the parameter. What does it do? |
| `min`         | no            | `numeric_limits<int>().min()` | Arguments must not be less than this value. |
| `max`         | no            | `numeric_limits<int>().max()` | Arguments must not be greated than this value. |
| `defaults_to` | when optional | n/a     | This value will be assumed if argument not given on command line. |

**Code example**

```cpp
Command_Line_Parser_Builder builder{"exe_name"};
builder.command("command_name")
       .description("description")
           .mandatory().int_parameter("port")
               .description("Port server will listen on.")
               .min(0)
               .max(65535)
       .action(some_action);
```

**Command line example**

```bash
exe_name command_name --port 8080
```

### Flag parameters

| Parameters    | Mandatory     | Default | Description |
|---------------|---------------|---------|-------------|
| `description` | yes           | n/a     | Describes the parameter. What does it do? |
| `defaults_to` | when optional | n/a     | This value will be assumed if argument not given on command line. |

By the way, these are valid values for flag parameters:

| Value given on command line | Boolean value |
|-----------------------------|---------------|
| `true`                      | `true`        |
| `yes`                       | `true`        |
| `1`                         | `true`        |
| `false`                     | `false`       |
| `no`                        | `false`       |
| `0`                         | `false`       |

**Code example**

```cpp
Command_Line_Parser_Builder builder{"exe_name"};
builder.command("command_name")
       .description("description")
           .optional().flag_parameter("enable_logging")
               .description("Server will write detailed log.")
               .defaults_to(false)
       .action(some_action);
```

**Command line example**

```bash
exe_name command_name --enable_logging yes
```

### Text parameters

| Parameters          | Mandatory     | Default | Description |
|---------------------|---------------|---------|-------------|
| `description`       | yes           | n/a     | Describes the parameter. What does it do? |
| `allow_empty_text`  | no            | `false` | Argument values may be empty, if `true`. |
| `allow_white_space` | no            | `false` | Argument values may be contain whitespace, if `true`. |
| `one_of`            | no            | n/a     | Arguments must be in given set of choices. |
| `defaults_to`       | when optional | n/a     | This value will be assumed if argument not given on command line. |

**Code example**

```cpp
Command_Line_Parser_Builder builder{"exe_name"};
builder.command("command_name")
       .description("description")
           .mandatory().text_parameter("log")
               .description("Server will log in this level and above.")
               .allow_empty_text(false)  // Default, just for demo
               .allow_white_space(false) // Default, just for demo
               .one_of("fatal")
               .one_of("error")
               .one_of("warn")
               .one_of("info")
               .one_of("debug")
               .one_of("trace")
               .one_of("off")
       .action(some_action);
```

**Command line example**

```bash
exe_name command_name --log info
```

## Project structure

| File             | Content |
|------------------|---------|
| `.clang-format`  | Configuration for the Clang formatting tool used to format the source code. |
| `.gitlab-ci.yml` | Build automation for https://gitlab.com. |
| `CMakeLists.txt` | Top level, hand crafted CMake file. Generated CMake files will be generated in the `build` folder. |
| `conanfile.txt`  | Libraries, this library depends upon. This is just the Catch2 testing framework. No other dependencies in this case. |
| `Makefile`       | Hand crafted make file used for convenient local/manual builds as well as automated CI builds. |
| `artwork/`       | Affinity Designer file for the project logo. |
| `build/`         | Created by build script, not checked in. Contains temporary, build-related files. |
| `include/`       | Contains all headers of the C++ Junk Box library. |
| `model/`         | Models to generate code from. |
| `packaging/`     | Conan receipt for packaging this library. Further files will created automatically when packaging. These are not checked in. |
| `src/`           | Contains all sources of the C++ Junk Box library. |
| `test/`          | Unit and component tests (based on Catch2). |

## Building/Testing

### Locally/Command Line

The acompanying `Makefile` contains targets for (almost) anything.
Once you have checked out this prject you can build it like so:

- `make dependencies` will download all the dependencies and have Conan produce a CMake configuration (preset) to work with.
- `make build` will then compile the library.
- `make test` runs all the tests
- You can also run `make clean dependencies build test` in one piece.

### IDE (Visual Studio Code)

Just execute `make code`.
This will picks the CMake preset (generated by `make dependencies`) and starts VS Code.
VS Code will be configured to allow for

- Building source and tests
- Code completion
- Debuggung
- Executing tests

### CI

See `.gitlab-ci.yml` for details.

## Using This Lib In Your Projects

GitLab`s Conan package registry currently does not support Conan 2.0.
Therefore you cannot reference the package from the registry.
Instead you need to build the library and push it to your local Conan registry.

So, this is how you could add this library to your project (assuming you are using Conan as a dependency manager):

-  First download, build and push that library to your local Conan registry
   ```shell
   git clone https://gitlab.com/hzahnlei/cpp-yacl.git
   cd cpp-yacl
   # Always suggest to explicitely pick a version
   git checkout 1.7.1
   make package
   ```
-  Add a dependency like so to your `conanfile.txt`
   ```shell
   [requires]
   yacl/1.7.1
   ```
-  Now build your own application or library.
   The dependency is now satisfied from your local Conan registry.

The process, described above, can be used for local development environments as well as for automated CI builds.

## Acknowledgments

This library is built on top of and by use of many opensource or free of charge libraries, tools and services.
Thank you very much:

*  [Visual Studio Code](https://code.visualstudio.com)
*  [C++ Extension for VSC](https://github.com/Microsoft/vscode-cpptools.git)
*  [CMake Extension for VSC](https://github.com/microsoft/vscode-cmake-tools)
*  [Test Extension for VSC](https://github.com/matepek/vscode-catch2-test-adapter.git)
*  [CMake](https://cmake.org)
*  [GNU Make](https://www.gnu.org/software/make/)
*  [Conan](https://conan.io)
*  [GCC](https://gcc.gnu.org)
*  [Clang](https://clang.llvm.org)
*  [GitLab](https://gitlab.com)
*  [Bintray](https://bintray.com/hzahnlei)
*  [Catch2](https://github.com/catchorg/Catch2)
*  [cppcheck](https://sourceforge.net/projects/cppcheck/)

## Disclaimer

This is a private, free, open source and non-profit project (see LINCENSE file).
Use on your own risk.
I am not liable for any damage caused.
I am a private person, not associated with any companies, or organizations I may have mentioned herein.
