/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/base_application.hpp"
#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include "yacl/text_cmdln_arg.hpp"
#include <catch2/catch_all.hpp>
#include <chrono>
#include <sstream>
#include <thread>

using Catch::Matchers::Message;
using std::istringstream;
using std::move;
using std::ostringstream;
using std::thread;
using std::to_string;
using std::this_thread::sleep_for;
using std::this_thread::yield;

using namespace std::chrono_literals;
using namespace yacl;

constexpr auto BANNER = "******************************\n"
			"My Cool Program\n"
			"******************************\n";

auto cmdln_parser_for_fictitious_app(const string &prog_name, const Command_Action &action)
{
	Command_Line_Parser_Builder command_syntax{prog_name, BANNER};
	// clang-format off
        command_syntax
            .version("1.0.0")
            .copyright("(c) 2020 by D. Duck, all rights reserved")
            .author("Donald Duck")
            .license(License::MIT)
            .open_source_software_used("Catch2")
            .open_source_software_used("fmt")
                .command("someCommand")
                    .description("Command that does cool things.")
                    .mandatory().flag_param("someMandatoryFlagArg")
                        .description("Some important flag argument!")
                    .mandatory().int_param("someMandatoryIntArg")
                        .description("Some important int argument!")
                    .mandatory().text_param("someMandatoryTextArg")
		    	.allow_white_space()
                        .description("Some important text argument!")
                    .optional().flag_param("someOptionalFlagParam")
                        .description("Some less important flag argument!")
                        .defaults_to(true)
                    .optional().int_param("someOptionalIntParam")
                        .description("Some less important int argument!")
                        .defaults_to(4711)
                    .optional().text_param("someOptionalTextParam")
                        .description("Some less important text argument!")
                        .defaults_to("beer")
                    .action(action);
	// clang-format on
	return command_syntax.description("App that does cool things.");
}

TEST_CASE("Application throws if no command given.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe"};
	const auto exe_name_from_cmdln_args =
			program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	Base_Application app{cmdln_parser_for_fictitious_app(exe_name_from_cmdln_args, test::mock::command_action)};
	const auto empty_cmdln_args =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;

	REQUIRE_THROWS_MATCHES(app.run(empty_cmdln_args, in, out, err), runtime_error,
	                       Message("No arguments given. "
	                               "Possible commands are: 'help', 'someCommand', 'version'."));
	REQUIRE(out.str() == "");
	REQUIRE(err.str() == "");
}

TEST_CASE("Application throws if unknown command given.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe", "bla"};
	const auto exe_name_from_cmdln_args =
			program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	Base_Application app{cmdln_parser_for_fictitious_app(exe_name_from_cmdln_args, test::mock::command_action)};
	const auto empty_cmdln_args =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;

	REQUIRE_THROWS_MATCHES(app.run(empty_cmdln_args, in, out, err), runtime_error,
	                       Message("Unknown command 'bla'. "
	                               "Possible commands are: 'help', 'someCommand', 'version'."));
	REQUIRE(out.str() == "");
	REQUIRE(err.str() == "");
}

TEST_CASE("Application not running after creation.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe", "command"};
	const auto exe_name_from_cmdln_args =
			program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	Base_Application app{cmdln_parser_for_fictitious_app(exe_name_from_cmdln_args, test::mock::command_action)};

	REQUIRE_FALSE(app.is_running());
}

TEST_CASE("Application prints help if invoked with built-in help command.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe", "help"};
	const auto exe_name_from_cmdln_args =
			program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	Base_Application app{cmdln_parser_for_fictitious_app(exe_name_from_cmdln_args, test::mock::command_action)};
	const auto cmdln_args_calling_for_help =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;

	app.run(cmdln_args_calling_for_help, in, out, err);
	REQUIRE(out.str() ==
	        "******************************\n"
	        "My Cool Program\n"
	        "******************************\n"
	        "\n"
	        "\n"
	        "App that does cool things.\n"
	        "\n"
	        "AVAILABLE COMMANDS\n"
	        "\n"
	        "\tHELP - This short help message.\n"
	        "\n"
	        "\t\texe help\n"
	        "\n"
	        "\tSOMECOMMAND - Command that does cool things.\n"
	        "\n"
	        "\t\texe someCommand --someMandatoryFlagArg <someMandatoryFlagArg> --someMandatoryIntArg "
	        "<someMandatoryIntArg> --someMandatoryTextArg <someMandatoryTextArg> [--someOptionalFlagParam "
	        "<someOptionalFlagParam>] [--someOptionalIntParam <someOptionalIntParam>] [--someOptionalTextParam "
	        "<someOptionalTextParam>]\n"
	        "\n"
	        "\t\t--someMandatoryFlagArg <someMandatoryFlagArg>: Some important flag argument!\n"
	        "\t\t                                               Mandatory, named parameter.\n"
	        "\t\t                                               Where <someMandatoryFlagArg> has to be one of '0', "
	        "'1', 'false', 'no', 'true', 'yes'.\n"
	        "\n"
	        "\t\t--someMandatoryIntArg <someMandatoryIntArg>: Some important int argument!\n"
	        "\t\t                                             Mandatory, named parameter.\n"
	        "\t\t                                             Where <someMandatoryIntArg> has to be in "
	        "[-2147483648, 2147483647].\n"
	        "\n"
	        "\t\t--someMandatoryTextArg <someMandatoryTextArg>: Some important text argument!\n"
	        "\t\t                                               Mandatory, named parameter.\n"
	        "\t\t                                               Where <someMandatoryTextArg> is a none-empty "
	        "string (may contain whitespace).\n"
	        "\n"
	        "\t\t--someOptionalFlagParam <someOptionalFlagParam>: Some less important flag argument!\n"
	        "\t\t                                                 Optional, named parameter.\n"
	        "\t\t                                                 Defaults to 1.\n"
	        "\t\t                                                 Where <someOptionalFlagParam> has to be one of "
	        "'0', '1', 'false', 'no', 'true', 'yes'.\n"
	        "\n"
	        "\t\t--someOptionalIntParam <someOptionalIntParam>: Some less important int argument!\n"
	        "\t\t                                               Optional, named parameter.\n"
	        "\t\t                                               Defaults to 4711.\n"
	        "\t\t                                               Where <someOptionalIntParam> has to be in "
	        "[-2147483648, 2147483647].\n"
	        "\n"
	        "\t\t--someOptionalTextParam <someOptionalTextParam>: Some less important text argument!\n"
	        "\t\t                                                 Optional, named parameter.\n"
	        "\t\t                                                 Defaults to 'beer'.\n"
	        "\t\t                                                 Where <someOptionalTextParam> is a none-empty "
	        "string (must not contain whitespace).\n"
	        "\n"
	        "\tVERSION - Version information.\n"
	        "\n"
	        "\t\texe version\n"
	        "\n"
	        "COPYRIGHT STATEMENT\n"
	        "\n"
	        "\t(c) 2020 by D. Duck, all rights reserved\n"
	        "\n"
	        "AUTHORS\n"
	        "\n"
	        "\tThis software has been written by Donald Duck.\n"
	        "\n"
	        "LICENSE\n"
	        "\n"
	        "\tThis software is distributed under the MIT license.\n"
	        "\n"
	        "\tPermission is hereby granted, free of charge, to any person obtaining a copy of this software and "
	        "associated documentation files (the \"Software\"), to deal in the Software without restriction, "
	        "including "
	        "without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or "
	        "sell "
	        "copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to "
	        "the "
	        "following conditions: The above copyright notice and this permission notice shall be included in all "
	        "copies or substantial portions of the Software.\n"
	        "\t\n"
	        "\tTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT "
	        "NOT "
	        "LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. "
	        "IN NO "
	        "EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, "
	        "WHETHER "
	        "IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE "
	        "OR "
	        "THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n"
	        "\n"
	        "ACKNOWLEDGEMENTS\n"
	        "\n"
	        "\tThis software is built on top of Catch2, fmt.\n"
	        "\n");
}

TEST_CASE("Application prints version if invoked with built-in version command.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe", "version"};
	const auto exe_name_from_cmdln_args =
			program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	Base_Application app{cmdln_parser_for_fictitious_app(exe_name_from_cmdln_args, test::mock::command_action)};
	const auto cmdln_args_calling_for_help =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;

	app.run(cmdln_args_calling_for_help, in, out, err);
	REQUIRE(out.str() == "exe 1.0.0\n");
}

TEST_CASE("Application executes action if invoked with user-defined command.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe",
	                                            "someCommand",
	                                            "--someMandatoryFlagArg",
	                                            "true",
	                                            "--someMandatoryIntArg",
	                                            "1234",
	                                            "--someMandatoryTextArg",
	                                            "duff"};
	const auto exe_name_from_cmdln_args =
			program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	auto parser = cmdln_parser_for_fictitious_app(
			exe_name_from_cmdln_args,
			[](const auto &context, auto &, auto &out, auto &)
			{
				const auto mandatoryFlagArg = context.flag_value_of("someMandatoryFlagArg");
				out << "mandatoryFlagArg=" << (mandatoryFlagArg ? "true" : "false") << '\n';
				const auto mandatoryIntArg = context.integer_value_of("someMandatoryIntArg");
				out << "mandatoryIntArg=" << to_string(mandatoryIntArg) << '\n';
				const auto mandatoryTextArg = context.text_value_of("someMandatoryTextArg");
				out << "mandatoryTextArg=" << mandatoryTextArg << '\n';

				const auto optionalFlagArg = context.flag_value_of("someOptionalFlagParam");
				out << "optionalFlagArg=" << (optionalFlagArg ? "true" : "false") << '\n';
				const auto optionalIntArg = context.integer_value_of("someOptionalIntParam");
				out << "optionalIntArg=" << to_string(optionalIntArg) << '\n';
				const auto optionalTextArg = context.text_value_of("someOptionalTextParam");
				out << "optionalTextArg=" << optionalTextArg << '\n';
			});
	Base_Application app{move(parser)};
	const auto cmdln_args_calling_for_help =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;

	app.run(cmdln_args_calling_for_help, in, out, err);
	REQUIRE(out.str() == "mandatoryFlagArg=true\n"
	                     "mandatoryIntArg=1234\n"
	                     "mandatoryTextArg=duff\n"
	                     "optionalFlagArg=true\n"
	                     "optionalIntArg=4711\n"
	                     "optionalTextArg=beer\n");
}

class Derived_Application final : public Base_Application
{

    public:
	explicit Derived_Application(const string &program_name) : Base_Application(command_line_parser(program_name))
	{
	}

    private:
	auto command_line_parser(const string &program_name) const -> Command_Line_Parser
	{
		constexpr auto TEST_BANNER{"--------------------------\n"
		                           "My super app!\n"
		                           "--------------------------\n"};
		Command_Line_Parser_Builder command_syntax{program_name, TEST_BANNER};
		// clang-format off
                command_syntax
					.version("1.13.9")
                    .copyright("(c) 2020 by Holger Zahnleiter, all rights reserved")
                    .author("Holger Zahnleiter")
                    .license(License::MIT)
                    .open_source_software_used("Catch2")
                    .open_source_software_used("GCC")
                    .open_source_software_used("Clang")
                    .open_source_software_used("GNU Make");
		// clang-format on
		register_print_command(command_syntax);
		register_sleep_command(command_syntax);
		// Register further commands...
		return command_syntax.description("My super app does super things for you.");
	}

	auto register_print_command(Command_Line_Parser_Builder &cmd_syntax) const -> void
	{
		constexpr auto CMD_PRINT{"print"};
		constexpr auto ARG_TEXT{"text"};
		constexpr auto ARG_REPEAT{"repeat"};
		// clang-format off
                cmd_syntax
                    .command(CMD_PRINT)
                    .description("Prints text n times to console.")
                    .mandatory()
						.text_param(ARG_TEXT)
						.description("Text to be printed.")
						.allow_white_space()
                    .mandatory()
						.int_param(ARG_REPEAT)
						.description("Repeat this many times.")
						.min(0)
                    .action([&](const auto &context, auto &in, auto &out, auto &err) {
                            // Do not have to write all logic here.
                            // Just call a method of Derived Application.
                            run_print(context.text_value_of(ARG_TEXT),
                                      context.integer_value_of(ARG_REPEAT),
                                      in, out, err);
                    });
		// clang-format on
	}

	auto run_print(const string &text_to_print, const size_t repetitions, const istream &, ostream &out,
	               const ostream &) const -> void
	{
		for (auto i = 0U; i < repetitions; i++)
		{
			out << text_to_print << '\n';
		}
	}

	auto register_sleep_command(Command_Line_Parser_Builder &cmd_syntax) const -> void
	{
		constexpr auto CMD_SLEEP{"sleep"};
		// clang-format off
                cmd_syntax
                    .command(CMD_SLEEP)
                    .description("Halts the program.")
                    .action([&](const auto &, auto &in, auto &out, auto &err) {
                            run_sleep(in, out, err);
                    });
		// clang-format on
	}

	auto run_sleep(const istream &, ostream &out, const ostream &) const -> void
	{
		out << "command started\n";
		while (!is_stop_requested())
		{
			yield();
			sleep_for(10ms);
		}
		out << "command stopped\n";
	}
};

/*!
 * Why would I want to derive my own version of Base_Application?
 * In that case you could encapsulated additional state within the application.
 * This additional state (or dependencies) can be used by the command actions.
 */
TEST_CASE("You can derive your own application from Application.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe", "print", "--text", "Hello, World!", "--repeat", "5"};
	Derived_Application app{program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main)};
	const auto cmdln_args_calling_for_help =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;
	app.run(cmdln_args_calling_for_help, in, out, err);
	REQUIRE(out.str() == "Hello, World!\n"
	                     "Hello, World!\n"
	                     "Hello, World!\n"
	                     "Hello, World!\n"
	                     "Hello, World!\n");
}

TEST_CASE("Application can be used to build servers.", "[Base_Application]")
{
	const char *arg_list_as_passed_to_main[] = {"exe", "sleep"};
	Derived_Application app{program_name(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main)};
	const auto cmdln_args_calling_for_help =
			list_of_arguments(NUMBER_OF_ARGS(arg_list_as_passed_to_main), arg_list_as_passed_to_main);
	istringstream in;
	ostringstream out;
	ostringstream err;

	thread app_worker{[&]() { app.run(cmdln_args_calling_for_help, in, out, err); }};
	while (!app.is_running())
	{
		yield();
		sleep_for(10ms);
	};
	REQUIRE(app.is_running());

	app.stop();
	while (app.is_running())
	{
		yield();
		sleep_for(10ms);
	};
	REQUIRE_FALSE(app.is_running());

	REQUIRE(out.str() == "command started\n"
	                     "command stopped\n");
	app_worker.join();
}
