/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include <stdexcept>

namespace yacl::test
{

        using std::cbegin;
        using std::cend;
        using std::for_each;
        using std::istream;
        using std::make_unique;
        using std::ostream;
        using std::runtime_error;
        using std::unique_ptr;

        namespace mock
        {
                auto command_action(const Execution_Context &, istream &, ostream &, ostream &) -> void
                {
                        throw runtime_error{"Don't call us, we call you!"};
                }

                auto registration_function_that_throws(const string &, const Command_Parser &) -> void
                {
                        FAIL("Did not expect to be called.");
                }

                auto registration_function(const string &, const Command_Parser &) -> void
                {
                }
        } // namespace mock

        auto command_count(const Command_Line_Parser &cmd) -> size_t
        {
                auto i = 0U;
                for_each(cbegin(cmd), cend(cmd), [&](const auto &) { i++; });
                return i;
        }

        auto some_cmd_parser_builder() -> unique_ptr<Command_Parser_Builder>
        {
                auto cmd_parser_builder = make_unique<Command_Parser_Builder_Default_Impl>(
                    [&](const auto &, auto &&) { FAIL("Unexpected invokation."); }, "commandName");
                cmd_parser_builder->description("command descr");
                return cmd_parser_builder;
        }

} // namespace yacl::test