/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <catch2/catch_all.hpp>
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/parser/cmdln_parser.hpp"
#include <istream>
#include <ostream>
#include <string>

namespace yacl::test
{

        namespace mock
        {
                auto command_action(const Execution_Context &, istream &, ostream &, ostream &) -> void;

                auto registration_function_that_throws(const string &, const Command_Parser &) -> void;
                auto registration_function(const string &, const Command_Parser &) -> void;
        } // namespace mock

        auto command_count(const Command_Line_Parser &) -> size_t;

        auto some_cmd_parser_builder() -> unique_ptr<Command_Parser_Builder>;

} // namespace yacl::test
