/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/base_application.hpp"
#include <catch2/catch_all.hpp>

using namespace yacl;

//-----------------------------------------------------------------------------
// Extract program name from command line arguments as passed to main()
//-----------------------------------------------------------------------------

TEST_CASE("Program name is extracted from command line arguments.", "[Base_Application]")
{
        const char *args[] = {"exe", "someOption"};
        REQUIRE(program_name(NUMBER_OF_ARGS(args), args) == "exe");
}

TEST_CASE("Program name is extracted from command line arguments, short path is disregarded.", "[Base_Application]")
{
        const char *args[] = {"./exe", "someOption"};
        REQUIRE(program_name(NUMBER_OF_ARGS(args), args) == "exe");
}

TEST_CASE("Program name is extracted from command line arguments, long path is disregarded.", "[Base_Application]")
{
        const char *args[] = {"/usr/local/bin/exe", "someOption"};
        REQUIRE(program_name(NUMBER_OF_ARGS(args), args) == "exe");
}

//-----------------------------------------------------------------------------
// Extract arguments from command line arguments as passed to main()
//-----------------------------------------------------------------------------

TEST_CASE("Argument list for the application is empty if no command line arguments given.", "[Base_Application]")
{
        const char *args[] = {"exe"};
        const auto cmdln_args = list_of_arguments(NUMBER_OF_ARGS(args), args);
        REQUIRE(cmdln_args.empty());
}

TEST_CASE("Argument list for the application contains all arguments from command line except the program name.",
          "[Base_Application]")
{
        const char *args[] = {"exe", "server", "--port", "6502"};
        const auto cmdln_args = list_of_arguments(NUMBER_OF_ARGS(args), args);
        REQUIRE(cmdln_args.size() == 3U);
        REQUIRE(cmdln_args[0] == "server");
        REQUIRE(cmdln_args[1] == "--port");
        REQUIRE(cmdln_args[2] == "6502");
}
