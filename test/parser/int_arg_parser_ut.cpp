/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/int_arg_parser.hpp"
#include <catch2/catch_all.hpp>
#include <optional>

using Catch::Matchers::Message;
using std::nullopt;

using namespace yacl;

TEST_CASE("'-1000' is a valid textual representation for a int value.", "[Integer_Argument_Parser]")
{
	const Integer_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME, -1000, 1000, nullopt};
	REQUIRE(arg.value_from("-1000") == -1000);
}

TEST_CASE("'0' is a valid textual representation for a int value.", "[Integer_Argument_Parser]")
{
	const Integer_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME, -1000, 1000, nullopt};
	REQUIRE(arg.value_from("0") == 0);
}

TEST_CASE("'1000' is a valid textual representation for a int value.", "[Integer_Argument_Parser]")
{
	const Integer_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME, -1000, 1000, nullopt};
	REQUIRE(arg.value_from("1000") == 1000);
}

TEST_CASE("Only numeric textual representations are allowed for int value.", "[Integer_Argument_Parser]")
{
	const Integer_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME, -1000, 1000, nullopt};
	REQUIRE_THROWS_MATCHES(arg.value_from("one"), runtime_error,
	                       Message("Invalid value 'one' for integer parameter 'name'."));
}

TEST_CASE("Numeric textual representations must adhere to the constraints.", "[Integer_Argument_Parser]")
{
	const Integer_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME, -1000, 1000, nullopt};
	REQUIRE_THROWS_MATCHES(arg.value_from("-1001"), runtime_error,
	                       Message("Value for integer parameter 'name' must not be less than -1000 but is -1001."));
}

TEST_CASE("Default values of int parameters must adhere to the constraints.", "[Integer_Argument_Parser]")
{
	REQUIRE_THROWS_MATCHES((Integer_Argument_Parser{"name", "description", param::Identifiability::BY_NAME, -1000,
	                                                1000, optional(2000)}),
	                       runtime_error,
	                       Message("Default value is invalid. Value for integer parameter 'name' must not be "
	                               "greater than 1000 but is 2000."));
}
