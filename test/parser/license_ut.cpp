/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include <catch2/catch_all.hpp>
#include "yacl/parser/license.hpp"

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Can get text for Apache2 license enum.", "[License]")
{
        REQUIRE(license_text(License::APACHE2) == "Licensed under the Apache License, Version 2.0 (the \"License\");"
                                                  "you may not use this file except in compliance with the License. "
                                                  "You may obtain a copy of the License at\n"
                                                  "\n"
                                                  "http://www.apache.org/licenses/LICENSE-2.0\n"
                                                  "\n"
                                                  "Unless required by applicable law or agreed to in writing, software "
                                                  "distributed under the License is distributed on an \"AS IS\" BASIS, "
                                                  "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or "
                                                  "implied. "
                                                  "See the License for the specific language governing permissions and "
                                                  "limitations under the License.");
}

TEST_CASE("Can get text for MIT license enum.", "[License]")
{
        REQUIRE(license_text(License::MIT) ==
                "Permission is hereby granted, free of charge, to any person obtaining "
                "a copy of this software and associated documentation files (the "
                "\"Software\"), to deal in the Software without restriction, including "
                "without limitation the rights to use, copy, modify, merge, publish, "
                "distribute, sublicense, and/or sell copies of the Software, and to "
                "permit persons to whom the Software is furnished to do so, subject to "
                "the following conditions: "
                "The above copyright notice and this permission notice shall be included in all copies or "
                "substantial portions of the Software.\n"
                "\n"
                "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, "
                "EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF "
                "MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND "
                "NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS "
                "BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN "
                "ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN "
                "CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE "
                "SOFTWARE.");
}

TEST_CASE("Can get Apache2 license enum from string 'APACHE2'.", "[License]")
{
        REQUIRE(*license_id_from("APACHE2") == License::APACHE2);
}

TEST_CASE("Can get MIT license enum from string 'MIT'.", "[License]")
{
        REQUIRE(*license_id_from("MIT") == License::MIT);
}

TEST_CASE("Cannot get license enum from invalid string 'Unknown license'.", "[License]")
{
        REQUIRE_FALSE(license_id_from("Unknown license").has_value());
}

TEST_CASE("Can get textual representation of license enum APACHE2.", "[License]")
{
        REQUIRE(textual_representation(License::APACHE2) == "APACHE2");
}

TEST_CASE("Can get textual representation of license enum MIT.", "[License]")
{
        REQUIRE(textual_representation(License::MIT) == "MIT");
}

TEST_CASE("Can get textual representation of invalid license enum.", "[License]")
{
        REQUIRE_THROWS_MATCHES(textual_representation(static_cast<License>(100)), runtime_error,
                               Message("Unknown license id."));
}
