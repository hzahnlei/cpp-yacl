/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/flag_arg_parser.hpp"
#include <catch2/catch_all.hpp>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("The set of recognized textual representations of flag values is {true, 'yes', '1', 'false', 'no', '0'}.",
          "[Flag_Argument_Parser]")
{
	const auto &mappings = Flag_Argument_Parser::TEXT_TO_BOOL;
	REQUIRE(mappings.size() == 6);
	REQUIRE(mappings.find("true") != mappings.end());
	REQUIRE(mappings.find("yes") != mappings.end());
	REQUIRE(mappings.find("1") != mappings.end());
	REQUIRE(mappings.find("false") != mappings.end());
	REQUIRE(mappings.find("no") != mappings.end());
	REQUIRE(mappings.find("0") != mappings.end());
}

TEST_CASE("'true' is a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE(arg.value_from("true"));
}

TEST_CASE("'yes' is a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE(arg.value_from("yes"));
}

TEST_CASE("'1' is a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE(arg.value_from("1"));
}

TEST_CASE("'false' is a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE_FALSE(arg.value_from("false"));
}

TEST_CASE("'no' is a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE_FALSE(arg.value_from("no"));
}

TEST_CASE("'0' is a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE_FALSE(arg.value_from("0"));
}

TEST_CASE("Everything else is not a valid textual representation for a flag value.", "[Flag_Argument_Parser]")
{
	const Flag_Argument_Parser arg{"name", "description", param::Identifiability::BY_NAME};
	REQUIRE_THROWS_MATCHES(arg.value_from("maybe"), runtime_error,
	                       Message("Invalid value 'maybe' for flag parameter 'name'. Allowed values are '0', '1', "
	                               "'false', 'no', 'true', 'yes'."));
}
