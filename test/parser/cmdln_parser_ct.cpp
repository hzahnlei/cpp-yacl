/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/parser/int_arg_parser.hpp"
#include "yacl/parser/text_arg_parser.hpp"
#include <algorithm>
#include <catch2/catch_all.hpp>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("All mandatory data must be given upon construction of command line parser.", "[Command_Line_Parser]")
{
	Command_Line_Parser::Map_Of_Command_Parsers empty_command_set;
	vector<string> authors{};
	vector<string> oss{};
	const Command_Line_Parser cmdln_parser{"myexecutable",          "some banner", "A cool program.", "1.0.0",
	                                       move(empty_command_set), nullopt,       move(authors),     move(oss)};
	REQUIRE(cmdln_parser.program_name() == "myexecutable");
	REQUIRE(cmdln_parser.banner() == "some banner");
	REQUIRE(cmdln_parser.description() == "A cool program.");
	REQUIRE(cmdln_parser.version() == "1.0.0");
	REQUIRE(test::command_count(cmdln_parser) == 0U);
}

TEST_CASE("Commands are accessed by the name they were registered with.", "[Command_Line_Parser]")
{
	Command_Line_Parser::Map_Of_Command_Parsers command_set;
	Command_Parser::Named_Argument_Parsers named_params;
	Command_Parser::Positional_Argument_Parsers pos_params;
	command_set.try_emplace("help", "help", "some description", test::mock::command_action, move(named_params),
	                        move(pos_params));
	vector<string> authors{};
	vector<string> oss{};
	const Command_Line_Parser cmdln_parser{"myexecutable",    "some banner", "A cool program.", "1.0.0",
	                                       move(command_set), nullopt,       move(authors),     move(oss)};
	REQUIRE(test::command_count(cmdln_parser) == 1U);
	REQUIRE(cmdln_parser.command_parser("help").description() == "some description");
}

TEST_CASE("Attempt to access unknown command results in error.", "[Command_Line_Parser]")
{
	Command_Line_Parser::Map_Of_Command_Parsers command_set;
	Command_Parser::Named_Argument_Parsers named_params;
	Command_Parser::Positional_Argument_Parsers pos_params;
	command_set.try_emplace("help", "help", "some description", test::mock::command_action, move(named_params),
	                        move(pos_params));
	vector<string> authors{};
	vector<string> oss{};
	const Command_Line_Parser cmdln_parser{"myexecutable",    "some banner", "A cool program.", "1.0.0",
	                                       move(command_set), nullopt,       move(authors),     move(oss)};
	REQUIRE_THROWS_MATCHES(cmdln_parser.command_parser("HELP"), runtime_error,
	                       Message("Unknown command 'HELP'. Possible commands are: 'help'."));
}
