/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/parser/cmdln_usage_printer.hpp"
#include "yacl/parser/int_arg_parser.hpp"
#include "yacl/parser/text_arg_parser.hpp"
#include <catch2/catch_all.hpp>
#include <sstream>

using std::ostringstream;

using namespace yacl;

TEST_CASE("Printing usage does not require banner.", "[Command_Line_Parser_Builder]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"dmine"};
	cmdln_parser_builder.version("1.13.2")
			.command("server")
			.mandatory()
			.int_param("port")
			.description("Port to listen for client connections.")
			.min(0)
			.max(65535)
			.build()
			.optional()
			.text_param("lang")
			.description("Use Lisp or Miner Script query language.")
			.one_of("lisp", "miner")
			.defaults_to("miner")
			.build()
			.description("Starts a server.")
			.action(test::mock::command_action);
	cmdln_parser_builder.command("repl")
			.mandatory()
			.text_param("lang")
			.description("Use Lisp or Miner Script query language.")
			.one_of("lisp", "miner")
			.build()
			.description("Starts a REPL for experimenting.")
			.action(test::mock::command_action);

	const auto cmdln_parser = cmdln_parser_builder.description("A cool database.");

	ostringstream output;
	output << cmdln_parser;

	REQUIRE(output.str() == "A cool database.\n"
	                        "\n"
	                        "AVAILABLE COMMANDS\n"
	                        "\n"
	                        "\tHELP - This short help message.\n"
	                        "\n"
	                        "\t\tdmine help\n"
	                        "\n"
	                        "\tREPL - Starts a REPL for experimenting.\n"
	                        "\n"
	                        "\t\tdmine repl --lang <lang>\n"
	                        "\n"
	                        "\t\t--lang <lang>: Use Lisp or Miner Script query language.\n"
	                        "\t\t               Mandatory, named parameter.\n"
	                        "\t\t               Where <lang> has to be one of 'lisp', 'miner'.\n"
	                        "\n"
	                        "\tSERVER - Starts a server.\n"
	                        "\n"
	                        "\t\tdmine server [--lang <lang>] --port <port>\n"
	                        "\n"
	                        "\t\t--lang <lang>: Use Lisp or Miner Script query language.\n"
	                        "\t\t               Optional, named parameter.\n"
	                        "\t\t               Defaults to 'miner'.\n"
	                        "\t\t               Where <lang> has to be one of 'lisp', 'miner'.\n"
	                        "\n"
	                        "\t\t--port <port>: Port to listen for client connections.\n"
	                        "\t\t               Mandatory, named parameter.\n"
	                        "\t\t               Where <port> has to be in [0, 65535].\n"
	                        "\n"
	                        "\tVERSION - Version information.\n"
	                        "\n"
	                        "\t\tdmine version\n");
}

TEST_CASE("Printing usage includes banner if given.", "[Command_Line_Parser_Builder]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"dmine", "   *\n"
	                                                          "  / \\\n"
	                                                          " *   * Diamond Mine\n"
	                                                          "  \\ /\n"
	                                                          "   *"};
	cmdln_parser_builder.version("1.13.2");
	const auto cmdln_parser = cmdln_parser_builder.description("A cool database.");

	ostringstream output;
	output << cmdln_parser;

	REQUIRE(output.str() == "   *\n"
	                        "  / \\\n"
	                        " *   * Diamond Mine\n"
	                        "  \\ /\n"
	                        "   *\n"
	                        "\n"
	                        "A cool database.\n"
	                        "\n"
	                        "AVAILABLE COMMANDS\n"
	                        "\n"
	                        "\tHELP - This short help message.\n"
	                        "\n"
	                        "\t\tdmine help\n"
	                        "\n"
	                        "\tVERSION - Version information.\n"
	                        "\n"
	                        "\t\tdmine version\n");
}

TEST_CASE("Printing usage includes disclaimer if given.", "[Command_Line_Parser_Builder]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"dmine", "   *\n"
	                                                          "  / \\\n"
	                                                          " *   * Diamond Mine\n"
	                                                          "  \\ /\n"
	                                                          "   *"};
	cmdln_parser_builder.version("007");
	cmdln_parser_builder.disclaimer("It wasn't me!");
	const auto cmdln_parser = cmdln_parser_builder.description("A cool database.");

	ostringstream output;
	output << cmdln_parser;

	REQUIRE(output.str() == "   *\n"
	                        "  / \\\n"
	                        " *   * Diamond Mine\n"
	                        "  \\ /\n"
	                        "   *\n"
	                        "\n"
	                        "A cool database.\n"
	                        "\n"
	                        "AVAILABLE COMMANDS\n"
	                        "\n"
	                        "\tHELP - This short help message.\n"
	                        "\n"
	                        "\t\tdmine help\n"
	                        "\n"
	                        "\tVERSION - Version information.\n"
	                        "\n"
	                        "\t\tdmine version\n"
	                        "\n"
	                        "DISCLAIMER\n"
	                        "\n"
	                        "\tIt wasn't me!\n");
}

TEST_CASE("Anonymous command ist printed.", "[Command_Line_Parser_Builder]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"dmine", "A Banner!"};
	cmdln_parser_builder.version("1.0.34");
	cmdln_parser_builder.anonymous_command()
			.description("anonymous does strange things")
			.mandatory()
			.text_param("filename")
			.description("name of file")
			.action([](const auto &, auto &, auto &, auto &) {});
	const auto cmdln_parser = cmdln_parser_builder.description("A command line tool.");

	ostringstream output;
	output << cmdln_parser;

	REQUIRE(output.str() ==
	        "A Banner!\n"
	        "\n"
	        "A command line tool.\n"
	        "\n"
	        "AVAILABLE COMMANDS\n"
	        "\n"
	        "\tHELP - This short help message.\n"
	        "\n"
	        "\t\tdmine help\n"
	        "\n"
	        "\tVERSION - Version information.\n"
	        "\n"
	        "\t\tdmine version\n"
	        "\n"
	        "\tIF NO COMMAND SPECIFIED - anonymous does strange things\n"
	        "\n"
	        "\t\tdmine --filename <filename>\n"
	        "\n"
	        "\t\t--filename <filename>: name of file\n"
	        "\t\t                       Mandatory, named parameter.\n"
	        "\t\t                       Where <filename> is a none-empty string (must not contain whitespace).\n");
}

TEST_CASE("Positional arguments are printed.", "[Command_Line_Parser_Builder]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"dmine", "A Banner!"};
	cmdln_parser_builder.version("1.0.34");
	cmdln_parser_builder.anonymous_command()
			.description("anonymous does strange things")
			.mandatory()
			.positional()
			.text_param("filename")
			.description("name of file")
			.action([](const auto &, auto &, auto &, auto &) {});
	const auto cmdln_parser = cmdln_parser_builder.description("A command line tool.");

	ostringstream output;
	output << cmdln_parser;

	REQUIRE(output.str() ==
	        "A Banner!\n"
	        "\n"
	        "A command line tool.\n"
	        "\n"
	        "AVAILABLE COMMANDS\n"
	        "\n"
	        "\tHELP - This short help message.\n"
	        "\n"
	        "\t\tdmine help\n"
	        "\n"
	        "\tVERSION - Version information.\n"
	        "\n"
	        "\t\tdmine version\n"
	        "\n"
	        "\tIF NO COMMAND SPECIFIED - anonymous does strange things\n"
	        "\n"
	        "\t\tdmine <filename>\n"
	        "\n"
	        "\t\t<filename>: name of file\n"
	        "\t\t            Mandatory, positional parameter.\n"
	        "\t\t            Where <filename> is a none-empty string (must not contain whitespace).\n");
}
