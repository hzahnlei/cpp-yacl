/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/builder/flag_arg_parser_builder.hpp"
#include "yacl/parser/flag_arg_parser.hpp"
#include <catch2/catch_all.hpp>
#include <stdexcept>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Flag parameters need to have a name.", "[Flag_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	constexpr auto empty_param_name = "";
	REQUIRE_THROWS_MATCHES((Flag_Argument_Parser_Builder{*cmd_parser_builder, empty_param_name,
	                                                     param::Requiredness::MANDATORY,
	                                                     param::Identifiability ::BY_NAME}),
	                       Argument_Parser_Builder_Error,
	                       Message("Parameter name for command 'commandName' must not be empty."));
}

TEST_CASE("Flag parameter names must not contain whitespace.", "[Flag_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	REQUIRE_THROWS_MATCHES((Flag_Argument_Parser_Builder{*cmd_parser_builder, "some arg name",
	                                                     param::Requiredness::MANDATORY,
	                                                     param::Identifiability ::BY_NAME}),
	                       Argument_Parser_Builder_Error,
	                       Message("Parameter name 'some arg name' for command 'commandName' must not contain "
	                               "white space."));
}

TEST_CASE("Mandatory flag parameters must not have a default value.", "[Flag_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Flag_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.defaults_to(false);
	REQUIRE_THROWS_MATCHES(arg_parser_builder.description("param descr").build(), Argument_Parser_Builder_Error,
	                       Message("Mandatory parameter 'paramName' of command 'commandName' must not have a "
	                               "default value."));
}

TEST_CASE("Default values of flag parameters must not violate contraints.", "[Flag_Argument_Parser_Builder]")
{
	// Intentionally left blank.
	// Not applicable, there can only be true/false.
}

#include <memory>

TEST_CASE("Parser for mandatory flag parameter can only be constructed by correctly initialized arg parser builder.",
          "[Flag_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Flag_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Flag_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Parser for optional flag parameter can only be constructed by correctly initialized arg parser builder.",
          "[Flag_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Flag_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE_FALSE(arg_parser.is_mandatory());
				REQUIRE(arg_parser.defaults_to().has_value());
				REQUIRE(*(arg_parser.defaults_to()));
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Flag_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.defaults_to(true);
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Flag parameter description must not be empty.", "[Flag_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Flag_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	constexpr auto empty_description = "";
	REQUIRE_THROWS_MATCHES(
			arg_parser_builder.description(empty_description), Argument_Parser_Builder_Error,
			Message("Description of parameter 'paramName' of command 'commandName' must not be empty."));
}

TEST_CASE("Building a flag arg parser can be finalized by calling build.", "[Flag_Argument_Parser_Builder]")
{
	// Intentionally left blank.
	// Demonstrated by many other test cases.
}

TEST_CASE("Building a flag arg parser can be finalized by calling optional.", "[Flag_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Flag_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Flag_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.description("param descr").optional();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	cmd_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Building a flag arg parser can be finalized by calling mandatory.", "[Flag_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Flag_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Flag_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.description("param descr").mandatory();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	cmd_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Flag parameters need a description before building the actual parser.", "[Flag_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Flag_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};

	// No description given!
	REQUIRE_THROWS_MATCHES(arg_parser_builder.build(), Argument_Parser_Builder_Error,
	                       Message("Description for parameter 'paramName' of command 'commandName' missing."));
}
