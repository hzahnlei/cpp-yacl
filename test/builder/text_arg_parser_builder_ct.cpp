/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/builder/text_arg_parser_builder.hpp"
#include "yacl/parser/text_arg_parser.hpp"
#include <catch2/catch_all.hpp>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Text parameters need to have a name.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	REQUIRE_THROWS_MATCHES((Text_Argument_Parser_Builder{*cmd_parser_builder, "", param::Requiredness::MANDATORY,
	                                                     param::Identifiability ::BY_NAME}),
	                       Argument_Parser_Builder_Error,
	                       Message("Parameter name for command 'commandName' must not be empty."));
}

TEST_CASE("Text parameter names must not contain white space.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	REQUIRE_THROWS_MATCHES((Text_Argument_Parser_Builder{*cmd_parser_builder, "some arg name",
	                                                     param::Requiredness::MANDATORY,
	                                                     param::Identifiability ::BY_NAME}),
	                       Argument_Parser_Builder_Error,
	                       Message("Parameter name 'some arg name' for command 'commandName' must not contain "
	                               "white space."));
}

TEST_CASE("Mandatory text parameters must not have a default value.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.defaults_to("bla, blub");
	REQUIRE_THROWS_MATCHES(arg_parser_builder.description("param descr").build(), Argument_Parser_Builder_Error,
	                       Message("Mandatory parameter 'paramName' of command 'commandName' must not have a "
	                               "default value."));
}

TEST_CASE("Default text values must not violate contraints.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.one_of("abc", "123");
	constexpr auto invalid_default = "xyz";
	arg_parser_builder.defaults_to(invalid_default);
	REQUIRE_THROWS_MATCHES(arg_parser_builder.description("param descr").build(), Argument_Parser_Error,
	                       Message("Default value is invalid. "
	                               "Value 'xyz' not in set of allowed values for text parameter 'paramName'."));
}

TEST_CASE("Default text values must not be empty.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.one_of("abc", "123");
	constexpr auto empty_default = "";
	arg_parser_builder.defaults_to(empty_default);
	REQUIRE_THROWS_MATCHES(arg_parser_builder.description("param descr").build(), Argument_Parser_Error,
	                       Message("Default value is invalid. "
	                               "Value for string parameter 'paramName' must not be empty."));
}

TEST_CASE("Do not repeat 'allow empty text'.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	REQUIRE_THROWS_MATCHES(arg_parser_builder.allow_empty_text().allow_empty_text(), Argument_Parser_Builder_Error,
	                       Message("Trying to override allow-empty property of parameter 'paramName'."));
}

TEST_CASE("Default text value must be from choices if choices given.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.one_of("abc", "123");
	arg_parser_builder.allow_empty_text();
	constexpr auto default_not_from_choices = "xyz";
	arg_parser_builder.defaults_to(default_not_from_choices);
	REQUIRE_THROWS_MATCHES(arg_parser_builder.description("param descr").build(), Argument_Parser_Error,
	                       Message("Default value is invalid. "
	                               "Value 'xyz' not in set of allowed values for text parameter 'paramName'."));
}

TEST_CASE("Default text value can be empty if empty choice given.", "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE_FALSE(arg_parser.is_mandatory());
				REQUIRE(arg_parser.defaults_to().has_value());
				REQUIRE(*(arg_parser.defaults_to()) == "");
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.allow_empty_text();
	arg_parser_builder.defaults_to("");
	arg_parser_builder.one_of("", "abc", "123");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Default text value can be empty if no choice given but empty values allowed.",
          "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE_FALSE(arg_parser.is_mandatory());
				REQUIRE(arg_parser.defaults_to().has_value());
				REQUIRE(*(arg_parser.defaults_to()) == "");
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.allow_empty_text();
	arg_parser_builder.defaults_to("");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Default value of optional text parameter must not contain whitespace if not allowed.",
          "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	constexpr auto default_with_whitespace = "bla blub";
	arg_parser_builder.defaults_to(default_with_whitespace);
	REQUIRE_THROWS_MATCHES(
			arg_parser_builder.description("param descr").build(), Argument_Parser_Error,
			Message("Default value is invalid. "
	                        "Value 'bla blub' for string parameter 'paramName' must not contain white space."));
}

TEST_CASE("Default text value might contain whitespace if allowed.", "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE_FALSE(arg_parser.is_mandatory());
				REQUIRE(arg_parser.defaults_to().has_value());
				REQUIRE(*(arg_parser.defaults_to()) == "bla blub");
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.allow_white_space();
	arg_parser_builder.defaults_to("bla blub");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Default value of mandatory text parameter must not contain whitespace if not allowed.",
          "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	REQUIRE_THROWS_MATCHES(arg_parser_builder.one_of("abc", "a b c", "xzy"), Argument_Parser_Builder_Error,
	                       Message("Choice 'a b c' for parameter 'paramName' of command 'commandName' contains "
	                               "whitespace."));
}

TEST_CASE("Do not repeat 'allow whitespace'.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	REQUIRE_THROWS_MATCHES(arg_parser_builder.allow_white_space().allow_white_space(),
	                       Argument_Parser_Builder_Error,
	                       Message("Trying to override allow-white-space property of parameter 'paramName'."));
}

TEST_CASE("Choices might contain whitespace if allowed.", "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
				REQUIRE(arg_parser.one_of()[0] == "abc");
				REQUIRE(arg_parser.one_of()[1] == "a b c");
				REQUIRE(arg_parser.one_of()[2] == "xyz");
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.allow_white_space();
	arg_parser_builder.one_of("abc", "a b c", "xyz");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Choices must not be empty if not allowed.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	REQUIRE_THROWS_MATCHES(arg_parser_builder.one_of("abc", "", "xzy"), Argument_Parser_Builder_Error,
	                       Message("Choice for parameter 'paramName' of command 'commandName' is empty."));
}

TEST_CASE("Choices might be empty if allowed.", "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
				REQUIRE(arg_parser.one_of()[0] == "abc");
				REQUIRE(arg_parser.one_of()[1] == "");
				REQUIRE(arg_parser.one_of()[2] == "xyz");
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.allow_empty_text();
	arg_parser_builder.one_of("abc", "", "xyz");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Choices must be unique.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	REQUIRE_THROWS_MATCHES(arg_parser_builder.one_of("abc", "xyz", "abc"), Argument_Parser_Builder_Error,
	                       Message("Duplicate choice 'abc' for parameter 'paramName' of command 'commandName'."));
}

TEST_CASE("Mandatory text arg parser can be constructed by correctly initialized arg parser builder.",
          "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
				REQUIRE(arg_parser.one_of() == (vector<string>{"8080", "8085", "Z80", "8086"}));
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.one_of("8080", "8085", "Z80", "8086");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Optional text arg parser can be constructed by correctly initialized arg parser builder.",
          "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE_FALSE(arg_parser.is_mandatory());
				REQUIRE(arg_parser.defaults_to().has_value());
				REQUIRE(*(arg_parser.defaults_to()) == "Z80");
				REQUIRE(arg_parser.one_of() == (vector<string>{"8080", "8085", "Z80", "8086"}));
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.one_of("8080", "8085", "Z80", "8086");
	arg_parser_builder.defaults_to("Z80");
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Text parameter description must not be empty.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName", param::Requiredness::OPTIONAL,
	                                                param::Identifiability ::BY_NAME};
	REQUIRE_THROWS_MATCHES(
			arg_parser_builder.description(""), Argument_Parser_Builder_Error,
			Message("Description of parameter 'paramName' of command 'commandName' must not be empty."));
}

TEST_CASE("Building a text arg parser can be finalized by calling build.", "[Text_Argument_Parser_Builder]")
{
	// Intentionally left blank.
	// Demonstrated by many other test cases.
}

TEST_CASE("Building a text arg parser can be finalized by calling optional.", "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.description("param descr").optional();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	cmd_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Building a text arg parser can be finalized by calling mandatory.", "[Text_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Text_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Text_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName", param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	arg_parser_builder.description("param descr").mandatory();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	cmd_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Text parameters need a description before building the actual parser.", "[Text_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Text_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                param::Requiredness::MANDATORY,
	                                                param::Identifiability ::BY_NAME};
	// No description given!
	REQUIRE_THROWS_MATCHES(arg_parser_builder.build(), Argument_Parser_Builder_Error,
	                       Message("Description for parameter 'paramName' of command 'commandName' missing."));
}
