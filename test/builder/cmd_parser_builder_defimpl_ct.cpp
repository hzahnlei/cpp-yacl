/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/parser/flag_arg_parser.hpp"
#include "yacl/parser/int_arg_parser.hpp"
#include <catch2/catch_all.hpp>
#include <optional>

using Catch::Matchers::Message;
using std::make_unique;

using namespace yacl;

TEST_CASE("Command name must not be empty.", "[Command_Parser_Builder_Default_Impl]")
{
	const auto empty_command_name = "";
	REQUIRE_THROWS_MATCHES(
			(Command_Parser_Builder_Default_Impl{test::mock::registration_function, empty_command_name}),
			runtime_error, Message("Command name must not be empty."));
}

TEST_CASE("Command name must not contain whitespace.", "[Command_Parser_Builder_Default_Impl]")
{
	const auto command_name_with_whitespace = "Hello, World!";
	REQUIRE_THROWS_MATCHES((Command_Parser_Builder_Default_Impl{test::mock::registration_function,
	                                                            command_name_with_whitespace}),
	                       runtime_error, Message("Command name 'Hello, World!' must not contain white space."));
}

TEST_CASE("Command description must not be empty.", "[Command_Parser_Builder_Default_Impl]")
{
	Command_Parser_Builder_Default_Impl command{test::mock::registration_function, "repl"};
	const auto empty_description = "";
	REQUIRE_THROWS_MATCHES(command.description(empty_description), runtime_error,
	                       Message("Description of command 'repl' must not be empty."));
}

TEST_CASE("Command description can only be set once.", "[Command_Parser_Builder_Default_Impl]")
{
	Command_Parser_Builder_Default_Impl command{test::mock::registration_function, "repl"};
	const auto description = "bla, blub";
	const auto another_description = "Duff beer!";
	REQUIRE_THROWS_MATCHES(command.description(description).description(another_description), runtime_error,
	                       Message("Description of command 'repl' already set. Trying to override 'bla, "
	                               "blub' with 'Duff beer!'."));
}

TEST_CASE("Command not registered if no action given.", "[Command_Parser_Builder_Default_Impl]")
{
	REQUIRE_NOTHROW((Command_Parser_Builder_Default_Impl{test::mock::registration_function_that_throws,
	                                                     "someCommand"}));
}

TEST_CASE("Command only registered if action given.", "[Command_Parser_Builder_Default_Impl]")
{
	auto registration_fun_has_been_invoked = false;
	Command_Parser_Builder_Default_Impl command{
			[&](const auto &, const auto &) { registration_fun_has_been_invoked = true; }, "someCommand"};
	command.description("Some description").action(test::mock::command_action);
	REQUIRE(registration_fun_has_been_invoked);
}

TEST_CASE("Action can only be associated with command once comand is described.",
          "[Command_Parser_Builder_Default_Impl]")
{
	Command_Parser_Builder_Default_Impl command{test::mock::registration_function, "repl"};
	REQUIRE_THROWS_MATCHES(command.action(test::mock::command_action), runtime_error,
	                       Message("Description missing for command 'repl'."));
}

TEST_CASE("Parameters of command need to have a unique name.", "[Command_Parser_Builder_Default_Impl]")
{
	Command_Parser_Builder_Default_Impl command{test::mock::registration_function, "repl"};
	command.register_named_parameter(make_unique<Flag_Argument_Parser>("flagArg", "flagArg description",
	                                                                   param::Identifiability::BY_NAME));
	REQUIRE_THROWS_MATCHES(command.register_named_parameter(make_unique<Integer_Argument_Parser>(
					       "flagArg", "intArg description", param::Identifiability::BY_NAME, 0, 100,
					       nullopt)),
	                       runtime_error,
	                       Message("Parameter with name 'flagArg' already defined for command 'repl'."));
}
