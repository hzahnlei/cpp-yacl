/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/parser/int_arg_parser.hpp"
#include "test_tools/test_tools.hpp"
#include "yacl/parser/text_arg_parser.hpp"
#include <algorithm>
#include <catch2/catch_all.hpp>
#include <tuple>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Program name and banner can be set upon construction.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.version("1.0.0");
        const auto cmdln_parser = cmdln_parser_builder.description("some descr");
        REQUIRE(cmdln_parser.program_name() == "myexecutable");
        REQUIRE(cmdln_parser.banner() == "A cool program.");
        REQUIRE(cmdln_parser.has_banner());
        REQUIRE(cmdln_parser.description() == "some descr");
}

TEST_CASE("Program name must not be empty.", "[Command_Line_Parser_Builder]")
{
        constexpr auto empty_exe_name = "";
        REQUIRE_THROWS_MATCHES((Command_Line_Parser_Builder{empty_exe_name, "A cool program."}), runtime_error,
                               Message("Program name must not be empty."));
}

TEST_CASE("Program name must not contain whitespace.", "[Command_Line_Parser_Builder]")
{
        constexpr auto exe_name_with_whitespace = "my exe";
        REQUIRE_THROWS_MATCHES((Command_Line_Parser_Builder{exe_name_with_whitespace, "A cool program."}),
                               runtime_error, Message("Program name 'my exe' must not contain white space."));
}

auto try_construct_parser_from_builder = [](auto &cmdln_parser_builder)
{
        // Calling description triggers building the parser.
        return cmdln_parser_builder.description("some descr");
};

TEST_CASE("Program version must not be missing.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_THROWS_MATCHES(try_construct_parser_from_builder(cmdln_parser_builder), runtime_error,
                               Message("Version information missing."));
}

TEST_CASE("Program banner is optional and thus can be empty.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", ""};
        const auto cmdln_parser = cmdln_parser_builder.version("1.0.0").description("some descr");
        REQUIRE(cmdln_parser.program_name() == "myexecutable");
        REQUIRE(cmdln_parser.banner() == "");
        REQUIRE_FALSE(cmdln_parser.has_banner());
        REQUIRE(cmdln_parser.description() == "some descr");
}

TEST_CASE("Program banner is optional and can be omitted.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable"};
        cmdln_parser_builder.version("1.0.0");
        const auto cmdln_parser = cmdln_parser_builder.description("some descr");
        REQUIRE(cmdln_parser.program_name() == "myexecutable");
        REQUIRE(cmdln_parser.banner() == "");
        REQUIRE_FALSE(cmdln_parser.has_banner());
        REQUIRE(cmdln_parser.description() == "some descr");
}

TEST_CASE("Program description cannot be empty.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable"};
        constexpr auto empty_description = "";
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.description(empty_description), runtime_error,
                               Message("Program description must not be empty."));
}

constexpr auto HELP_COMMAND = 1U;
constexpr auto VERSION_COMMAND = 1U;
constexpr auto DEFAULT_COMMAND_COUNT = HELP_COMMAND + VERSION_COMMAND;

TEST_CASE("Command is not registed until action given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.version("1.0.0");
        // Normally we would not ignore return value of comman, however, in this test, the value returnd can safely be
        // ignored. With an assignment to std::ignore we can avoid complaints by the compiler.
        std::ignore = cmdln_parser_builder.command("repl");
        const auto cmdln_parser = cmdln_parser_builder.description("some description.");
        REQUIRE(test::command_count(cmdln_parser) == DEFAULT_COMMAND_COUNT);
}

TEST_CASE("Command name must be unique.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.version("1.0.0");
        constexpr auto COMMAND_NAME{"repl"};
        cmdln_parser_builder.command(COMMAND_NAME).description("description").action(test::mock::command_action);
        constexpr auto DUPLICATE_COMMAND_NAME{COMMAND_NAME};
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.command(DUPLICATE_COMMAND_NAME)
                                   .description("description")
                                   .action(test::mock::command_action),
                               runtime_error, Message("Command 'repl' already registered."));
}

TEST_CASE("Command is registed once action given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.version("1.0.0");
        cmdln_parser_builder.command("repl").description("description").action(test::mock::command_action);
        const auto cmdln_parser = cmdln_parser_builder.description("some description.");
        REQUIRE(test::command_count(cmdln_parser) == DEFAULT_COMMAND_COUNT + 1U);
}

TEST_CASE("Mandatory parameters do not have a default value.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_THROWS_MATCHES(
            cmdln_parser_builder.command("server")
                .mandatory()
                .int_param("port")
                .description("Port to listen for client connections.")
                .defaults_to(8080)
                .build()
                .action(test::mock::command_action),
            runtime_error, Message("Mandatory parameter 'port' of command 'server' must not have a default value."));
}

TEST_CASE("Optional parameters must have a default value.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.command("server")
                                   .optional()
                                   .int_param("port")
                                   .description("Port to listen for client connections.")
                                   .build(),
                               runtime_error,
                               Message("Optional parameter 'port' of command 'server' is missing a default value."));
}

TEST_CASE("Default int value must adhere to constraints.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        // clang-format off
        REQUIRE_THROWS_MATCHES(
                cmdln_parser_builder.command("server")
                    .optional()
                        .int_param("port")
                        .description("Port to listen for client connections.")
                        .min(1000)
                        .max(10000)
                        .defaults_to(50000)
                    .build(),
            runtime_error,
            Message("Default value is invalid. Value for integer parameter 'port' must not be greater than "
                    "10000 but is 50000."));
        // clang-format on
}

TEST_CASE("Default text value must adhere to constraints.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_THROWS_MATCHES(
            cmdln_parser_builder.command("server")
                .optional()
                .text_param("lang")
                .description("A choice of languages.")
                .one_of("lisp")
                .one_of("java")
                .one_of("cpp")
                .one_of("bliss")
                .defaults_to("50000")
                .build(),
            runtime_error,
            Message("Default value is invalid. Value '50000' not in set of allowed values for text parameter 'lang'."));
}

TEST_CASE("Optional program version can be given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_NOTHROW(cmdln_parser_builder.version("1.13.2"));
}

TEST_CASE("Optional program version must not be empty when given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto empty_version = "";
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.version(empty_version), runtime_error,
                               Message("Program version must not be empty."));
}

TEST_CASE("Optional program version can only be set once.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.version("1.13.2");
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.version("2.0.0"), runtime_error,
                               Message("Trying to override program version '1.13.2' with '2.0.0'."));
}

TEST_CASE("Optional copyright statement can be given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_NOTHROW(cmdln_parser_builder.copyright("(c) forever by me"));
}

TEST_CASE("Optional copyright statement must not be empty when given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto empty_copyright = "";
        REQUIRE_THROWS_MATCHES(
            cmdln_parser_builder.copyright(empty_copyright), runtime_error,
            Message("Copyright statement must not be empty when given. "
                    "It is optional. "
                    "Just leave it out, if it is your intention not to provide a copyright statement."));
}

TEST_CASE("Optional copyright statement can only be set once.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.copyright("(c) forever by me");
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.copyright("(c) you"), runtime_error,
                               Message("Trying to override copyright statement '(c) forever by me' with '(c) you'."));
}

TEST_CASE("Optional license statement can be given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_NOTHROW(cmdln_parser_builder.license(License::MIT));
}

TEST_CASE("Invalid license values are possible in C++.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        // This is possible in C++ because 100 is an integer and enums are integers,
        // too!?!
        REQUIRE_NOTHROW(cmdln_parser_builder.license(static_cast<License>(100)));
}

TEST_CASE("Optional license statement can only be set once.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.license(License::MIT);
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.license(License::APACHE2), runtime_error,
                               Message("Trying to override license MIT with APACHE2."));
}

TEST_CASE("Optional disclaimer can be given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_NOTHROW(cmdln_parser_builder.disclaimer("IT'S NOT MY FAULT"));
}

TEST_CASE("Optional disclaimer must not be empty when given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto empty_disclaimer = "";
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.disclaimer(empty_disclaimer), runtime_error,
                               Message("Disclaimer must not be empty when given. "
                                       "It is optional. "
                                       "Just leave it out, if it is your intention not to provide a disclaimer."));
}

TEST_CASE("Optional disclaimer can only be set once.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        cmdln_parser_builder.disclaimer("IT'S NOT MY FAULT");
        REQUIRE_THROWS_MATCHES(
            cmdln_parser_builder.disclaimer("IT'S ALL YOUR FAULT"), runtime_error,
            Message("Trying to override disclaimer 'IT\\'S NOT MY FAULT' with 'IT\\'S ALL YOUR FAULT'."));
}

TEST_CASE("Optionally, multiple authors can be given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_NOTHROW(cmdln_parser_builder.author("me").author("myself").author("and I"));
}

TEST_CASE("Authors must be unique.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto DUPLICATE_AUTHOR = "me";
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.author(DUPLICATE_AUTHOR).author("myself").author(DUPLICATE_AUTHOR),
                               runtime_error, Message("Duplicate author 'me'."));
}

TEST_CASE("Authors must not be empty when given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto EMPTY_AUTHOR = "";
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.author(EMPTY_AUTHOR), runtime_error,
                               Message("Author name must not be empty when given. It is optional. Just leave it out, "
                                       "if it is your intention not to provide information on the author(s)."));
}

TEST_CASE("Optionally, multiple OSS can be given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        REQUIRE_NOTHROW(
            cmdln_parser_builder.open_source_software_used("RocksDB").open_source_software_used("Google Test"));
}

TEST_CASE("OSS must be unique.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto DUPLICATE_OSS = "RocksDB";
        REQUIRE_THROWS_MATCHES(cmdln_parser_builder.open_source_software_used(DUPLICATE_OSS)
                                   .open_source_software_used("Google Test")
                                   .open_source_software_used(DUPLICATE_OSS),
                               runtime_error, Message("Duplicate open source usage statement 'RocksDB'."));
}

TEST_CASE("OSS must not be empty when given.", "[Command_Line_Parser_Builder]")
{
        Command_Line_Parser_Builder cmdln_parser_builder{"myexecutable", "A cool program."};
        constexpr auto EMPTY_OSS = "";
        REQUIRE_THROWS_MATCHES(
            cmdln_parser_builder.open_source_software_used(EMPTY_OSS), runtime_error,
            Message(
                "Open source usage statement must not be empty when given. It is optional. Just leave it out, if it is "
                "your intention not to provide information on open source software used to build this software."));
}