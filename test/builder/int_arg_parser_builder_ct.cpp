/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/builder/int_arg_parser_builder.hpp"
#include "yacl/parser/int_arg_parser.hpp"
#include <catch2/catch_all.hpp>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Int parameters need to have a name.", "[Integer_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	constexpr auto empty_param_name = "";
	REQUIRE_THROWS_MATCHES((Integer_Argument_Parser_Builder{*cmd_parser_builder, empty_param_name,
	                                                        param::Requiredness::MANDATORY,
	                                                        param::Identifiability ::BY_NAME}),
	                       Argument_Parser_Builder_Error,
	                       Message("Parameter name for command 'commandName' must not be empty."));
}

TEST_CASE("Int parameter names must not contain whitespace.", "[Integer_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	REQUIRE_THROWS_MATCHES((Integer_Argument_Parser_Builder{*cmd_parser_builder, "some arg name",
	                                                        param::Requiredness::MANDATORY,
	                                                        param::Identifiability ::BY_NAME}),
	                       Argument_Parser_Builder_Error,
	                       Message("Parameter name 'some arg name' for command 'commandName' must not contain "
	                               "white space."));
}

TEST_CASE("Mandatory int parameters must not have a default value.", "[Integer_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Integer_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                   param::Requiredness::MANDATORY,
	                                                   param::Identifiability ::BY_NAME};
	arg_parser_builder.defaults_to(123);
	REQUIRE_THROWS_MATCHES(arg_parser_builder.description("param descr").build(), Argument_Parser_Builder_Error,
	                       Message("Mandatory parameter 'paramName' of command 'commandName' must not have a "
	                               "default value."));
}

TEST_CASE("Default int values must not violate contraints.", "[Integer_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Integer_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                   param::Requiredness::OPTIONAL,
	                                                   param::Identifiability ::BY_NAME};
	arg_parser_builder.min(0);
	arg_parser_builder.max(65535);
	constexpr auto invalid_default = -1000;
	arg_parser_builder.defaults_to(invalid_default);
	REQUIRE_THROWS_MATCHES(
			arg_parser_builder.description("param descr").build(), Argument_Parser_Error,
			Message("Default value is invalid. "
	                        "Value for integer parameter 'paramName' must not be less than 0 but is -1000."));
}

TEST_CASE("Mandatory int arg parser can be constructed by correctly initialized arg parser builder.",
          "[Integer_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Integer_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
				REQUIRE(arg_parser.min() == 0);
				REQUIRE(arg_parser.max() == 65535);
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Integer_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName",
	                                                   param::Requiredness::MANDATORY,
	                                                   param::Identifiability ::BY_NAME};
	arg_parser_builder.min(0);
	arg_parser_builder.max(65535);
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Optional int arg parser can be constructed by correctly initialized arg parser builder.",
          "[Integer_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Integer_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE_FALSE(arg_parser.is_mandatory());
				REQUIRE(arg_parser.defaults_to().has_value());
				REQUIRE(*(arg_parser.defaults_to()) == 6502);
				REQUIRE(arg_parser.min() == 0);
				REQUIRE(arg_parser.max() == 65535);
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Integer_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName",
	                                                   param::Requiredness::OPTIONAL,
	                                                   param::Identifiability ::BY_NAME};
	arg_parser_builder.min(0);
	arg_parser_builder.max(65535);
	arg_parser_builder.defaults_to(6502);
	auto &another_ref_to_cmd_arg_parser_builder = arg_parser_builder.description("param descr").build();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	another_ref_to_cmd_arg_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Int parameter description must not be empty.", "[Integer_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Integer_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                   param::Requiredness::OPTIONAL,
	                                                   param::Identifiability ::BY_NAME};
	constexpr auto empty_description = "";
	REQUIRE_THROWS_MATCHES(
			arg_parser_builder.description(empty_description), Argument_Parser_Builder_Error,
			Message("Description of parameter 'paramName' of command 'commandName' must not be empty."));
}

TEST_CASE("Building an int arg parser can be finalized by calling build.", "[Integer_Argument_Parser_Builder]")
{
	// Intentionally left blank.
	// Demonstrated by many other test cases.
}

TEST_CASE("Building an int arg parser can be finalized by calling optional.", "[Integer_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Integer_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Integer_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName",
	                                                   param::Requiredness::MANDATORY,
	                                                   param::Identifiability ::BY_NAME};
	arg_parser_builder.description("param descr").optional();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	cmd_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Building an int arg parser can be finalized by calling mandatory.", "[Integer_Argument_Parser_Builder]")
{
	Command_Parser_Builder_Default_Impl cmd_parser_builder{
			[](const auto &, auto &&cmd_def)
			{
				const auto &arg_parser = static_cast<const Integer_Argument_Parser &>(
						cmd_def.parameter("paramName"));
				REQUIRE(arg_parser.name() == "paramName");
				REQUIRE(arg_parser.description() == "param descr");
				REQUIRE(arg_parser.is_mandatory());
				REQUIRE_FALSE(arg_parser.defaults_to().has_value());
			},
			"commandName"};
	cmd_parser_builder.description("command descr");

	Integer_Argument_Parser_Builder arg_parser_builder{cmd_parser_builder, "paramName",
	                                                   param::Requiredness::MANDATORY,
	                                                   param::Identifiability ::BY_NAME};
	arg_parser_builder.description("param descr").mandatory();

	// Now we have set up our builder. Calling action will trigger the built of
	// the actual parser. Verification takes place in the command registration
	// lambda above.
	cmd_parser_builder.action(test::mock::command_action);
}

TEST_CASE("Int parameters need a description before building the actual parser.", "[Integer_Argument_Parser_Builder]")
{
	auto cmd_parser_builder = test::some_cmd_parser_builder();
	Integer_Argument_Parser_Builder arg_parser_builder{*cmd_parser_builder, "paramName",
	                                                   param::Requiredness::MANDATORY,
	                                                   param::Identifiability ::BY_NAME};

	// No description given!
	REQUIRE_THROWS_MATCHES(arg_parser_builder.build(), Argument_Parser_Builder_Error,
	                       Message("Description for parameter 'paramName' of command 'commandName' missing."));
}
