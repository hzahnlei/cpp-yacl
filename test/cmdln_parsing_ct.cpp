/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "test_tools/test_tools.hpp"
#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include "yacl/text_cmdln_arg.hpp"
#include <catch2/catch_all.hpp>
#include <iostream>
#include <sstream>

using Catch::Matchers::Message;
using std::cerr;
using std::cin;
using std::cout;
using std::istream;
using std::istringstream;
using std::ostream;
using std::ostringstream;

using namespace yacl;

auto some_command_line_parser()
{
	Command_Line_Parser_Builder cmdln_parser_builder{"dmine"};
	// clang-format off
        cmdln_parser_builder
            .version("0815")
            .command("server")
                .description("Starts a server.")
                .mandatory()
                    .int_param("port")
                    .description("Port to listen for client connections.")
                    .min(0)
                    .max(65535)
                .optional()
                    .text_param("lang")
                    .description("Use Lisp or Miner Script query language.")
                    .one_of("lisp", "miner")
                    .defaults_to("miner")
                .optional()
                    .flag_param("cleandb")
                    .description("Delete database files.")
                    .defaults_to(false)
            .action(test::mock::command_action);
	// clang-format on
	return cmdln_parser_builder.description("A cool database.");
}

TEST_CASE("Parse empty command line will give usage message.", "[Command_Line_Parser]")
{
	const auto cmdln_parser = some_command_line_parser();
	const vector<string> empty_arg_list;
	REQUIRE_THROWS_MATCHES(cmdln_parser.command_from(empty_arg_list), runtime_error,
	                       Message("No arguments given. Possible commands are: 'help', 'server', 'version'."));
}

auto command_line_parser_with_regular_and_anonymous_commands()
{
	Command_Line_Parser_Builder cmdln_parser_builder{"myprogram"};
	// clang-format off
        cmdln_parser_builder
		.version("0815")
		.command("namedcommand")
			.description("A command with a name.")
			.mandatory()
				.text_param("x")
				.description("xxx")
			.optional()
				.text_param("y")
				.description("yyy")
				.defaults_to("ydefault")
			.action([&](const Execution_Context &, istream &, ostream &, ostream &) -> void {});
	cmdln_parser_builder
		.anonymous_command()
			.description("A command without a name.")
			.optional()
				.text_param("z")
				.description("zzz")
				.defaults_to("zdefault")
				.action([&](const Execution_Context &, istream &, ostream &, ostream &) -> void {});
	// clang-format on
	return cmdln_parser_builder.description("My cool program.");
}

TEST_CASE("Parse empty command line will result in anonymous command (when configured).", "[Command_Line_Parser]")
{
	const auto cmdln_parser{command_line_parser_with_regular_and_anonymous_commands()};
	const vector<string> empty_arg_list;
	const auto command{cmdln_parser.command_from(empty_arg_list)};
	REQUIRE(command != nullptr);
	REQUIRE(command->is_anonymous_command());
}

TEST_CASE("Parse non-empty command line will result in anonymous command (when configured).", "[Command_Line_Parser]")
{
	const auto cmdln_parser{command_line_parser_with_regular_and_anonymous_commands()};
	const vector<string> non_empty_arg_list{"--z", "abc"};
	const auto command{cmdln_parser.command_from(non_empty_arg_list)};
	REQUIRE(command != nullptr);
	REQUIRE(command->is_anonymous_command());
}

auto command_line_parser_with_positional_parameters()
{
	Command_Line_Parser_Builder cmdln_parser_builder{"myprogram"};
	// clang-format off
        cmdln_parser_builder
		.version("0815")
		.command("namedcommand")
			.description("A command with a name.")
			.mandatory().positional()
				.text_param("x")
				.description("xxx")
			.optional().positional()
				.text_param("y")
				.description("yyy")
				.defaults_to("ydefault")
			.action([&](const Execution_Context &, istream &, ostream &, ostream &) -> void {
			});
	cmdln_parser_builder
		.anonymous_command()
			.description("A command without a name.")
			.optional().positional()
				.text_param("z")
				.allow_empty_text()
				.allow_white_space()
				.description("zzz")
				.defaults_to("")
			.action([&](const Execution_Context &context, istream &, ostream &, ostream &) -> void {
				const auto &val{context.text_value_at(0)};
                    		REQUIRE(val == "thefilename");
			});
	// clang-format on
	return cmdln_parser_builder.description("My cool program.");
}

TEST_CASE("Process positional parameters.", "[Command_Line_Parser]")
{
	const auto cmdln_parser{command_line_parser_with_positional_parameters()};
	const vector<string> arg_list_with_positional_parameter{"thefilename"};
	const auto command{cmdln_parser.command_from(arg_list_with_positional_parameter)};
	REQUIRE(command != nullptr);
	REQUIRE(command->is_anonymous_command());
	REQUIRE(command->argument_by_position(0).name() == "z");
}

TEST_CASE("Command line must not contain unknown command.", "[Command_Line_Parser]")
{
	const auto cmdln_parser = some_command_line_parser();
	const vector<string> arg_list{"unknown command"};
	REQUIRE_THROWS_MATCHES(cmdln_parser.command_from(arg_list), runtime_error,
	                       Message("Unknown command 'unknown command'. Possible commands are: 'help', 'server', "
	                               "'version'."));
}

TEST_CASE("Help command prints usage message.", "[Command_Line_Parser]")
{
	const auto cmdln_parser = some_command_line_parser();
	const vector<string> arg_list{"help"};
	const auto command = cmdln_parser.command_from(arg_list);
	istringstream in;
	ostringstream out;
	ostringstream err;
	(*command)(Execution_Context{*command, cmdln_parser}, in, out, err);
	REQUIRE(out.str() ==
	        "A cool database.\n"
	        "\n"
	        "AVAILABLE COMMANDS\n"
	        "\n"
	        "\tHELP - This short help message.\n\n"
	        "\t\tdmine help\n"
	        "\n"
	        "\tSERVER - Starts a server.\n"
	        "\n"
	        "\t\tdmine server [--cleandb <cleandb>] [--lang <lang>] --port <port>\n"
	        "\n"
	        "\t\t--cleandb <cleandb>: Delete database files.\n"
	        "\t\t                     Optional, named parameter.\n"
	        "\t\t                     Defaults to 0.\n"
	        "\t\t                     Where <cleandb> has to be one of '0', '1', 'false', 'no', 'true', 'yes'.\n"
	        "\n"
	        "\t\t--lang <lang>: Use Lisp or Miner Script query language.\n"
	        "\t\t               Optional, named parameter.\n"
	        "\t\t               Defaults to 'miner'.\n"
	        "\t\t               Where <lang> has to be one of 'lisp', 'miner'.\n"
	        "\n"
	        "\t\t--port <port>: Port to listen for client connections.\n"
	        "\t\t               Mandatory, named parameter.\n"
	        "\t\t               Where <port> has to be in [0, 65535].\n"
	        "\n"
	        "\tVERSION - Version information.\n"
	        "\n"
	        "\t\tdmine version\n"
	        "\n");
}

TEST_CASE("Dashes cannot be omitted for arguments.", "[Command_Line_Parser]")
{
	const auto cmdln_parser = some_command_line_parser();
	const vector<string> arg_list{"server", "port"};
	REQUIRE_THROWS_MATCHES(cmdln_parser.command_from(arg_list), runtime_error,
	                       Message("Mandatory, named parameter '--port' missing for command 'server'."));
}

TEST_CASE("Unknown parameter results in error.", "[Command_Line_Parser]")
{
	const auto cmdln_parser = some_command_line_parser();
	const vector<string> arg_list{"server", "--poort"};
	REQUIRE_THROWS_MATCHES(
			cmdln_parser.command_from(arg_list), runtime_error,
			Message("Failed to process command line arguments of command 'server': There is no parameter "
	                        "with name '--poort'."));
}

TEST_CASE("Missing mandatory argument value results in error.", "[Command_Line_Parser]")
{
	const auto cmdln_parser = some_command_line_parser();
	const vector<string> arg_list{"server", "--port"};
	REQUIRE_THROWS_MATCHES(cmdln_parser.command_from(arg_list), runtime_error,
	                       Message("Failed to process command line arguments of command 'server': Named parameter "
	                               "'--port' "
	                               "is mandatory, but no value given."));
}

TEST_CASE("Missing optional boolean argument value is replaced by default value.", "[Command_Line_Parser]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"exe"};
	// clang-format off
        cmdln_parser_builder
            .version("4711")
            .command("cmd")
                .description("cmd descr")
                .optional()
                    .flag_param("flag")
                    .description("flag descr")
                    .defaults_to(false)
            .build()
            .action([](const auto &context, auto &, auto &, auto &) {
                    const auto &l_flag = context.command.argument_by_name("flag");
                    REQUIRE_FALSE(static_cast<const Flag_Command_Line_Argument &>(l_flag).value());
            });
	// clang-format on
	auto cmdln_parser = cmdln_parser_builder.description("exe descr");
	const vector<string> arg_list{"cmd"};
	const auto command = cmdln_parser.command_from(arg_list);
	(*command)(Execution_Context{*command, cmdln_parser}, cin, cout, cerr);
}

TEST_CASE("Missing optional integer argument value is replaced by default value.", "[Command_Line_Parser]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"exe"};
	// clang-format off
        cmdln_parser_builder
            .version("4711")
            .command("cmd")
                .description("cmd descr")
                .optional()
                    .int_param("int")
                    .description("int descr")
                    .defaults_to(123)
            .build()
            .action([](const auto &context, auto &, auto &, auto &) {
                    const auto &l_int = context.command.argument_by_name("int");
                    REQUIRE(static_cast<const Integer_Command_Line_Argument &>(l_int).value()==123);
            });
	// clang-format on
	auto cmdln_parser = cmdln_parser_builder.description("exe descr");
	const vector<string> arg_list{"cmd"};
	const auto command = cmdln_parser.command_from(arg_list);
	(*command)(Execution_Context{*command, cmdln_parser}, cin, cout, cerr);
}

TEST_CASE("Missing optional text argument value is replaced by default value.", "[Command_Line_Parser]")
{
	Command_Line_Parser_Builder cmdln_parser_builder{"exe"};
	// clang-format off
        cmdln_parser_builder.version("4711")
            .command("cmd")
                .description("cmd descr")
                    .optional()
                        .text_param("text")
                        .description("text descr")
                        .defaults_to("zzz")
            .build()
            .action([](const auto &context, auto &, auto &, auto &) {
                    const auto &text = context.command.argument_by_name("text");
                    REQUIRE(static_cast<const Text_Command_Line_Argument &>(text).value()=="zzz");
            });
	// clang-format on
	auto cmdln_parser = cmdln_parser_builder.description("exe descr");
	const vector<string> arg_list{"cmd"};
	const auto command = cmdln_parser.command_from(arg_list);
	(*command)(Execution_Context{*command, cmdln_parser}, cin, cout, cerr);
}
