/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include "yacl/text_cmdln_arg.hpp"
#include "yacl/text_val_extracting_visitor.hpp"
#include <catch2/catch_all.hpp>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Cannot extract int value from text argument.", "[Text_Value_Extracting_Visitor]")
{
	const Integer_Command_Line_Argument cmdln_arg{"some name", 123};
	Text_Value_Extracting_Visitor visitor;
	REQUIRE_THROWS_MATCHES(cmdln_arg.accept(visitor), runtime_error,
	                       Message("Failed to access value of given argument. "
	                               "Parameter 'some name' is an integer parameter not a text parameter."));
}

TEST_CASE("Cannot extract flag value from text argument.", "[Text_Value_Extracting_Visitor]")
{
	const Flag_Command_Line_Argument cmdln_arg{"some name", true};
	Text_Value_Extracting_Visitor visitor;
	REQUIRE_THROWS_MATCHES(cmdln_arg.accept(visitor), runtime_error,
	                       Message("Failed to access value of given argument. "
	                               "Parameter 'some name' is a flag parameter not a text parameter."));
}

TEST_CASE("Text value can be extracted from text argument.", "[Text_Value_Extracting_Visitor]")
{
	const Text_Command_Line_Argument cmdln_arg{"some name", "some value"};
	Text_Value_Extracting_Visitor visitor;
	REQUIRE(visitor.value == "");
	cmdln_arg.accept(visitor);
	REQUIRE(visitor.value == "some value");
}
