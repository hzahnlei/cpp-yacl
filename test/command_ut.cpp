/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/command.hpp"
#include <catch2/catch_all.hpp>
#include <stdexcept>

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Cannot query argument from command if command does not know that argument.", "[Command]")
{
	Command command{"some command", [](const auto &, auto &, auto &, auto &) {},
	                Command::Named_Command_Line_Arguments{}, Command::Positional_Command_Line_Arguments{}};
	REQUIRE_THROWS_MATCHES(command.argument_by_name("x"), runtime_error,
	                       Message("Argument 'x' not defined for command 'some command'."));
}
