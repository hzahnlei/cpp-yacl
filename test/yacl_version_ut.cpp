/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/yacl.hpp"
#include <catch2/catch_all.hpp>

using Catch::Matchers::Message;

TEST_CASE("Library version.", "[fast]")
{
	REQUIRE(yacl::VERSION_MAJOR == 1);
	REQUIRE(yacl::VERSION_MINOR == 8);
	REQUIRE(yacl::VERSION_PATCH == 0);
	REQUIRE(yacl::VERSION_EXTENSION == "");
}
