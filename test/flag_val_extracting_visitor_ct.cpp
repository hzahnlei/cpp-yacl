/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include <catch2/catch_all.hpp>
#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/flag_val_extracting_visitor.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include "yacl/text_cmdln_arg.hpp"

using Catch::Matchers::Message;

using namespace yacl;

TEST_CASE("Cannot extract int value from flag argument.", "[Flag_Value_Extracting_Visitor]")
{
        const Integer_Command_Line_Argument cmdln_arg{"some name", 123};
        Flag_Value_Extracting_Visitor visitor;
        REQUIRE_THROWS_MATCHES(cmdln_arg.accept(visitor), runtime_error,
                               Message("Failed to access value of given argument. "
                                       "Parameter 'some name' is an integer parameter not a flag parameter."));
}

TEST_CASE("Flag value can be extracted from flag argument.", "[Flag_Value_Extracting_Visitor]")
{
        const Flag_Command_Line_Argument cmdln_arg{"some name", true};
        Flag_Value_Extracting_Visitor visitor;
        REQUIRE_FALSE(visitor.value);
        cmdln_arg.accept(visitor);
        REQUIRE(visitor.value);
}

TEST_CASE("Cannot extract text value from flag argument.", "[Flag_Value_Extracting_Visitor]")
{
        const Text_Command_Line_Argument cmdln_arg{"some name", "some value"};
        Flag_Value_Extracting_Visitor visitor;
        REQUIRE_THROWS_MATCHES(cmdln_arg.accept(visitor), runtime_error,
                               Message("Failed to access value of given argument. "
                                       "Parameter 'some name' is a text parameter not a flag parameter."));
}
