# *****************************************************************************
#
# cpp-conan-cmake-templates - Templates for  modern C++ projects with dependen-
# cies managed by Conan, and Make/CMake used as the build tool.
#
# (c) 2019, 2023 Holger Zahnleiter, All rights reserved
#
# *****************************************************************************

import os
from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout, CMakeDeps
from conan.tools.files import copy


class YaclRecipe(ConanFile):
    name = "yacl"
    version = "1.8.0"
    package_type = "library"

    # Optional metadata
    license = "MIT"
    author = "Holger Zahnleiter opensource@holger.zahnleiter.org"
    url = "https://gitlab.com/hzahnlei/cpp-yacl"
    description = "Yet another C++ library for handling command line arguments."
    topics = ("c++", "library", "command line", "argument", "utility")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "models/*"

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "include"),
            dst=os.path.join(self.package_folder, "include"),
        )
        copy(
            self,
            pattern="*.hpp",
            src=os.path.join(self.source_folder, "generated"),
            dst=os.path.join(self.package_folder, "include"),
        )
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["yacl"]

    def requirements(self):
        self.requires("junkbox/1.7.3", transitive_headers=True)
