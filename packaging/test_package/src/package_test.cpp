/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include <catch2/catch_all.hpp>
#include <junkbox/junkbox.hpp> // Access to trasitive dependency
#include <yacl/yacl.hpp>

/**
 * @brief A view random samples to see whether the library can be included and linked.
 */
TEST_CASE("Verify package version", "[package-test]")
{
	REQUIRE(yacl::VERSION_MAJOR == 1);
	REQUIRE(yacl::VERSION_MINOR == 8);
	REQUIRE(yacl::VERSION_PATCH == 0);
	REQUIRE(yacl::VERSION_EXTENSION == "");
}

/**
 * @brief Access to trasitive dependency
 */
TEST_CASE("Access to trasitive dependency", "[package-test]")
{
	REQUIRE(junkbox::text::single_quoted("Hello, World!") == "'Hello, World!'");
	REQUIRE(junkbox::text::double_quoted("Hello, World!") == "\"Hello, World!\"");
}
