/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <optional>
#include <stdexcept>
#include <string>

namespace yacl
{

        using std::optional;
        using std::runtime_error;
        using std::string;

        class Unknown_License final : public runtime_error
        {
                using runtime_error::runtime_error;
        };

        enum class License
        {
                APACHE2,
                MIT
        };

        [[nodiscard]] auto license_text(const License license) -> const string &;

        [[nodiscard]] auto license_id_from(const string &textual_rep) -> optional<License>;

        [[nodiscard]] auto textual_representation(const License license) -> string;

} // namespace yacl
