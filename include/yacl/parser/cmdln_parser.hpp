/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/command.hpp"
#include "yacl/command_action.hpp"
#include "yacl/parser/cmd_parser.hpp"
#include "yacl/parser/license.hpp"
#include <map>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace yacl
{

	using std::nullopt;

	class Command_Line_Parser_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	class Command_Line_Parser final
	{

	    public:
		using Map_Of_Command_Parsers = map<string, Command_Parser>;
		using Command_Parser_Iter = Map_Of_Command_Parsers::const_iterator;

	    private:
		const string m_program_name;
		const string m_banner;
		const string m_description;
		const string m_version;
		// Not const to make it movable to application.
		Map_Of_Command_Parsers m_command_parsers;
		optional<Command_Parser> m_anonymous_command = nullopt;

		vector<string> m_authors;
		vector<string> m_oss_used;
		optional<string> m_copyright;
		optional<License> m_license;
		optional<string> m_disclaimer;

	    public:
		Command_Line_Parser(const string &program_name, const string &banner, const string &description,
		                    const string &version, Map_Of_Command_Parsers &&,
		                    optional<Command_Parser> = nullopt, vector<string> &&p_authors = {},
		                    vector<string> &&p_oss = {}, optional<string> &&copyright = nullopt,
		                    optional<License> && = nullopt, optional<string> &&disclaimer = nullopt);
		Command_Line_Parser(Command_Line_Parser &&) = default;
		~Command_Line_Parser() = default;

		[[nodiscard]] auto command_from(const Command_Parser::List_Of_Cmd_Ln_Args &) const
				-> unique_ptr<Command>;

		[[nodiscard]] auto program_name() const noexcept -> const string &;
		[[nodiscard]] auto banner() const noexcept -> const string &;
		[[nodiscard]] auto has_banner() const -> bool;
		[[nodiscard]] auto description() const noexcept -> const string &;
		[[nodiscard]] auto version() const noexcept -> const string &;

		[[nodiscard]] auto copyright() const noexcept -> const optional<string> &;
		[[nodiscard]] auto license() const noexcept -> const optional<License> &;
		[[nodiscard]] auto disclaimer() const noexcept -> const optional<string> &;
		[[nodiscard]] auto authors() const noexcept -> const vector<string> &;
		[[nodiscard]] auto open_source_software_used() const noexcept -> const vector<string> &;

		[[nodiscard]] auto command_parser(const string &) const -> const Command_Parser &;
		[[nodiscard]] auto has_anonymous_command() const noexcept -> bool;
		[[nodiscard]] auto anonymous_command() const -> const Command_Parser &;
		[[nodiscard]] [[nodiscard]] auto begin() const -> Command_Parser_Iter;
		auto end() const -> Command_Parser_Iter;

	    private:
		[[nodiscard]] auto missing_command_error() const -> unique_ptr<Command>;
		[[nodiscard]] auto command_by_name(const Command_Parser::List_Of_Cmd_Ln_Args &) const
				-> unique_ptr<Command>;
		[[nodiscard]] auto anonymous_command(const Command_Parser::List_Of_Cmd_Ln_Args &arg_list) const
				-> unique_ptr<Command>;
		[[nodiscard]] auto command_unknown_error(const string &) const -> unique_ptr<Command>;
	};

	inline auto Command_Line_Parser::has_anonymous_command() const noexcept -> bool
	{
		return m_anonymous_command.has_value();
	}

	inline auto Command_Line_Parser::anonymous_command() const -> const Command_Parser &
	{
		return *m_anonymous_command;
	}

} // namespace yacl
