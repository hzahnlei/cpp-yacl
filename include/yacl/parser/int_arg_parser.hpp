/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_val_arg_parser.hpp"

namespace yacl
{

	class Integer_Argument_Parser final : public Abstract_Value_Argument_Parser<int>
	{

	    private:
		const int m_min;
		const int m_max;

	    public:
		Integer_Argument_Parser(const string &name, const string &description, const param::Identifiability,
		                        const int min_value, const int max_value, const optional<int> &default_val);
		~Integer_Argument_Parser() override = default;

		[[nodiscard]] auto min() const noexcept -> int;
		[[nodiscard]] auto max() const noexcept -> int;

		auto accept(Argument_Parser_Visitor &) const -> void override;

	    private:
		auto ensure_constraints(const int) const -> void override;
		[[nodiscard]] auto unchecked_value_from(const string &) const -> int override;
	};

} // namespace yacl
