/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

namespace yacl::param
{

        enum class Requiredness : bool
        {
                MANDATORY,
                OPTIONAL
        };

} // namespace yacl::param
