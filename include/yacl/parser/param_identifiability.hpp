/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

namespace yacl::param
{

	enum class Identifiability : bool
	{
		BY_POSITION,
		BY_NAME
	};

} // namespace yacl::param
