/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/abstract_cmdln_arg.hpp"
#include "yacl/parser/arg_parser_visitor.hpp"
#include <memory>

namespace yacl
{

	using std::unique_ptr;

	class Argument_Producing_Visitor final : public Argument_Parser_Visitor
	{

	    public:
		string textual_representation = "";
		unique_ptr<Abstract_Command_Line_Argument> argument = nullptr;

		auto visit(const Integer_Argument_Parser &) -> void override;
		auto visit(const Flag_Argument_Parser &) -> void override;
		auto visit(const Text_Argument_Parser &) -> void override;
	};

} // namespace yacl
