/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/parser/flag_arg_parser.hpp"
#include "yacl/parser/int_arg_parser.hpp"
#include "yacl/parser/text_arg_parser.hpp"

namespace yacl
{

	class Argument_Parser_Visitor
	{

	    public:
		Argument_Parser_Visitor() = default;
		virtual ~Argument_Parser_Visitor() = default;

		virtual auto visit(const Integer_Argument_Parser &) -> void = 0;
		virtual auto visit(const Flag_Argument_Parser &) -> void = 0;
		virtual auto visit(const Text_Argument_Parser &) -> void = 0;
	};

} // namespace yacl
