/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_val_arg_parser.hpp"
#include <string>
#include <vector>

namespace yacl
{

	using std::string;
	using std::vector;

	class Text_Argument_Parser final : public Abstract_Value_Argument_Parser<string>
	{

	    private:
		const bool m_allow_empty;
		const bool m_is_white_space_allowed;
		const vector<string> m_allowed_values;

	    public:
		Text_Argument_Parser(const string &name, const string &description, const param::Identifiability,
		                     const bool allow_empty, const bool allow_w_space, vector<string> &&allowed_vals,
		                     const optional<string> &default_val);
		~Text_Argument_Parser() override = default;

		[[nodiscard]] auto empty_text_is_allowed() const noexcept -> bool;
		[[nodiscard]] auto white_space_is_allowed() const noexcept -> bool;
		[[nodiscard]] auto one_of() const noexcept -> const vector<string> &;
		auto accept(Argument_Parser_Visitor &) const -> void override;

	    private:
		auto ensure_constraints(const string) const -> void override;
		[[nodiscard]] auto unchecked_value_from(const string &) const -> string override;
	};

} // namespace yacl
