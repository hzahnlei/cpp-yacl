/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_val_arg_parser.hpp"
#include <map>
#include <string>

namespace yacl
{

	using std::map;

	class Flag_Argument_Parser final : public Abstract_Value_Argument_Parser<bool>
	{

	    public:
		using Text_to_Bool = map<string, bool>;
		static const Text_to_Bool TEXT_TO_BOOL;

		Flag_Argument_Parser(const string &name, const string &description, const param::Identifiability,
		                     const bool default_val);
		Flag_Argument_Parser(const string &name, const string &description, const param::Identifiability);
		~Flag_Argument_Parser() override = default;

		auto accept(Argument_Parser_Visitor &) const -> void override;
		[[nodiscard]] auto valid_flag_values() const noexcept -> const Text_to_Bool &;

	    private:
		auto ensure_constraints(const bool) const -> void override;
		[[nodiscard]] auto unchecked_value_from(const string &) const -> bool override;
	};

	inline auto Flag_Argument_Parser::valid_flag_values() const noexcept -> const Text_to_Bool &
	{
		return TEXT_TO_BOOL;
	}

} // namespace yacl
