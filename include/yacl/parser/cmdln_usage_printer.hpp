/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/parser/cmdln_parser.hpp"
#include <ostream>

namespace yacl
{

        using std::ostream;

        auto operator<<(ostream &textual_rep, const Command_Line_Parser &command_line) -> ostream &;

} // namespace yacl
