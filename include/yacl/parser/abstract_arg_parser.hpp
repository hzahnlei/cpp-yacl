/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/parser/param_identifiability.hpp"
#include <memory>
#include <stdexcept>
#include <string>

namespace yacl
{

	using std::runtime_error;
	using std::string;

	class Argument_Parser_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	class Constraint_Violation final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	// Forward declaration.
	class Argument_Parser_Visitor;

	class Abstract_Argument_Parser
	{

	    protected:
		const string m_parameter_name;
		const string m_description;
		const param::Identifiability m_identifiable_by;

	    public:
		Abstract_Argument_Parser(const string &name, const string &description, const param::Identifiability);
		explicit Abstract_Argument_Parser(Abstract_Argument_Parser &) = delete;
		explicit Abstract_Argument_Parser(const Abstract_Argument_Parser &) = delete;
		explicit Abstract_Argument_Parser(Abstract_Argument_Parser &&) = default;
		virtual ~Abstract_Argument_Parser() = default;

		auto operator=(const Abstract_Argument_Parser &) -> Abstract_Argument_Parser & = delete;
		auto operator=(Abstract_Argument_Parser &&) -> Abstract_Argument_Parser & = delete;

		[[nodiscard]] auto name() const noexcept -> const string &;
		[[nodiscard]] virtual auto is_mandatory() const -> bool = 0;
		[[nodiscard]] auto is_positional() const -> bool;
		[[nodiscard]] auto description() const noexcept -> const string &;

		virtual auto accept(Argument_Parser_Visitor &) const -> void = 0;
	};

} // namespace yacl
