/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/command.hpp"
#include "yacl/command_action.hpp"
#include "yacl/parser/abstract_arg_parser.hpp"
#include "yacl/parser/arg_producing_visitor.hpp"
#include "yacl/parser/default_arg_producing_visitor.hpp"
#include <algorithm>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace yacl
{

	using std::cbegin;
	using std::cend;
	using std::for_each;

	class Command_Parser_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	class Command_Parser final
	{

	    public:
		using Named_Argument_Parsers = map<string, unique_ptr<Abstract_Argument_Parser>>;
		using Argument_Parser_Iter = Named_Argument_Parsers::const_iterator;

		using Positional_Argument_Parsers = vector<unique_ptr<Abstract_Argument_Parser>>;
		using Positional_Argument_Parser_Iter = Positional_Argument_Parsers::const_iterator;

		using List_Of_Cmd_Ln_Args = vector<string>;
		using List_Of_Cmd_Ln_Args_Iter = List_Of_Cmd_Ln_Args::const_iterator;

	    private:
		const string m_name;
		const string m_description;
		const Command_Action m_action;
		Named_Argument_Parsers m_named_parameters;
		Positional_Argument_Parsers m_positional_parameters;
		mutable Default_Argument_Producing_Visitor m_optional_argument_value_producer;
		mutable Argument_Producing_Visitor m_argument_value_producer;

	    public:
		Command_Parser(const string &description, const Command_Action &, Named_Argument_Parsers &&,
		               Positional_Argument_Parsers &&);
		Command_Parser(const string &name, const string &description, const Command_Action &,
		               Named_Argument_Parsers &&, Positional_Argument_Parsers &&);
		explicit Command_Parser(Command_Parser &) = delete;
		explicit Command_Parser(const Command_Parser &) = delete;
		explicit Command_Parser(Command_Parser &&) = default;
		~Command_Parser() = default;

		auto operator=(const Command_Parser &) -> Command_Parser & = delete;
		auto operator=(Command_Parser &&) -> Command_Parser & = delete;

		[[nodiscard]] auto command_from(List_Of_Cmd_Ln_Args_Iter &curr_arg,
		                                const List_Of_Cmd_Ln_Args_Iter &last_arg) const -> unique_ptr<Command>;

		[[nodiscard]] auto name() const noexcept -> const string &;
		[[nodiscard]] auto is_anonymous_command() const noexcept -> bool;
		[[nodiscard]] auto description() const noexcept -> const string &;
		[[nodiscard]] auto has_parameter(const string &) const -> bool;
		[[nodiscard]] auto parameter(const string &) const -> const Abstract_Argument_Parser &;
		template <class FUN> auto for_each_named_parameter(const FUN f) const
		{
			for_each(cbegin(m_named_parameters), cend(m_named_parameters),
			         [&f](const auto &param) { f(*param.second); });
		}
		[[nodiscard]] inline auto named_parameter_count() const noexcept
		{
			return m_named_parameters.size();
		}
		template <class FUN> auto for_each_positional_parameter(const FUN f) const
		{
			for_each(cbegin(m_positional_parameters), cend(m_positional_parameters),
			         [&f](const auto &param) { f(*param); });
		}
		[[nodiscard]] inline auto positional_parameter_count() const noexcept
		{
			return m_positional_parameters.size();
		}

	    private:
		auto evaluated_named_arguments(List_Of_Cmd_Ln_Args_Iter &curr_arg,
		                               const List_Of_Cmd_Ln_Args_Iter &last_arg) const
				-> Command::Named_Command_Line_Arguments;
		auto evaluated_positional_arguments(List_Of_Cmd_Ln_Args_Iter &curr_arg,
		                                    const List_Of_Cmd_Ln_Args_Iter &last_arg) const
				-> Command::Positional_Command_Line_Arguments;

		auto ensure_all_mandatory_parameters_have_a_value(const Command::Named_Command_Line_Arguments &) const
				-> void;
		auto
		ensure_all_mandatory_parameters_have_a_value(const Command::Positional_Command_Line_Arguments &) const
				-> void;

		[[nodiscard]] auto default_value_from(const Abstract_Argument_Parser &) const
				-> unique_ptr<Abstract_Command_Line_Argument>;
		auto populate_missing_optionals_with_default_values(Command::Named_Command_Line_Arguments &) const
				-> void;
		auto populate_missing_optionals_with_default_values(
				Command::Positional_Command_Line_Arguments &actual_args) const -> void;

		[[nodiscard]] auto descriptive_command_name() const -> string;
		[[nodiscard]] auto command_line_parsing_error(const string &cause) const -> Command_Parser_Error;
	};

	[[nodiscard]] inline auto Command_Parser::command_line_parsing_error(const string &cause) const
			-> Command_Parser_Error
	{
		return Command_Parser_Error{"Failed to process command line arguments of " +
		                            descriptive_command_name() + ": " + cause};
	}

} // namespace yacl
