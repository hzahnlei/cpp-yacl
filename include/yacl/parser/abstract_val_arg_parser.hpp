/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/parser/abstract_arg_parser.hpp"
#include <junkbox/text.hpp>
#include <optional>

namespace yacl
{

	using std::optional;

	template <typename T> class Abstract_Value_Argument_Parser : public Abstract_Argument_Parser
	{

	    protected:
		const optional<const T> m_default_value;

	    public:
		Abstract_Value_Argument_Parser(const string &name, const string &description,
		                               const param::Identifiability identifiable_by,
		                               const optional<T> &default_val)
				: Abstract_Argument_Parser{name, description, identifiable_by},
				  m_default_value{default_val}
		{
			// Descendants have to make sure that the default value does not
			// violate the given constraints.
		}

		// I need this for flag (bool) parameters! Seems like this class'
		// default value will always be true if the defaults-to-parameter
		// holds any boolean value no matter if true or false.
		Abstract_Value_Argument_Parser(const string &name, const string &description,
		                               const param::Identifiability identifiable_by, const T default_val)
				: Abstract_Argument_Parser{name, description, identifiable_by},
				  m_default_value{default_val}
		{
			// Descendants have to make sure that the default value does not
			// violate the given constraints.
		}

	    protected:
		// I need this for flag (bool) parameters! If default_val has no
		// value, THEN still m_default_value gets assigned a value of false.
		// However, the first ctor works for int and string.
		Abstract_Value_Argument_Parser(const string &name, const string &description,
		                               const param::Identifiability identifiable_by)
				: Abstract_Argument_Parser{name, description, identifiable_by}
		{
		}

	    public:
		~Abstract_Value_Argument_Parser() override = default;

		[[nodiscard]] auto is_mandatory() const -> bool override
		{
			return !m_default_value.has_value();
		}

		[[nodiscard]] auto defaults_to() const noexcept -> const optional<const T> &
		{
			return m_default_value;
		}

		[[nodiscard]] auto value_from(const string &textual_rep) const -> T
		{
			const auto value = unchecked_value_from(textual_rep);
			ensure_constraints(value);
			return value;
		}

	    protected:
		[[nodiscard]] virtual auto unchecked_value_from(const string &) const -> T = 0;

		virtual auto ensure_constraints(const T) const -> void = 0;

		auto ensure_constraints_for_default(const T value) const -> void
		{
			try
			{
				ensure_constraints(value);
			}
			catch (const Constraint_Violation &cause)
			{
				throw Argument_Parser_Error{"Default value is invalid. " + string{cause.what()}};
			}
		}
	};

} // namespace yacl
