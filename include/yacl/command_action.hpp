/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/execution_context.hpp"
#include <functional>

namespace yacl
{

        using std::function;
        using std::istream;
        using std::ostream;

        using Command_Action = function<void(const Execution_Context &, istream &, ostream &, ostream &)>;

} // namespace yacl
