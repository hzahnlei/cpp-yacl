/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/abstract_cmdln_arg.hpp"

namespace yacl
{

        class Integer_Command_Line_Argument final : public Abstract_Command_Line_Argument
        {

            private:
                const int m_value;

            public:
                Integer_Command_Line_Argument(const string &, const int);
                ~Integer_Command_Line_Argument() override = default;

                auto accept(Command_Line_Argument_Visitor &) const -> void override;
                auto value() const noexcept -> int;
        };

} // namespace yacl
