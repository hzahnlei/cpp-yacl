/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include <string>

namespace yacl
{
        using std::string;

        // Forward declaration
        class Command_Line_Argument_Visitor;

        class Abstract_Command_Line_Argument
        {

            private:
                const string m_parameter_name;

            public:
                explicit Abstract_Command_Line_Argument(const string &);
                virtual ~Abstract_Command_Line_Argument() = default;

                [[nodiscard]] auto name() const noexcept -> const string &;
                virtual auto accept(Command_Line_Argument_Visitor &) const -> void = 0;
        };

} // namespace yacl
