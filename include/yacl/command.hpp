/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/abstract_cmdln_arg.hpp"
#include "yacl/command_action.hpp"
#include "yacl/execution_context.hpp"
#include <cstddef>
#include <istream>
#include <map>
#include <memory>
#include <ostream>
#include <string>
#include <vector>

namespace yacl
{

	using std::map;
	using std::runtime_error;
	using std::size_t;
	using std::string;
	using std::unique_ptr;
	using std::vector;

	class Command_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	class Command final
	{

	    public:
		using Named_Command_Line_Arguments = map<string, unique_ptr<Abstract_Command_Line_Argument>>;
		using Positional_Command_Line_Arguments = vector<unique_ptr<Abstract_Command_Line_Argument>>;
		using Position = size_t;

	    private:
		const string m_command_name;
		const Command_Action m_action;
		const Named_Command_Line_Arguments m_arguments;
		const Positional_Command_Line_Arguments m_positional_arguments;

	    public:
		explicit Command(const string &, const Command_Action &, Named_Command_Line_Arguments &&,
		                 Positional_Command_Line_Arguments &&);
		~Command() = default;

		auto operator()(const Execution_Context &, istream &in, ostream &out, ostream &err) const -> void;
		[[nodiscard]] auto name() const noexcept -> const string &;
		[[nodiscard]] auto is_anonymous_command() const noexcept -> bool;
		[[nodiscard]] auto argument_by_name(const string &) const -> const Abstract_Command_Line_Argument &;
		[[nodiscard]] auto argument_by_position(const Position) const -> const Abstract_Command_Line_Argument &;
	};

	inline auto Command::name() const noexcept -> const string &
	{
		return m_command_name;
	}

	inline auto Command::is_anonymous_command() const noexcept -> bool
	{
		return m_command_name == "";
	}

} // namespace yacl
