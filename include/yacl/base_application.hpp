/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/execution_context.hpp"
#include <atomic>
#include <ostream>
#include <vector>

namespace yacl
{

        using std::atomic_bool;
        using std::istream;
        using std::ostream;

#define NUMBER_OF_ARGS(array) (sizeof array / sizeof *array)

        [[nodiscard]] auto program_name(const int arg_count, const char *arg_list[]) -> string;

        using List_Of_Arguments = vector<string>;
        [[nodiscard]] auto list_of_arguments(const int arg_count, const char *arg_list[]) -> List_Of_Arguments;

        class Base_Application
        {

            private:
                const Command_Line_Parser m_command_line_parser;
                mutable Integer_Value_Extracting_Visitor m_integer_value_extractor;
                mutable Flag_Value_Extracting_Visitor m_flag_value_extractor;
                mutable Text_Value_Extracting_Visitor m_text_value_extractor;
                mutable atomic_bool m_is_running = false;
                mutable atomic_bool m_request_stop = false;

            public:
                explicit Base_Application(Command_Line_Parser &&m_command_line_parser);
                virtual ~Base_Application() = default;

                auto run(const List_Of_Arguments &, istream &in, ostream &out, ostream &err) -> void;
                [[nodiscard]] auto is_running() const noexcept -> bool;

                auto stop() -> void;
                [[nodiscard]] auto is_stop_requested() const -> bool;
        };

} // namespace yacl
