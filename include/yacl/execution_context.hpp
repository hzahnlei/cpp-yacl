/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/flag_val_extracting_visitor.hpp"
#include "yacl/int_val_extracting_visitor.hpp"
#include "yacl/text_val_extracting_visitor.hpp"
#include <cstddef>
#include <string>

namespace yacl
{

	// Forward declarations
	class Command;
	class Command_Line_Parser;

	using std::string;

	class Execution_Context final
	{
	    public:
		const Command &command;
		const Command_Line_Parser &command_line_parser;
		using Position = std::size_t;
		using Name = std::string;

	    private:
		mutable Integer_Value_Extracting_Visitor m_integer_value_extractor;
		mutable Flag_Value_Extracting_Visitor m_flag_value_extractor;
		mutable Text_Value_Extracting_Visitor m_text_value_extractor;

	    public:
		Execution_Context(const Command &, const Command_Line_Parser &);

		auto integer_value_of(const Name &) const -> int;
		auto flag_value_of(const Name &) const -> bool;
		auto text_value_of(const Name &) const -> string;

		auto integer_value_at(const Position) const -> int;
		auto flag_value_at(const Position) const -> bool;
		auto text_value_at(const Position) const -> string;

	    private:
		auto argument_by_name(const string &) const -> const Abstract_Command_Line_Argument &;
		auto argument_by_position(const Position) const -> const Abstract_Command_Line_Argument &;
	};

} // namespace yacl
