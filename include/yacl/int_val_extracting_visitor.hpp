/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/cmdln_arg_visitor.hpp"

namespace yacl
{

        class Integer_Value_Extracting_Visitor final : public Command_Line_Argument_Visitor
        {

            public:
                int value = 0;

                ~Integer_Value_Extracting_Visitor() override = default;

                auto visit(const Integer_Command_Line_Argument &) -> void override;
                auto visit(const Flag_Command_Line_Argument &) -> void override;
                auto visit(const Text_Command_Line_Argument &) -> void override;
        };

} // namespace yacl
