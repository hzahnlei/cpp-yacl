/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/base_application.hpp"
#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/flag_val_extracting_visitor.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include "yacl/int_val_extracting_visitor.hpp"
#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/text_cmdln_arg.hpp"
#include "yacl/text_val_extracting_visitor.hpp"
#include "yacl/version.hpp"

/**
 * @brief Yet another C++ library for handling command line arguments.
 *
 * @details
 * # Motivation
 *
 * I needed a framework for processing commandline arguments. It should automatically print user instructions and
 * produce meaningful error messages in case one misspells the command line arguments or uses prohibited values etc.
 *
 * # Solution
 *
 * I wanted to build a small framework for processing commandline arguments myself. This library is intended for console
 * applications that implement a command approach (popularized by Git and others).
 *
 * The library provides builders to assemble commands and their parameters. The intention is to make it easy to
 * understand what commands an applications supports and how to add new commands.
 *
 * # Counterindication
 *
 * For more complex use cases prefer one of the more established and advanced frameworks.
 *
 * # Examples
 *
 * The framework features builders to configure commands and their arguments.
 *
 * ```cpp
 * class My_Application final : public Application {
 *   public:
 *     explicit My_Application(const string &program_name) : Application{command_line_parser(program_name)} {
 *     }
 *
 *  private:
 *    auto command_line_parser(const string &program_name) const -> Command_Line_Parser {
 *        Command_Line_Parser_Builder command_syntax{program_name};
 *        // 1st command
 *        cmd_syntax
 *            .command("start")
 *                .description("Starts the server.")
 *                .mandatory()
 *                    .int_param("port")
 *                    .description("Port to listen on.")
 *                    .min(0)
 *                    .max(65535)
 *                    .action([&](const auto &context, auto &in, auto &out, auto &err) {
 *                        // Start the server...
 *                    });
 *        // 2nd command
 *        cmd_syntax
 *            .command("stop")
 *                .description("Stops the server.")
 *                .optional()
 *                    .flag_param("wait")
 *                    .description("Defer shut-down until all clients disconnected.")
 *                    .defaults_to(false)
 *                    .action([&](const auto &context, auto &in, auto &out, auto &err) {
 *                        // Stop the server...
 *                    });
 *        // Describing the application ends the building process.
 *        // The result is a valid, functional command line parser.
 *        return command_syntax.description("My backend server.");
 *    }
 * };
 * ```
 * It is a good idea to use methods instead of lambdas. I did not do this here to keep the example short.
 * `My_Application` is used in `main` like shown in the most simple example below:
 * ```cpp
 * auto main(int arg_count, const char *arg_list[]) -> int {
 *    // Extract program name, it's the first argument
 *    My_Application application{yacl::program_name(arg_count, arg_list)};
 *    // Extract actual arguments, these are all arguments
 *    application.run(yacl::list_of_arguments(arg_count, arg_list), std::cin, std::cout, std::cerr);
 *    return 0;
 *}
 * ```
 *
 * @author Holger Zahnleiter
 *
 * @copyright MIT License, see repository LICENSE file
 */
namespace yacl
{
} // namespace yacl
