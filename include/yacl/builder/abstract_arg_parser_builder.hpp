/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/builder/cmd_parser_builder.hpp"
#include "yacl/parser/abstract_arg_parser.hpp"
#include "yacl/parser/param_identifiability.hpp"
#include "yacl/parser/param_requiredness.hpp"
#include <memory>
#include <stdexcept>
#include <string>

namespace yacl
{

	using std::runtime_error;
	using std::string;
	using std::unique_ptr;

	class Argument_Parser_Builder_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	class Abstract_Argument_Parser_Builder
	{
	    protected:
		Command_Parser_Builder &m_command_parser_builder;
		const string m_parameter_name;
		const param::Requiredness m_requiredness;
		const param::Identifiability m_identifiable_by;
		string m_description;

	    public:
		Abstract_Argument_Parser_Builder(Command_Parser_Builder &, const string &param_name,
		                                 const param::Requiredness, const param::Identifiability);
		explicit Abstract_Argument_Parser_Builder(Abstract_Argument_Parser_Builder &) = delete;
		explicit Abstract_Argument_Parser_Builder(const Abstract_Argument_Parser_Builder &) = delete;
		explicit Abstract_Argument_Parser_Builder(Abstract_Argument_Parser_Builder &&) = default;
		virtual ~Abstract_Argument_Parser_Builder() = default;

		auto operator=(const Abstract_Argument_Parser_Builder &) -> Abstract_Argument_Parser_Builder & = delete;
		auto operator=(Abstract_Argument_Parser_Builder &&) -> Abstract_Argument_Parser_Builder & = delete;

		virtual auto description(const string &) -> Abstract_Argument_Parser_Builder &;

		// These are mirroring the functions of same name in
		// Command_Parser_Builder. It's a matter of taste. Use one of these
		// below or first call build and THEN
		// Command_Parser_Builder::optional or
		// Command_Parser_Builder::mandatory.
		auto optional() -> Argument_Parser_Builder;
		auto mandatory() -> Argument_Parser_Builder;

		auto action(const Command_Action &action) -> void;

		[[nodiscard]] auto build() -> Command_Parser_Builder &;

		[[nodiscard]] auto is_positional() const noexcept -> bool;

	    protected:
		[[nodiscard]] auto is_supposed_to_be_mandatory() const noexcept -> bool;

		[[nodiscard]] virtual auto has_default_value() const noexcept -> bool = 0;

		[[nodiscard]] auto build_parser() -> unique_ptr<Abstract_Argument_Parser>;
		auto ensure_requiredness() const -> void;
		[[nodiscard]] virtual auto build_parser_with_requiredness_ensured()
				-> unique_ptr<Abstract_Argument_Parser> = 0;
	};

} // namespace yacl
