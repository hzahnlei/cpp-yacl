/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

// #include "yacl/builder/pos_arg_parser_builder.hpp"
#include "yacl/command_action.hpp"
#include "yacl/parser/cmd_parser.hpp"
#include <memory>
#include <string>

namespace yacl
{

	using std::runtime_error;
	using std::string;
	using std::unique_ptr;

	class Command_Parser_Builder_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	// Forward declaration.
	class Argument_Parser_Builder;

	class Command_Parser_Builder
	{

	    public:
		Command_Parser_Builder() = default;
		explicit Command_Parser_Builder(Command_Parser_Builder &) = delete;
		explicit Command_Parser_Builder(const Command_Parser_Builder &) = delete;
		explicit Command_Parser_Builder(Command_Parser_Builder &&) = default;
		virtual ~Command_Parser_Builder() = default;

		auto operator=(const Command_Parser_Builder &) -> Command_Parser_Builder & = delete;
		auto operator=(Command_Parser_Builder &&) -> Command_Parser_Builder & = default;

		virtual auto name() const noexcept -> const string & = 0;
		virtual auto is_anonymous_command() const noexcept -> bool = 0;
		virtual auto description(const string &) -> Command_Parser_Builder & = 0;

		[[nodiscard]] virtual auto optional() -> Argument_Parser_Builder = 0;
		[[nodiscard]] virtual auto mandatory() -> Argument_Parser_Builder = 0;

		virtual auto action(const Command_Action &) -> void = 0;

		virtual auto register_named_parameter(unique_ptr<Abstract_Argument_Parser>) -> void = 0;

		virtual auto register_positional_parameter(unique_ptr<Abstract_Argument_Parser>) -> void = 0;
	};

} // namespace yacl
