/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_arg_parser_builder.hpp"
#include <optional>
#include <string>

namespace yacl
{

	using junkbox::text::contains_whitespace;
	using junkbox::text::single_quoted;
	using std::optional;
	using std::string;

	template <typename T> class Abstract_Value_Argument_Parser_Builder : public Abstract_Argument_Parser_Builder
	{

	    protected:
		std::optional<T> m_default_value;

	    public:
		Abstract_Value_Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder, const string &name,
		                                       const param::Requiredness required,
		                                       const param::Identifiability identifiable_by)
				: Abstract_Argument_Parser_Builder{cmd_parser_builder, name, required, identifiable_by}
		{
			if (contains_whitespace(name))
			{
				throw Argument_Parser_Builder_Error{"Parameter name " + single_quoted(name) +
				                                    " for command " +
				                                    single_quoted(m_command_parser_builder.name()) +
				                                    " must not contain white space."};
			}
		}

		~Abstract_Value_Argument_Parser_Builder() override = default;

	    protected:
		[[nodiscard]] auto has_default_value() const noexcept -> bool final
		{
			return m_default_value.has_value();
		}
	};

} // namespace yacl
