/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder.hpp"
#include "yacl/parser/cmd_parser.hpp"
#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/parser/license.hpp"
#include <map>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace yacl
{

	using std::runtime_error;

	class Command_Line_Parser_Builder_Error final : public runtime_error
	{
		using runtime_error::runtime_error;
	};

	class Command_Line_Parser_Builder final
	{

	    private:
		const string m_program_name;
		const string m_banner;
		string m_version;
		optional<string> m_copyright;
		optional<License> m_license;
		optional<string> m_disclaimer;
		vector<string> m_authors;
		vector<string> m_oss_used;
		Command_Line_Parser::Map_Of_Command_Parsers m_commands;
		optional<Command_Parser> m_anonymous_command = nullopt;
		unique_ptr<Command_Parser_Builder> m_curr_cmd_in_progress;

	    public:
		Command_Line_Parser_Builder(const string &program_name, const string &banner = "");
		~Command_Line_Parser_Builder() = default;

		auto version(const string &) -> Command_Line_Parser_Builder &;
		auto copyright(const string &) -> Command_Line_Parser_Builder &;
		auto license(const License) -> Command_Line_Parser_Builder &;
		auto disclaimer(const string &) -> Command_Line_Parser_Builder &;
		auto author(const string &) -> Command_Line_Parser_Builder &;
		auto open_source_software_used(const string &) -> Command_Line_Parser_Builder &;
		[[nodiscard]] auto command(const string &) -> Command_Parser_Builder &;
		[[nodiscard]] auto has_anonymous_command() const noexcept -> bool;
		[[nodiscard]] auto anonymous_command() -> Command_Parser_Builder &;
		[[nodiscard]] auto description(const string &) -> Command_Line_Parser;

	    private:
		auto register_help_command() -> void;
		auto register_version_command() -> void;

		auto ensure_valid_version(const string &) const -> void;
		auto ensure_valid_copyright(const string &) const -> void;
		auto ensure_valid_license(const License) const -> void;
		auto ensure_valid_disclaimer(const string &) const -> void;
		auto ensure_valid_author(const string &) const -> void;
		auto ensure_valid_oss(const string &) const -> void;
		auto register_command(const string &, Command_Parser &&) -> void;
		auto register_anonymous_command(Command_Parser &&) -> void;
		auto ensure_command_not_registered_yet(const string &) const -> void;
		auto ensure_valid_description(const string &) const -> void;
	};

} // namespace yacl
