/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/builder/cmd_parser_builder.hpp"
#include "yacl/builder/flag_arg_parser_builder.hpp"
#include "yacl/builder/int_arg_parser_builder.hpp"
#include "yacl/builder/text_arg_parser_builder.hpp"

namespace yacl
{

	using std::string;

	class Argument_Parser_Builder final
	{

	    private:
		Command_Parser_Builder &m_command_parser_builder;
		const param::Requiredness m_requiredness;
		param::Identifiability m_identifiable_by = param::Identifiability::BY_NAME;

	    public:
		Argument_Parser_Builder(Command_Parser_Builder &, const param::Requiredness);
		~Argument_Parser_Builder() = default;

		[[nodiscard]] auto positional() -> Argument_Parser_Builder &;
		[[nodiscard]] auto named() -> Argument_Parser_Builder &;
		[[nodiscard]] auto int_param(const string &) -> Integer_Argument_Parser_Builder;
		[[nodiscard]] auto flag_param(const string &) -> Flag_Argument_Parser_Builder;
		[[nodiscard]] auto text_param(const string &) -> Text_Argument_Parser_Builder;
	};

} // namespace yacl
