/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/builder/cmd_parser_builder.hpp"
#include "yacl/parser/cmd_parser.hpp"
#include <functional>
#include <memory>
#include <string>

namespace yacl
{

	using std::function;
	using std::string;
	using std::unique_ptr;

	// Forward declaration.
	class Argument_Parser_Builder;

	class Command_Parser_Builder_Default_Impl final : public Command_Parser_Builder
	{

	    public:
		using Cmd_Parser_Registration_Fun = function<void(const string &cmd_name, Command_Parser &&)>;

	    private:
		const Cmd_Parser_Registration_Fun m_register_command_parser;
		const string m_name;
		string m_description;
		Command_Parser::Named_Argument_Parsers m_named_parameters;
		Command_Parser::Positional_Argument_Parsers m_positional_parameters;

	    public:
		explicit Command_Parser_Builder_Default_Impl(const Cmd_Parser_Registration_Fun &);
		Command_Parser_Builder_Default_Impl(const Cmd_Parser_Registration_Fun &, const string &cmd_name);
		~Command_Parser_Builder_Default_Impl() override = default;

		[[nodiscard]] auto name() const noexcept -> const string & override;
		[[nodiscard]] auto is_anonymous_command() const noexcept -> bool override;
		auto description(const string &) -> Command_Parser_Builder & override;

		[[nodiscard]] auto optional() -> Argument_Parser_Builder override;
		[[nodiscard]] auto mandatory() -> Argument_Parser_Builder override;

		auto action(const Command_Action &) -> void override;

		auto register_named_parameter(unique_ptr<Abstract_Argument_Parser>) -> void override;
		auto register_positional_parameter(unique_ptr<Abstract_Argument_Parser>) -> void override;

	    private:
		[[nodiscard]] auto descriptive_command_name() const -> string;

		auto ensure_valid_description(const string &) const -> void;
		auto ensure_named_parameter_not_registered_yet(const Abstract_Argument_Parser &) const -> void;
		auto ensure_positional_parameter_not_registered_yet(const Abstract_Argument_Parser &) const -> void;
		auto ensure_no_optional_positional_parameters_yet(const Abstract_Argument_Parser &) const -> void;
	};

} // namespace yacl
