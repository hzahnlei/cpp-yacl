/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_ref_val_arg_parser_builder.hpp"
#include <string>
#include <vector>

namespace yacl
{

	class Text_Argument_Parser_Builder final : public Abstract_Reference_Value_Argument_Parser_Builder<string>
	{

	    private:
		bool m_is_empty_text_allowed = false;
		bool m_is_white_space_allowed = false;
		vector<string> m_allowed_values;

	    public:
		Text_Argument_Parser_Builder(Command_Parser_Builder &, const string &, const param::Requiredness,
		                             const param::Identifiability);
		~Text_Argument_Parser_Builder() override = default;

		auto description(const string &) -> Text_Argument_Parser_Builder & override;

		auto allow_empty_text() -> Text_Argument_Parser_Builder &;
		auto allow_white_space() -> Text_Argument_Parser_Builder &;

		template <typename T = string> auto one_of(const T &value) -> Text_Argument_Parser_Builder &
		{
			enforce_constraints_on_choice(value);
			m_allowed_values.emplace_back(value);
			return *this;
		}

		template <typename T = string, typename... T_s>
		auto one_of(const T &value, const T_s &...values) -> Text_Argument_Parser_Builder &
		{
			enforce_constraints_on_choice(value);
			m_allowed_values.emplace_back(value);
			return one_of(values...);
		}

	    private:
		[[nodiscard]] auto build_parser_with_requiredness_ensured()
				-> unique_ptr<Abstract_Argument_Parser> override;
		auto enforce_constraints_on_choice(const string &) const -> void;
	};

} // namespace yacl
