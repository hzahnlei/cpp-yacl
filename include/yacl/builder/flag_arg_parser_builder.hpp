/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_copy_val_arg_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/parser/param_requiredness.hpp"

namespace yacl
{

	class Flag_Argument_Parser_Builder final : public Abstract_Copyable_Value_Argument_Parser_Builder<bool>
	{

	    public:
		Flag_Argument_Parser_Builder(Command_Parser_Builder &, const string &, const param::Requiredness,
		                             const param::Identifiability);
		~Flag_Argument_Parser_Builder() override = default;

		auto description(const string &) -> Flag_Argument_Parser_Builder & override;

	    private:
		[[nodiscard]] auto build_parser_with_requiredness_ensured()
				-> unique_ptr<Abstract_Argument_Parser> override;
	};

} // namespace yacl
