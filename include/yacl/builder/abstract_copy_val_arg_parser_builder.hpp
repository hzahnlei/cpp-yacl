/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "abstract_val_arg_parser_builder.hpp"
#include <string>

namespace yacl
{

	using std::string;

	/*!
	 * This is for parameters that represent a single value that my be copied
	 * when passing as parmeter. Such values are passed by value here (e.g. int
	 * and long).
	 */
	template <typename T>
	class Abstract_Copyable_Value_Argument_Parser_Builder : public Abstract_Value_Argument_Parser_Builder<T>
	{

	    public:
		Abstract_Copyable_Value_Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder,
		                                                const string &name, const param::Requiredness required,
		                                                const param::Identifiability identifiable_by)
				: Abstract_Value_Argument_Parser_Builder<T>{cmd_parser_builder, name, required,
		                                                            identifiable_by}
		{
		}

		~Abstract_Copyable_Value_Argument_Parser_Builder() override = default;

		auto defaults_to(const T value) -> Abstract_Value_Argument_Parser_Builder<T> &
		{
			Abstract_Value_Argument_Parser_Builder<T>::m_default_value = value;
			return *this;
		}
	};

} // namespace yacl
