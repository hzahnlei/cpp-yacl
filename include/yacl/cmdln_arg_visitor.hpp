/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#pragma once

#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include "yacl/text_cmdln_arg.hpp"
#include <stdexcept>

namespace yacl
{

        using std::runtime_error;

        class Command_Line_Argument_Error final : public runtime_error
        {
                using runtime_error::runtime_error;
        };

        class Command_Line_Argument_Visitor
        {

            public:
                Command_Line_Argument_Visitor() = default;
                virtual ~Command_Line_Argument_Visitor() = default;

                virtual auto visit(const Integer_Command_Line_Argument &) -> void = 0;
                virtual auto visit(const Flag_Command_Line_Argument &) -> void = 0;
                virtual auto visit(const Text_Command_Line_Argument &) -> void = 0;
        };

} // namespace yacl
