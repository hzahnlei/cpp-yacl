# CHANGELOG

# 1.8.0 (2023-10-24)

- New concepts of anonymous command
- Positional parameters
- Fixed transitive dependency not visible by consumer
- Package tests now using Catch2 to perform a view random sample test to make sure
  - headers from lib can be included by comsumer
  - consumer can be linked against lib

# 1.7.1 (2023-10-22)

- Fixed version in Doxygen documentation

# 1.7.0 (2023-10-22)

- Upgrading to Conan 2.0
- Using latest version of Junk Box

# 1.1.5 (2023-05-06)

- Using latest version of Junk Box

# 1.1.4 (2023-04-18)

- Updated Junk Box

# 1.1.3 (2023-04-18)

- Updated Junk Box
- Use updated build container image
- Generate Doxygen docs

# 1.1.2 (2023-04-04)

- Updated Junk Box
- Updated Conan and Catch2
- Use updated build container image

# 1.1.1 (2022-04-18)

-  Fixed different versions of Junk Box dependency in `conanfile.txt` and `packaging/conanfile.py`

# 1.1.0 (2022-04-16)

-  Using newer C++20 build image
-  Fixing SonarLint findings
-  Using [[nodiscard]] attribute
-  Push Conan package to GitLab registry

# 1.0.0 (2020-04-04)

-  Initial project set-up.
