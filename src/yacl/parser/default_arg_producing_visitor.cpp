/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/default_arg_producing_visitor.hpp"
#include <junkbox/assertion.hpp>
#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include <junkbox/text.hpp>
#include "yacl/text_cmdln_arg.hpp"
#include <junkbox/todo_marker.hpp>

namespace yacl
{

        using namespace std;
        using namespace junkbox;

        auto Default_Argument_Producing_Visitor::visit(const Integer_Argument_Parser &param) -> void
        {
                DEV_ASSERT(!param.is_mandatory(), "Cannot produce a default value from mandatory parameter " +
                                                                text::single_quoted(param.name()) +
                                                                ". Mandatory parameters do not have a default value.");
                argument = make_unique<Integer_Command_Line_Argument>(param.name(), *(param.defaults_to()));
        }

        auto Default_Argument_Producing_Visitor::visit(const Flag_Argument_Parser &param) -> void
        {
                DEV_ASSERT(!param.is_mandatory(), "Cannot produce a default value from mandatory parameter " +
                                                                text::single_quoted(param.name()) +
                                                                ". Mandatory parameters do not have a default value.");
                argument = make_unique<Flag_Command_Line_Argument>(param.name(), *(param.defaults_to()));
        }

        auto Default_Argument_Producing_Visitor::visit(const Text_Argument_Parser &param) -> void
        {
                DEV_ASSERT(!param.is_mandatory(), "Cannot produce a default value from mandatory parameter " +
                                                                text::single_quoted(param.name()) +
                                                                ". Mandatory parameters do not have a default value.");
                argument = make_unique<Text_Command_Line_Argument>(param.name(), *(param.defaults_to()));
        }

} // namespace yacl
