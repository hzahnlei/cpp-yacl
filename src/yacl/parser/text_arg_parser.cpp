/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/text_arg_parser.hpp"
#include "yacl/parser/arg_parser_visitor.hpp"
#include <junkbox/convenient_algorithms.hpp>
#include <junkbox/text.hpp>

namespace yacl
{

	using junkbox::algo::contains;
	using junkbox::text::contains_whitespace;
	using junkbox::text::single_quoted;

	Text_Argument_Parser::Text_Argument_Parser(const string &name, const string &description,
	                                           const param::Identifiability identifiable_by, const bool allow_empty,
	                                           const bool allow_w_space, vector<string> &&allowed_vals,
	                                           const optional<string> &default_val)
			: Abstract_Value_Argument_Parser{name, description, identifiable_by, default_val},
			  m_allow_empty{allow_empty}, m_is_white_space_allowed{allow_w_space},
			  m_allowed_values{move(allowed_vals)}
	{
		if (m_default_value.has_value())
		{
			ensure_constraints_for_default(*m_default_value);
		}
		// I am only checking the default value, because I cannot reliably check when constructing the builder.
		// However, I do not check if the choices respect the constraints. This is already ensured reliably by
		// the builder.
	}

	auto Text_Argument_Parser::empty_text_is_allowed() const noexcept -> bool
	{
		return m_allow_empty;
	}

	auto Text_Argument_Parser::white_space_is_allowed() const noexcept -> bool
	{
		return m_is_white_space_allowed;
	}

	auto Text_Argument_Parser::one_of() const noexcept -> const vector<string> &
	{
		return m_allowed_values;
	}

	auto Text_Argument_Parser::ensure_constraints(const string value) const -> void
	{
		if (!empty_text_is_allowed() && "" == value)
		{
			throw Constraint_Violation{"Value for string parameter " + single_quoted(name()) +
			                           " must not be empty."};
		}
		else if (!white_space_is_allowed() && contains_whitespace(value))
		{
			throw Constraint_Violation{"Value " + single_quoted(value) + " for string parameter " +
			                           single_quoted(name()) + " must not contain white space."};
		}
		else if (!m_allowed_values.empty() && !contains(m_allowed_values, value))
		{
			throw Constraint_Violation{"Value " + single_quoted(value) +
			                           " not in set of allowed values for text parameter " +
			                           single_quoted(name()) + "."};
		}
	}

	auto Text_Argument_Parser::accept(Argument_Parser_Visitor &visitor) const -> void
	{
		visitor.visit(*this);
	}

	auto Text_Argument_Parser::unchecked_value_from(const string &textual_rep) const -> string
	{
		return textual_rep;
	}

} // namespace yacl
