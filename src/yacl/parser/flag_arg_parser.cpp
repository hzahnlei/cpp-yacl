/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/flag_arg_parser.hpp"
#include "yacl/parser/arg_parser_visitor.hpp"
#include <junkbox/text.hpp>
#include <sstream>

namespace yacl
{

	using junkbox::text::representation_of_keys;
	using junkbox::text::single_quoted;
	using junkbox::operator<<;
	using std::ostringstream;

	const Flag_Argument_Parser::Text_to_Bool Flag_Argument_Parser::TEXT_TO_BOOL{
			{"true", true}, {"yes", true}, {"1", true}, {"false", false}, {"no", false}, {"0", false}};

	Flag_Argument_Parser::Flag_Argument_Parser(const string &name, const string &description,
	                                           const param::Identifiability identifiable_by, const bool default_val)
			: Abstract_Value_Argument_Parser{name, description, identifiable_by, default_val}
	{
		ensure_constraints_for_default(*m_default_value);
	}

	Flag_Argument_Parser::Flag_Argument_Parser(const string &name, const string &description,
	                                           const param::Identifiability identifiable_by)
			: Abstract_Value_Argument_Parser{name, description, identifiable_by}
	{
	}

	auto Flag_Argument_Parser::accept(Argument_Parser_Visitor &visitor) const -> void
	{
		visitor.visit(*this);
	}

	auto Flag_Argument_Parser::ensure_constraints(const bool) const -> void
	{
		// Not applicable to bool
	}

	auto Flag_Argument_Parser::unchecked_value_from(const string &textual_rep) const -> bool
	{
		const auto value = TEXT_TO_BOOL.find(textual_rep);
		if (value != TEXT_TO_BOOL.end())
		{
			return (*value).second;
		}
		else
		{
			ostringstream allowed_values;
			allowed_values << representation_of_keys(valid_flag_values());
			throw Argument_Parser_Error{"Invalid value " + single_quoted(textual_rep) +
			                            " for flag parameter " + single_quoted(name()) +
			                            ". Allowed values are " + allowed_values.str() + '.'};
		}
	}

} // namespace yacl
