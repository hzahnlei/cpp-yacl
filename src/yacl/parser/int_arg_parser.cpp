/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/int_arg_parser.hpp"
#include "yacl/parser/arg_parser_visitor.hpp"
#include <junkbox/text.hpp>

namespace yacl
{

	using junkbox::text::single_quoted;
	using std::invalid_argument;
	using std::to_string;

	Integer_Argument_Parser::Integer_Argument_Parser(const string &name, const string &description,
	                                                 const param::Identifiability identifiable_by,
	                                                 const int min_value, const int max_value,
	                                                 const optional<int> &default_val)
			: Abstract_Value_Argument_Parser{name, description, identifiable_by, default_val},
			  m_min{min_value}, m_max{max_value}
	{
		if (m_default_value.has_value())
		{
			ensure_constraints_for_default(*m_default_value);
		}
	}

	auto Integer_Argument_Parser::min() const noexcept -> int
	{
		return m_min;
	}

	auto Integer_Argument_Parser::max() const noexcept -> int
	{
		return m_max;
	}

	auto Integer_Argument_Parser::accept(Argument_Parser_Visitor &visitor) const -> void
	{
		visitor.visit(*this);
	}

	auto Integer_Argument_Parser::ensure_constraints(const int value) const -> void
	{
		if (m_min > value)
		{
			throw Constraint_Violation{"Value for integer parameter " + single_quoted(name()) +
			                           " must not be less than " + to_string(m_min) + " but is " +
			                           to_string(value) + "."};
		}
		else if (m_max < value)
		{
			throw Constraint_Violation{"Value for integer parameter " + single_quoted(name()) +
			                           " must not be greater than " + to_string(m_max) + " but is " +
			                           to_string(value) + "."};
		}
	}

	auto Integer_Argument_Parser::unchecked_value_from(const string &textual_rep) const -> int
	{
		try
		{
			return stoi(textual_rep);
		}
		catch (const invalid_argument &)
		{
			throw Argument_Parser_Error{"Invalid value " + single_quoted(textual_rep) +
			                            " for integer parameter " + single_quoted(name()) + "."};
		}
	}

} // namespace yacl
