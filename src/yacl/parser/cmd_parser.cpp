/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/cmd_parser.hpp"
#include "yacl/parser/arg_parser_visitor.hpp"
#include <algorithm>
#include <functional>
#include <junkbox/assertion.hpp>
#include <junkbox/convenient_algorithms.hpp>
#include <junkbox/text.hpp>
#include <junkbox/todo_marker.hpp>
#include <sstream>
#include <string>

namespace yacl
{

	using junkbox::algo::none_of;
	using junkbox::text::contains_whitespace;
	using junkbox::text::single_quoted;
	using junkbox::text::starts_with;
	using std::for_each;
	using std::make_unique;
	using std::to_string;

	Command_Parser::Command_Parser(const string &description, const Command_Action &action,
	                               Named_Argument_Parsers &&named_params, Positional_Argument_Parsers &&pos_params)
			: m_name{""}, m_description{description}, m_action{action},
			  m_named_parameters{move(named_params)}, m_positional_parameters{move(pos_params)}
	{
		DEV_ASSERT(!description.empty(),
		           "Description of command " + single_quoted(m_name) + " must not be empty.");
		DEV_ASSERT(nullptr != action, "Action for command " + single_quoted(m_name) + " must not be nullptr.");
	}

	Command_Parser::Command_Parser(const string &name, const string &description, const Command_Action &action,
	                               Named_Argument_Parsers &&named_params, Positional_Argument_Parsers &&pos_params)
			: m_name{name}, m_description{description}, m_action{action},
			  m_named_parameters{move(named_params)}, m_positional_parameters{move(pos_params)}
	{
		DEV_ASSERT(!m_name.empty(), "Command name must not be empty.");
		DEV_ASSERT(!contains_whitespace(m_name),
		           "Command name " + single_quoted(m_name) + " must not contain white space.");
		DEV_ASSERT(!description.empty(),
		           "Description of command " + single_quoted(m_name) + " must not be empty.");
		DEV_ASSERT(nullptr != action, "Action for command " + single_quoted(m_name) + " must not be nullptr.");
	}

	[[nodiscard]] inline auto is_named_argument(const string &arg)
	{
		return starts_with(arg, "--");
	}

	/**
	 * @brief Assuming is_named_argument(arg_name) == true
	 */
	[[nodiscard]] inline auto argument_name_with_dashes_stripped(const string &arg_name)
	{
		return arg_name.substr(2);
	};

	auto Command_Parser::command_from(List_Of_Cmd_Ln_Args_Iter &curr_arg,
	                                  const List_Of_Cmd_Ln_Args_Iter &last_arg) const -> unique_ptr<Command>
	{
		// This works with Clang but not with GCC. GCC seems to revert the order in which
		// the functions evaluated_named_arguments and evaluated_positional_arguments
		// are executed.
		// return make_unique<Command>(m_name, m_action, evaluated_named_arguments(curr_arg, last_arg),
		//                             evaluated_positional_arguments(curr_arg, last_arg));
		auto named_args{evaluated_named_arguments(curr_arg, last_arg)};
		auto pos_args{evaluated_positional_arguments(curr_arg, last_arg)};
		return make_unique<Command>(m_name, m_action, move(named_args), move(pos_args));
	}

	auto Command_Parser::name() const noexcept -> const string &
	{
		return m_name;
	}

	auto Command_Parser::is_anonymous_command() const noexcept -> bool
	{
		return m_name == "";
	}

	auto Command_Parser::description() const noexcept -> const string &
	{
		return m_description;
	}

	auto Command_Parser::has_parameter(const string &param_name) const -> bool
	{
		return m_named_parameters.find(param_name) != m_named_parameters.end();
	}

	auto Command_Parser::parameter(const string &param_name) const -> const Abstract_Argument_Parser &
	{
		return *(m_named_parameters.at(param_name));
	}

	auto Command_Parser::evaluated_named_arguments(List_Of_Cmd_Ln_Args_Iter &curr_arg,
	                                               const List_Of_Cmd_Ln_Args_Iter &last_arg) const
			-> Command::Named_Command_Line_Arguments
	{
		Command::Named_Command_Line_Arguments actual_args;
		while (curr_arg != last_arg && is_named_argument(*curr_arg))
		{
			const auto effective_arg_name{argument_name_with_dashes_stripped(*curr_arg)};
			curr_arg++;
			if (has_parameter(effective_arg_name))
			{
				const auto &param_def{parameter(effective_arg_name)};
				if (curr_arg == last_arg)
				{
					throw command_line_parsing_error("Named parameter " +
					                                 single_quoted("--" + param_def.name()) +
					                                 " is mandatory, but no value given.");
				}
				const auto &curr_arg_value{*curr_arg};
				curr_arg++;
				m_argument_value_producer.textual_representation = curr_arg_value;
				param_def.accept(m_argument_value_producer); // Also checks constraints.
				actual_args.try_emplace(effective_arg_name, move(m_argument_value_producer.argument));
			}
			else
			{
				throw command_line_parsing_error("There is no parameter with name " +
				                                 single_quoted("--" + effective_arg_name) + ".");
			}
		}
		ensure_all_mandatory_parameters_have_a_value(actual_args);
		populate_missing_optionals_with_default_values(actual_args);
		return actual_args;
	}

	auto Command_Parser::evaluated_positional_arguments(List_Of_Cmd_Ln_Args_Iter &curr_arg,
	                                                    const List_Of_Cmd_Ln_Args_Iter &last_arg) const
			-> Command::Positional_Command_Line_Arguments
	{
		Command::Positional_Command_Line_Arguments actual_positional_args;
		auto pos{0U};
		while (curr_arg != last_arg)
		{
			const auto effective_value{*curr_arg};
			if (pos >= m_positional_parameters.size())
			{
				throw command_line_parsing_error("To many positional parameters (" +
				                                 single_quoted(effective_value) + "). Expect only " +
				                                 to_string(m_positional_parameters.size()) +
				                                 " but found " + to_string(pos + 1) + ".");
			}
			if (is_named_argument(*curr_arg))
			{
				throw command_line_parsing_error("No named parameters (" +
				                                 single_quoted(effective_value) +
				                                 ") allowed after positional parameters.");
			}
			m_argument_value_producer.textual_representation = effective_value;
			const auto &param_def{*m_positional_parameters[pos]};
			param_def.accept(m_argument_value_producer); // Also checks constraints.
			actual_positional_args.emplace_back(move(m_argument_value_producer.argument));
			curr_arg++;
			pos++;
		}
		ensure_all_mandatory_parameters_have_a_value(actual_positional_args);
		populate_missing_optionals_with_default_values(actual_positional_args);
		return actual_positional_args;
	}

	[[nodiscard]] inline auto mandatory_arg_is_missing(const Command::Named_Command_Line_Arguments &arguments,
	                                                   const auto &parameter)
	{
		return parameter.is_mandatory() && arguments.find(parameter.name()) == arguments.end();
	};

	auto Command_Parser::ensure_all_mandatory_parameters_have_a_value(
			const Command::Named_Command_Line_Arguments &arguments) const -> void
	{
		for_each_named_parameter(
				[&](const auto &parameter)
				{
					if (mandatory_arg_is_missing(arguments, parameter))
					{
						throw Command_Parser_Error{"Mandatory, named parameter " +
				                                           single_quoted("--" + parameter.name()) +
				                                           " missing for " +
				                                           descriptive_command_name() + "."};
					}
				});
	}

	[[nodiscard]] inline auto mandatory_arg_is_missing(const Command::Positional_Command_Line_Arguments &arguments,
	                                                   const auto &parameter)
	{
		return parameter.is_mandatory() &&
		       none_of(arguments, [&](const auto &arg) { return arg->name() == parameter.name(); });
	};

	auto Command_Parser::ensure_all_mandatory_parameters_have_a_value(
			const Command::Positional_Command_Line_Arguments &arguments) const -> void
	{
		for_each_positional_parameter(
				[&](const auto &parameter)
				{
					if (mandatory_arg_is_missing(arguments, parameter))
					{
						throw Command_Parser_Error{"Mandatory, positional parameter " +
				                                           single_quoted(parameter.name()) +
				                                           " missing for " +
				                                           descriptive_command_name() + "."};
					}
				});
	}

	[[nodiscard]] inline auto optional_arg_is_missing(const Command::Named_Command_Line_Arguments &arguments,
	                                                  const auto &parameter)
	{
		return !parameter.is_mandatory() && arguments.find(parameter.name()) == arguments.end();
	};

	auto Command_Parser::default_value_from(const Abstract_Argument_Parser &argument) const
			-> unique_ptr<Abstract_Command_Line_Argument>
	{
		argument.accept(m_optional_argument_value_producer);
		return move(m_optional_argument_value_producer.argument);
	}

	auto Command_Parser::populate_missing_optionals_with_default_values(
			Command::Named_Command_Line_Arguments &actual_args) const -> void
	{
		for_each_named_parameter(
				[this, &actual_args](const auto &parameter)
				{
					if (optional_arg_is_missing(actual_args, parameter))
					{
						actual_args.emplace(parameter.name(), default_value_from(parameter));
					}
				});
	}

	[[nodiscard]] inline auto optional_arg_is_missing(const Command::Positional_Command_Line_Arguments &arguments,
	                                                  const auto &parameter)
	{
		return !parameter.is_mandatory() &&
		       none_of(arguments, [&](const auto &arg) { return arg->name() == parameter.name(); });
	};

	auto Command_Parser::populate_missing_optionals_with_default_values(
			Command::Positional_Command_Line_Arguments &actual_args) const -> void
	{
		for_each_positional_parameter(
				[this, &actual_args](const auto &parameter)
				{
					if (optional_arg_is_missing(actual_args, parameter))
					{
						actual_args.emplace_back(default_value_from(parameter));
					}
				});
	}

	auto Command_Parser::descriptive_command_name() const -> string
	{
		return is_anonymous_command() ? "anonymous command" : "command " + single_quoted(m_name);
	}

} // namespace yacl
