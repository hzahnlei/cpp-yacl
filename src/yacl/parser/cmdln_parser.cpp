/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/cmdln_parser.hpp"
#include "yacl/parser/cmdln_usage_printer.hpp"
#include <algorithm>
#include <junkbox/assertion.hpp>
#include <junkbox/text.hpp>
#include <junkbox/todo_marker.hpp>
#include <sstream>

namespace yacl
{

	using junkbox::text::contains_whitespace;
	using junkbox::text::representation_of_keys;
	using junkbox::text::single_quoted;
	using junkbox::operator<<;
	using std::ostringstream;

	Command_Line_Parser::Command_Line_Parser(const string &program_name, const string &banner,
	                                         const string &description, const string &version,
	                                         Map_Of_Command_Parsers &&cmd_parsers,
	                                         optional<Command_Parser> m_anonymous_command,
	                                         vector<string> &&p_authors, vector<string> &&p_oss,
	                                         optional<string> &&copyright, optional<License> &&license,
	                                         optional<string> &&disclaimer)
			: m_program_name{program_name}, m_banner{banner}, m_description{description},
			  m_version{version}, m_command_parsers{move(cmd_parsers)},
			  m_anonymous_command{move(m_anonymous_command)}, m_authors{move(p_authors)},
			  m_oss_used{move(p_oss)}, m_copyright{move(copyright)}, m_license{move(license)},
			  m_disclaimer{move(disclaimer)}
	{
		DEV_ASSERT(!m_program_name.empty(), "Program name must not be empty.");
		DEV_ASSERT(!contains_whitespace(m_program_name),
		           "Program name " + single_quoted(m_program_name) + " must not contain white space.");
		DEV_ASSERT(!m_description.empty(),
		           "Description of program " + single_quoted(m_program_name) + " must not be empty.");
		DEV_ASSERT(!m_version.empty(),
		           "Version of program " + single_quoted(m_program_name) + " must not be empty.");
	}

	[[nodiscard]] inline auto arguments_are_given(const Command_Parser::List_Of_Cmd_Ln_Args &arg_list)
	{
		return !arg_list.empty();
	}

	const auto textual_rep_of_all_commands = [](const auto &commands)
	{
		ostringstream result;
		result << "Possible commands are: " << representation_of_keys(commands) << ".";
		return result.str();
	};

	auto Command_Line_Parser::command_from(const Command_Parser::List_Of_Cmd_Ln_Args &arg_list) const
			-> unique_ptr<Command>
	{
		if (arguments_are_given(arg_list))
		{
			return command_by_name(arg_list);
		}
		else if (has_anonymous_command())
		{
			return anonymous_command(arg_list);
		}
		else
		{
			throw Command_Line_Parser_Error{"No arguments given. " +
			                                textual_rep_of_all_commands(m_command_parsers)};
		}
	}

	auto Command_Line_Parser::program_name() const noexcept -> const string &
	{
		return m_program_name;
	}

	auto Command_Line_Parser::description() const noexcept -> const string &
	{
		return m_description;
	}

	auto Command_Line_Parser::version() const noexcept -> const string &
	{
		return m_version;
	}

	auto Command_Line_Parser::banner() const noexcept -> const string &
	{
		return m_banner;
	}

	auto Command_Line_Parser::has_banner() const -> bool
	{
		return !m_banner.empty();
	}

	auto Command_Line_Parser::copyright() const noexcept -> const optional<string> &
	{
		return m_copyright;
	}

	auto Command_Line_Parser::license() const noexcept -> const optional<License> &
	{
		return m_license;
	}

	auto Command_Line_Parser::disclaimer() const noexcept -> const optional<string> &
	{
		return m_disclaimer;
	}

	auto Command_Line_Parser::authors() const noexcept -> const vector<string> &
	{
		return m_authors;
	}

	auto Command_Line_Parser::open_source_software_used() const noexcept -> const vector<string> &
	{
		return m_oss_used;
	}

	auto Command_Line_Parser::command_parser(const string &name) const -> const Command_Parser &
	{
		const auto cmd = m_command_parsers.find(name);
		if (cmd != m_command_parsers.end())
		{
			return (*cmd).second;
		}
		else if (has_anonymous_command())
		{
			return *m_anonymous_command;
		}
		else
		{
			throw Command_Line_Parser_Error{"Unknown command " + single_quoted(name) + ". " +
			                                textual_rep_of_all_commands(m_command_parsers)};
		}
	}

	auto Command_Line_Parser::begin() const -> Command_Parser_Iter
	{
		return m_command_parsers.cbegin();
	}

	auto Command_Line_Parser::end() const -> Command_Parser_Iter
	{
		return m_command_parsers.cend();
	}

	auto Command_Line_Parser::command_by_name(const Command_Parser::List_Of_Cmd_Ln_Args &arg_list) const
			-> unique_ptr<Command>
	{
		const auto &cmd_name = arg_list[0];
		const auto &effective_command{command_parser(cmd_name)};
		auto curr_arg{cbegin(arg_list)};
		const auto last_arg{cend(arg_list)};
		return effective_command.is_anonymous_command() ? effective_command.command_from(curr_arg, last_arg)
		                                                : effective_command.command_from(++curr_arg, last_arg);
	}

	auto Command_Line_Parser::anonymous_command(const Command_Parser::List_Of_Cmd_Ln_Args &arg_list) const
			-> unique_ptr<Command>
	{
		auto curr_arg{cbegin(arg_list)};
		const auto last_arg{cend(arg_list)};
		return m_anonymous_command->command_from(curr_arg, last_arg);
	}

} // namespace yacl
