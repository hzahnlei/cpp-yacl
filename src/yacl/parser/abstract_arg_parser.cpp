/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/abstract_arg_parser.hpp"
#include <junkbox/assertion.hpp>
#include <junkbox/text.hpp>
#include <junkbox/todo_marker.hpp>

namespace yacl
{

	using namespace junkbox;

	Abstract_Argument_Parser::Abstract_Argument_Parser(const string &name, const string &description,
	                                                   const param::Identifiability identifiable_by)
			: m_parameter_name{name}, m_description{description}, m_identifiable_by{identifiable_by}
	{
		DEV_ASSERT(!m_parameter_name.empty(), "Parameter name must not be empty.");
		DEV_ASSERT(!text::contains_whitespace(m_parameter_name),
		           "Parameter name " + text::single_quoted(m_parameter_name) +
		                           " must not contain white space.");
		DEV_ASSERT(!m_description.empty(), "Parameter description must not be empty.");
	}

	auto Abstract_Argument_Parser::name() const noexcept -> const string &
	{
		return m_parameter_name;
	}

	auto Abstract_Argument_Parser::is_positional() const -> bool
	{
		return m_identifiable_by == param::Identifiability::BY_POSITION;
	}

	auto Abstract_Argument_Parser::description() const noexcept -> const string &
	{
		return m_description;
	}

} // namespace yacl
