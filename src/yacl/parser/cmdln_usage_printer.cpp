/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/cmdln_usage_printer.hpp"
#include "yacl/parser/arg_parser_visitor.hpp"
#include "yacl/parser/license.hpp"
#include <junkbox/convenient_algorithms.hpp>
#include <junkbox/text.hpp>

namespace yacl
{

	using namespace junkbox;
	using junkbox::operator<<;
	using std::to_string;

	auto print_program_description(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		textual_rep << command_line.description() << "\n\n";
	}

	auto parameter_name_with_dashes(const string &par_name)
	{
		return "--" + par_name;
	}

	auto variable_name(const string &var_name)
	{
		return '<' + var_name + '>';
	}

	auto syntax_of_named_parameter(const string &param_name)
	{
		return parameter_name_with_dashes(param_name) + ' ' + variable_name(param_name);
	}

	auto syntax_of_positional_parameter(const string &param_name)
	{
		return variable_name(param_name);
	}

	auto print_named_parameter_for_synopsis(ostream &textual_rep, const Abstract_Argument_Parser &param_def)
	{
		if (param_def.is_mandatory())
		{
			textual_rep << syntax_of_named_parameter(param_def.name());
		}
		else
		{
			textual_rep << '[' << syntax_of_named_parameter(param_def.name()) << ']';
		}
	}

	auto print_positional_parameter_for_synopsis(ostream &textual_rep, const Abstract_Argument_Parser &param_def)
	{
		if (param_def.is_mandatory())
		{
			textual_rep << syntax_of_positional_parameter(param_def.name());
		}
		else
		{
			textual_rep << '[' << syntax_of_positional_parameter(param_def.name()) << ']';
		}
	}

	auto print_synopsis(ostream &textual_rep, const Command_Parser &cmd_def)
	{
		cmd_def.for_each_named_parameter(
				[&textual_rep](const auto &param)
				{
					textual_rep << ' ';
					print_named_parameter_for_synopsis(textual_rep, param);
				});
		cmd_def.for_each_positional_parameter(
				[&textual_rep](const auto &param)
				{
					textual_rep << ' ';
					print_positional_parameter_for_synopsis(textual_rep, param);
				});
	}

	class Parameter_Detail_Printing_Visitor : public Argument_Parser_Visitor
	{
	    private:
		ostream &m_stream;
		const string m_alignment;

	    public:
		explicit Parameter_Detail_Printing_Visitor(ostream &textual_rep, const string &alignment)
				: m_stream{textual_rep}, m_alignment{alignment}
		{
		}

		auto visit(const Integer_Argument_Parser &param) -> void override
		{
			if (param.defaults_to().has_value())
			{
				// Maybe for boolean better print true/false for 1/0?
				m_stream << m_alignment << "Defaults to " << to_string(*(param.defaults_to())) << ".\n";
			}
			m_stream << m_alignment << "Where " << variable_name(param.name()) << " has to be in ["
				 << param.min() << ", " << param.max() << "].\n";
		}

		auto visit(const Flag_Argument_Parser &param) -> void override
		{
			if (param.defaults_to().has_value())
			{
				m_stream << m_alignment << "Defaults to " << to_string(*(param.defaults_to())) << ".\n";
			}
			m_stream << m_alignment << "Where " << variable_name(param.name()) << " has to be one of ";
			print_all_valid_flag_values(param);
			m_stream << ".\n";
		}

		auto visit(const Text_Argument_Parser &param) -> void override
		{
			if (param.defaults_to().has_value())
			{
				m_stream << m_alignment << "Defaults to " << text::single_quoted(*(param.defaults_to()))
					 << ".\n";
			}
			m_stream << m_alignment << "Where " << variable_name(param.name());
			if (constrained_to_choices(param))
			{
				m_stream << " has to be one of ";
				print_all_choices(param);
				m_stream << ".\n";
			}
			else
			{
				m_stream << (param.empty_text_is_allowed() ? " is a string that might be empty"
				                                           : " is a none-empty string")
					 << (param.white_space_is_allowed() ? " (may contain whitespace)"
				                                            : " (must not contain whitespace)")
					 << ".\n";
			}
		}

	    private:
		[[nodiscard]] auto constrained_to_choices(const Text_Argument_Parser &param) const -> bool
		{
			return !param.one_of().empty();
		}

		auto print_all_choices(const Text_Argument_Parser &param) -> void
		{
			m_stream << text::representation_of_elements(param.one_of());
		}

		auto print_all_valid_flag_values(const Flag_Argument_Parser &param) -> void
		{
			m_stream << text::representation_of_keys(param.valid_flag_values());
		}
	};

	[[nodiscard]] static auto alignment(const string &indentation, const size_t align_count)
	{
		string alignment{indentation};
		for (auto i = 0U; i < align_count; i++)
		{
			alignment += ' ';
		}
		return alignment;
	}

	auto print_detail_for_named_parameter(ostream &textual_rep, const Abstract_Argument_Parser &param_def,
	                                      const string &align)
	{
		const auto syntax{syntax_of_named_parameter(param_def.name())};
		textual_rep << align << syntax << ": " << param_def.description();
		const auto new_align{alignment(align, (syntax + ": ").size())};
		textual_rep << "\n"
			    << new_align << (param_def.is_mandatory() ? "Mandatory" : "Optional")
			    << ", named parameter.\n";
		Parameter_Detail_Printing_Visitor param_detail_printer{textual_rep, new_align};
		param_def.accept(param_detail_printer);
	}

	auto print_detail_for_positional_parameter(ostream &textual_rep, const Abstract_Argument_Parser &param_def,
	                                           const string &align)
	{
		const auto syntax{syntax_of_positional_parameter(param_def.name())};
		textual_rep << align << syntax << ": " << param_def.description();
		const auto new_align{alignment(align, (syntax + ": ").size())};
		textual_rep << "\n"
			    << new_align << (param_def.is_mandatory() ? "Mandatory" : "Optional")
			    << ", positional parameter.\n";
		Parameter_Detail_Printing_Visitor param_detail_printer{textual_rep, new_align};
		param_def.accept(param_detail_printer);
	}

	auto print_details_for_all_parameters(ostream &textual_rep, const Command_Parser &cmd_def,
	                                      const string &alignment)
	{
		if (cmd_def.named_parameter_count() > 0 || cmd_def.positional_parameter_count() > 0)
		{
			textual_rep << "\n";
		}
		auto i{0U};
		cmd_def.for_each_named_parameter(
				[&](const auto &param)
				{
					if (i > 0)
					{
						textual_rep << '\n';
					}
					print_detail_for_named_parameter(textual_rep, param, alignment);
					i++;
				});
		cmd_def.for_each_positional_parameter(
				[&](const auto &param)
				{
					if (i > 0)
					{
						textual_rep << '\n';
					}
					print_detail_for_positional_parameter(textual_rep, param, alignment);
					i++;
				});
	}

	auto print_named_command(ostream &textual_rep, const string &prog_name, const string &cmd_name,
	                         const Command_Parser &cmd_def)
	{
		const auto command_name_caption{text::upper_case(cmd_name)};
		textual_rep << '\t' << command_name_caption << " - " << cmd_def.description() << "\n\n";
		const auto align{alignment("\t\t", 0)};
		textual_rep << align << prog_name << ' ' << cmd_name;
		print_synopsis(textual_rep, cmd_def);
		textual_rep << '\n';
		print_details_for_all_parameters(textual_rep, cmd_def, align);
	}

	auto print_anonymous_command(ostream &textual_rep, const string &prog_name, const Command_Parser &cmd_def)
	{
		const auto command_name_caption{text::upper_case("If no command specified")};
		textual_rep << '\t' << command_name_caption << " - " << cmd_def.description() << "\n\n";
		const auto align{alignment("\t\t", 0)};
		textual_rep << align << prog_name;
		print_synopsis(textual_rep, cmd_def);
		textual_rep << '\n';
		print_details_for_all_parameters(textual_rep, cmd_def, align);
	}

	auto print_all_commands(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		textual_rep << "AVAILABLE COMMANDS\n\n";
		auto i = 0U;
		algo::for_each(command_line,
		               [&i, &textual_rep, &command_line](const auto &cmd)
		               {
				       if (i > 0)
				       {
					       textual_rep << "\n";
				       }
				       print_named_command(textual_rep, command_line.program_name(), cmd.first,
			                                   cmd.second);
				       i++;
			       });
		if (command_line.has_anonymous_command())
		{
			if (i > 0)
			{
				textual_rep << "\n";
			}
			print_anonymous_command(textual_rep, command_line.program_name(),
			                        command_line.anonymous_command());
			i++;
		}
	}

	auto print_banner(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		if (command_line.has_banner())
		{
			textual_rep << command_line.banner() << "\n\n";
		}
	}

	auto print_copyright(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		if (command_line.copyright().has_value())
		{
			textual_rep << "\nCOPYRIGHT STATEMENT\n\n";
			textual_rep << '\t' << *(command_line.copyright()) << '\n';
		}
	}

	auto print_authors(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		if (!command_line.authors().empty())
		{
			textual_rep << "\nAUTHORS\n\n";
			textual_rep << "\tThis software has been written by ";
			textual_rep << text::representation_of_elements(command_line.authors(),
			                                                text::do_not_quote_elements);
			textual_rep << ".\n";
		}
	}

	auto print_license(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		if (command_line.license().has_value())
		{
			textual_rep << "\nLICENSE\n\n";
			textual_rep << "\tThis software is distributed under the "
				    << textual_representation(*(command_line.license())) << " license.\n\n";
			textual_rep << text::indented("\t", license_text(*(command_line.license()))) << '\n';
		}
	}

	auto print_disclaimer(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		if (command_line.disclaimer().has_value())
		{
			textual_rep << "\nDISCLAIMER\n\n";
			textual_rep << text::indented("\t", *(command_line.disclaimer())) << '\n';
		}
	}

	auto print_oss(ostream &textual_rep, const Command_Line_Parser &command_line)
	{
		if (!command_line.open_source_software_used().empty())
		{
			textual_rep << "\nACKNOWLEDGEMENTS\n\n";
			textual_rep << "\tThis software is built on top of ";
			textual_rep << text::representation_of_elements(command_line.open_source_software_used(),
			                                                text::do_not_quote_elements);
			textual_rep << ".\n";
		}
	}

	auto operator<<(ostream &textual_rep, const Command_Line_Parser &command_line) -> ostream &
	{
		print_banner(textual_rep, command_line);
		print_program_description(textual_rep, command_line);
		print_all_commands(textual_rep, command_line);
		print_copyright(textual_rep, command_line);
		print_authors(textual_rep, command_line);
		print_license(textual_rep, command_line);
		print_disclaimer(textual_rep, command_line);
		print_oss(textual_rep, command_line);
		return textual_rep;
	}

} // namespace yacl
