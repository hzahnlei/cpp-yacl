/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/parser/arg_producing_visitor.hpp"
#include <junkbox/assertion.hpp>
#include "yacl/flag_cmdln_arg.hpp"
#include "yacl/int_cmdln_arg.hpp"
#include <junkbox/text.hpp>
#include "yacl/text_cmdln_arg.hpp"
#include <junkbox/todo_marker.hpp>

namespace yacl
{

        using std::make_unique;

        auto Argument_Producing_Visitor::visit(const Integer_Argument_Parser &param) -> void
        {
                argument = make_unique<Integer_Command_Line_Argument>(param.name(),
                                                                      param.value_from(textual_representation));
        }

        auto Argument_Producing_Visitor::visit(const Flag_Argument_Parser &param) -> void
        {
                argument = make_unique<Flag_Command_Line_Argument>(param.name(),
                                                                   param.value_from(textual_representation));
        }

        auto Argument_Producing_Visitor::visit(const Text_Argument_Parser &param) -> void
        {
                argument = make_unique<Text_Command_Line_Argument>(param.name(),
                                                                   param.value_from(textual_representation));
        }

} // namespace yacl
