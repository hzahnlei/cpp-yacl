/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/text_cmdln_arg.hpp"
#include "yacl/cmdln_arg_visitor.hpp"

namespace yacl
{

        Text_Command_Line_Argument::Text_Command_Line_Argument(const string &param_name, const string &value)
            : Abstract_Command_Line_Argument{param_name}, m_value{value}
        {
        }

        auto Text_Command_Line_Argument::accept(Command_Line_Argument_Visitor &visitor) const -> void
        {
                visitor.visit(*this);
        }

        auto Text_Command_Line_Argument::value() const noexcept -> const string &
        {
                return m_value;
        }

} // namespace yacl
