/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/command.hpp"
#include <junkbox/text.hpp>
#include <string>

namespace yacl
{

	using namespace junkbox;
	using std::to_string;

	Command::Command(const string &cmd_name, const Command_Action &action, Named_Command_Line_Arguments &&arg_list,
	                 Positional_Command_Line_Arguments &&pos_arguments)
			: m_command_name{cmd_name}, m_action{action}, m_arguments{move(arg_list)},
			  m_positional_arguments{move(pos_arguments)}
	{
	}

	auto Command::operator()(const Execution_Context &context, istream &in, ostream &out, ostream &err) const
			-> void
	{
		m_action(context, in, out, err);
	}

	[[nodiscard]] auto descriptive_command_name(const Command &cmd)
	{
		return cmd.is_anonymous_command() ? "anonymous command" : "command " + text::single_quoted(cmd.name());
	}

	auto Command::argument_by_name(const string &name) const -> const Abstract_Command_Line_Argument &
	{
		const auto argument = m_arguments.find(name);
		if (argument != m_arguments.end())
		{
			return *((*argument).second);
		}
		else
		{
			throw Command_Error{"Argument " + text::single_quoted(name) + " not defined for " +
			                    descriptive_command_name(*this) + "."};
		}
	}

	auto Command::argument_by_position(const Position pos) const -> const Abstract_Command_Line_Argument &
	{
		if (pos >= m_positional_arguments.size())
		{
			throw Command_Error{"Trying to access positional argument " + to_string(pos + 1) +
			                    ", but only " + to_string(m_positional_arguments.size()) +
			                    " positional arguments given for " + descriptive_command_name(*this) + "."};
		}
		return *m_positional_arguments[pos];
	}

} // namespace yacl
