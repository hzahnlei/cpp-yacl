/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/text_val_extracting_visitor.hpp"
#include <junkbox/text.hpp>
#include <stdexcept>

namespace yacl
{

        using junkbox::text::single_quoted;

        auto Text_Value_Extracting_Visitor::visit(const Integer_Command_Line_Argument &argument) -> void
        {
                throw Command_Line_Argument_Error{"Failed to access value of given argument. "
                                                  "Parameter " +
                                                  single_quoted(argument.name()) +
                                                  " is an integer parameter not a text parameter."};
        }

        auto Text_Value_Extracting_Visitor::visit(const Flag_Command_Line_Argument &argument) -> void
        {
                throw Command_Line_Argument_Error{"Failed to access value of given argument. "
                                                  "Parameter " +
                                                  single_quoted(argument.name()) +
                                                  " is a flag parameter not a text parameter."};
        }

        auto Text_Value_Extracting_Visitor::visit(const Text_Command_Line_Argument &argument) -> void
        {
                value = argument.value();
        }

} // namespace yacl
