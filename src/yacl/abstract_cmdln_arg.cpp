/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/abstract_cmdln_arg.hpp"

namespace yacl
{

        Abstract_Command_Line_Argument::Abstract_Command_Line_Argument(const string &param_name)
            : m_parameter_name{param_name}
        {
        }

        auto Abstract_Command_Line_Argument::name() const noexcept -> const string &
        {
                return m_parameter_name;
        }

} // namespace yacl
