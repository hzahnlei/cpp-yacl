//******************************************************************************
//
//   *
//  / \   Diamond Mine - A terminological database.
// *   *
//  \ /   Copyright (c) 2014-2020 Holger Zahnleiter. All rights reserved.
//   *
//
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// 2018-03-31; 0.0.0; Holger Zahnleiter; Initial version.
//******************************************************************************

#include "yacl/base_application.hpp"
#include <iostream>
#include <junkbox/assertion.hpp>
#include <junkbox/on_exit.hpp>
#include <junkbox/text.hpp>
#include <regex>

namespace yacl
{

	using std::istream;
	using std::move;
	using std::ostream;
	using std::regex;
	using std::regex_match;
	using std::smatch;

	const regex LAST_PART_OF_PATH{"(.*/)*(.+)$"};

	auto program_name(const int arg_count, const char *arg_list[]) -> string
	{
		DEV_ASSERT(arg_count > 0, "Expected at least one argument.");
		const string exe_name_full_path{arg_list[0]};
		smatch all_matches;
		regex_match(exe_name_full_path, all_matches, LAST_PART_OF_PATH);
		return all_matches[all_matches.size() - 1];
	}

	auto list_of_arguments(const int arg_count, const char *arg_list[]) -> List_Of_Arguments
	{
		List_Of_Arguments result;
		for (auto i = 1; i < arg_count; i++)
		{
			result.emplace_back(arg_list[i]);
		}
		return result;
	}

	Base_Application::Base_Application(Command_Line_Parser &&command_line_parser)
			: m_command_line_parser{move(command_line_parser)}
	{
	}

	auto Base_Application::run(const List_Of_Arguments &arg_list, istream &in, ostream &out, ostream &err) -> void
	{
		ON_EXIT_DO(m_is_running = false);
		const auto command = m_command_line_parser.command_from(arg_list);
		m_is_running = true;
		(*command)(Execution_Context{*command, m_command_line_parser}, in, out, err);
	}

	auto Base_Application::is_running() const noexcept -> bool
	{
		return m_is_running;
	}

	auto Base_Application::stop() -> void
	{
		m_request_stop = true;
	}

	auto Base_Application::is_stop_requested() const -> bool
	{
		return m_request_stop;
	}

} // namespace yacl
