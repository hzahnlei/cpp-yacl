/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/execution_context.hpp"
#include "yacl/command.hpp"
#include "yacl/parser/cmdln_parser.hpp"

namespace yacl
{

	Execution_Context::Execution_Context(const Command &cmd, const Command_Line_Parser &cmdln_parser)
			: command{cmd}, command_line_parser{cmdln_parser}
	{
	}

	auto Execution_Context::integer_value_of(const Name &name) const -> int
	{
		argument_by_name(name).accept(m_integer_value_extractor);
		return m_integer_value_extractor.value;
	}

	auto Execution_Context::flag_value_of(const Name &name) const -> bool
	{
		argument_by_name(name).accept(m_flag_value_extractor);
		return m_flag_value_extractor.value;
	}

	auto Execution_Context::text_value_of(const Name &name) const -> string
	{
		argument_by_name(name).accept(m_text_value_extractor);
		return m_text_value_extractor.value;
	}

	auto Execution_Context::integer_value_at(const Position pos) const -> int
	{
		argument_by_position(pos).accept(m_integer_value_extractor);
		return m_integer_value_extractor.value;
	}

	auto Execution_Context::flag_value_at(const Position pos) const -> bool
	{
		argument_by_position(pos).accept(m_flag_value_extractor);
		return m_flag_value_extractor.value;
	}

	auto Execution_Context::text_value_at(const Position pos) const -> string
	{
		argument_by_position(pos).accept(m_text_value_extractor);
		return m_text_value_extractor.value;
	}

	auto Execution_Context::argument_by_name(const string &name) const -> const Abstract_Command_Line_Argument &
	{
		return command.argument_by_name(name);
	}

	auto Execution_Context::argument_by_position(const Position pos) const -> const Abstract_Command_Line_Argument &
	{
		return command.argument_by_position(pos);
	}

} // namespace yacl
