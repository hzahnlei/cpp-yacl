/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/flag_arg_parser_builder.hpp"
#include "yacl/parser/flag_arg_parser.hpp"

namespace yacl
{

	using std::make_unique;

	Flag_Argument_Parser_Builder::Flag_Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder,
	                                                           const string &name,
	                                                           const param::Requiredness required,
	                                                           const param::Identifiability identifiable_by)
			: Abstract_Copyable_Value_Argument_Parser_Builder{cmd_parser_builder, name, required,
	                                                                  identifiable_by}
	{
	}

	auto Flag_Argument_Parser_Builder::description(const string &description) -> Flag_Argument_Parser_Builder &
	{
		return static_cast<Flag_Argument_Parser_Builder &>(
				Abstract_Argument_Parser_Builder::description(description));
	}

	auto Flag_Argument_Parser_Builder::build_parser_with_requiredness_ensured()
			-> unique_ptr<Abstract_Argument_Parser>
	{
		return has_default_value() ? make_unique<Flag_Argument_Parser>(m_parameter_name, m_description,
		                                                               m_identifiable_by, *m_default_value)
		                           : make_unique<Flag_Argument_Parser>(m_parameter_name, m_description,
		                                                               m_identifiable_by);
	}

} // namespace yacl
