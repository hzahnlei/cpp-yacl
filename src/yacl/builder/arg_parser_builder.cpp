/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/arg_parser_builder.hpp"

namespace yacl
{

	Argument_Parser_Builder::Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder,
	                                                 const param::Requiredness required)
			: m_command_parser_builder{cmd_parser_builder}, m_requiredness{required}
	{
	}

	auto Argument_Parser_Builder::positional() -> Argument_Parser_Builder &
	{
		m_identifiable_by = param::Identifiability::BY_POSITION;
		return *this;
	}

	auto Argument_Parser_Builder::named() -> Argument_Parser_Builder &
	{
		m_identifiable_by = param::Identifiability::BY_NAME;
		return *this;
	}

	auto Argument_Parser_Builder::int_param(const string &name) -> Integer_Argument_Parser_Builder
	{
		return Integer_Argument_Parser_Builder{m_command_parser_builder, name, m_requiredness,
		                                       m_identifiable_by};
	}

	auto Argument_Parser_Builder::flag_param(const string &name) -> Flag_Argument_Parser_Builder
	{
		return Flag_Argument_Parser_Builder{m_command_parser_builder, name, m_requiredness, m_identifiable_by};
	}

	auto Argument_Parser_Builder::text_param(const string &name) -> Text_Argument_Parser_Builder
	{
		return Text_Argument_Parser_Builder{m_command_parser_builder, name, m_requiredness, m_identifiable_by};
	}

} // namespace yacl
