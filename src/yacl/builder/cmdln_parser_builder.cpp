/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/cmdln_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/parser/cmdln_usage_printer.hpp"
#include <algorithm>
#include <junkbox/text.hpp>

namespace yacl
{

	using junkbox::text::contains_whitespace;
	using junkbox::text::single_quoted;
	using std::make_unique;
	using std::move;

	Command_Line_Parser_Builder::Command_Line_Parser_Builder(const string &program_name, const string &banner)
			: m_program_name{program_name}, m_banner{banner}
	{
		if ("" == program_name)
		{
			throw Command_Line_Parser_Builder_Error{"Program name must not be empty."};
		}
		else if (contains_whitespace(program_name))
		{
			throw Command_Line_Parser_Builder_Error{"Program name " + single_quoted(program_name) +
			                                        " must not contain white space."};
		}
		else
		{
			register_help_command();
			register_version_command();
		}
	}

	auto Command_Line_Parser_Builder::version(const string &version) -> Command_Line_Parser_Builder &
	{
		ensure_valid_version(version);
		m_version = version;
		return *this;
	}

	auto Command_Line_Parser_Builder::copyright(const string &copyright) -> Command_Line_Parser_Builder &
	{
		ensure_valid_copyright(copyright);
		m_copyright = copyright;
		return *this;
	}

	auto Command_Line_Parser_Builder::license(const License license) -> Command_Line_Parser_Builder &
	{
		ensure_valid_license(license);
		m_license = license;
		return *this;
	}

	auto Command_Line_Parser_Builder::disclaimer(const string &disclaimer) -> Command_Line_Parser_Builder &
	{
		ensure_valid_disclaimer(disclaimer);
		m_disclaimer = disclaimer;
		return *this;
	}

	auto Command_Line_Parser_Builder::author(const string &author) -> Command_Line_Parser_Builder &
	{
		ensure_valid_author(author);
		m_authors.emplace_back(author);
		return *this;
	}

	auto Command_Line_Parser_Builder::open_source_software_used(const string &oss_used)
			-> Command_Line_Parser_Builder &
	{
		ensure_valid_oss(oss_used);
		m_oss_used.emplace_back(oss_used);
		return *this;
	}

	auto Command_Line_Parser_Builder::command(const string &cmd_name) -> Command_Parser_Builder &
	{
		m_curr_cmd_in_progress = make_unique<Command_Parser_Builder_Default_Impl>(
				[this](const auto &name, Command_Parser &&cmd)
				{ this->register_command(name, move(cmd)); },
				cmd_name);
		return *m_curr_cmd_in_progress;
	}

	auto Command_Line_Parser_Builder::has_anonymous_command() const noexcept -> bool
	{
		return m_anonymous_command.has_value();
	}

	auto Command_Line_Parser_Builder::anonymous_command() -> Command_Parser_Builder &
	{
		m_curr_cmd_in_progress = make_unique<Command_Parser_Builder_Default_Impl>(
				[this](const auto &, Command_Parser &&cmd)
				{ this->register_anonymous_command(move(cmd)); });
		return *m_curr_cmd_in_progress;
	}

	auto Command_Line_Parser_Builder::description(const string &description) -> Command_Line_Parser
	{
		ensure_valid_description(description);
		return Command_Line_Parser{m_program_name,  m_banner,          description,
		                           m_version,       move(m_commands),  move(m_anonymous_command),
		                           move(m_authors), move(m_oss_used),  move(m_copyright),
		                           move(m_license), move(m_disclaimer)};
	}

	auto Command_Line_Parser_Builder::register_help_command() -> void
	{
		constexpr auto CMD_HELP = "help";
		this->command(CMD_HELP)
				.description("This short help message.")
				.action([](const auto &context, auto &, auto &out, auto &)
		                        { out << context.command_line_parser << '\n'; });
	}

	auto Command_Line_Parser_Builder::register_version_command() -> void
	{
		constexpr auto CMD_VERSION = "version";
		this->command(CMD_VERSION)
				.description("Version information.")
				.action(
						[](const auto &context, auto &, auto &out, auto &) {
							out << context.command_line_parser.program_name() << " "
							    << context.command_line_parser.version() << '\n';
						});
	}

	auto Command_Line_Parser_Builder::ensure_valid_version(const string &version) const -> void
	{
		if ("" == version)
		{
			throw Command_Line_Parser_Builder_Error{"Program version must not be empty."};
		}
		else if (!m_version.empty())
		{
			throw Command_Line_Parser_Builder_Error{"Trying to override program version " +
			                                        single_quoted(m_version) + " with " +
			                                        single_quoted(version) + "."};
		}
	}

	auto Command_Line_Parser_Builder::ensure_valid_copyright(const string &copyright) const -> void
	{
		if ("" == copyright)
		{
			throw Command_Line_Parser_Builder_Error{"Copyright statement must not be empty when given. "
			                                        "It is optional. "
			                                        "Just leave it out, if it is your intention not to "
			                                        "provide a copyright statement."};
		}
		else if (m_copyright.has_value())
		{
			throw Command_Line_Parser_Builder_Error{"Trying to override copyright statement " +
			                                        single_quoted(*m_copyright) + " with " +
			                                        single_quoted(copyright) + "."};
		}
	}

	auto Command_Line_Parser_Builder::ensure_valid_license(const License license) const -> void
	{
		if (m_license.has_value())
		{
			throw Command_Line_Parser_Builder_Error{"Trying to override license " +
			                                        textual_representation(*m_license) + " with " +
			                                        textual_representation(license) + "."};
		}
	}

	auto Command_Line_Parser_Builder::ensure_valid_disclaimer(const string &disclaimer) const -> void
	{
		if ("" == disclaimer)
		{
			throw Command_Line_Parser_Builder_Error{
					"Disclaimer must not be empty when given. "
					"It is optional. "
					"Just leave it out, if it is your intention not to provide a disclaimer."};
		}
		else if (m_disclaimer.has_value())
		{
			throw Command_Line_Parser_Builder_Error{"Trying to override disclaimer " +
			                                        single_quoted(*m_disclaimer) + " with " +
			                                        single_quoted(disclaimer) + "."};
		}
	}

	auto Command_Line_Parser_Builder::ensure_valid_author(const string &author) const -> void
	{
		if ("" == author)
		{
			throw Command_Line_Parser_Builder_Error{"Author name must not be empty when given. "
			                                        "It is optional. "
			                                        "Just leave it out, if it is your intention not to "
			                                        "provide information on the author(s)."};
		}
		else if (find(m_authors.begin(), m_authors.end(), author) != m_authors.end())
		{
			throw Command_Line_Parser_Builder_Error{"Duplicate author " + single_quoted(author) + "."};
		}
	}

	auto Command_Line_Parser_Builder::ensure_valid_oss(const string &oss_used) const -> void
	{
		if ("" == oss_used)
		{
			throw Command_Line_Parser_Builder_Error{
					"Open source usage statement must not be empty when given. "
					"It is optional. "
					"Just leave it out, if it is your intention not to provide "
					"information on open source software used to build this software."};
		}
		else if (find(m_oss_used.begin(), m_oss_used.end(), oss_used) != m_oss_used.end())
		{
			throw Command_Line_Parser_Builder_Error{"Duplicate open source usage statement " +
			                                        single_quoted(oss_used) + "."};
		}
	}

	auto Command_Line_Parser_Builder::register_command(const string &name, Command_Parser &&cmd) -> void
	{
		ensure_command_not_registered_yet(name);
		m_commands.try_emplace(name, move(cmd));
	}

	auto Command_Line_Parser_Builder::register_anonymous_command(Command_Parser &&cmd) -> void
	{
		if (has_anonymous_command())
		{
			throw Command_Line_Parser_Builder_Error("There cannot be more than one anonymous command.");
		}
		m_anonymous_command.emplace(move(cmd));
	}

	auto Command_Line_Parser_Builder::ensure_command_not_registered_yet(const string &name) const -> void
	{
		if (m_commands.find(name) != m_commands.end())
		{
			throw Command_Line_Parser_Builder_Error("Command " + single_quoted(name) +
			                                        " already registered.");
		}
	}

	auto Command_Line_Parser_Builder::ensure_valid_description(const string &description) const -> void
	{
		if ("" == description)
		{
			throw Command_Line_Parser_Builder_Error{"Program description must not be empty."};
		}
		else if ("" == m_version)
		{
			throw Command_Line_Parser_Builder_Error{"Version information missing."};
		}
	}

} // namespace yacl
