/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/int_arg_parser_builder.hpp"
#include "yacl/parser/int_arg_parser.hpp"

namespace yacl
{
	using std::make_unique;

	Integer_Argument_Parser_Builder::Integer_Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder,
	                                                                 const string &name,
	                                                                 const param::Requiredness required,
	                                                                 const param::Identifiability identifiable_by)
			: Abstract_Copyable_Value_Argument_Parser_Builder{cmd_parser_builder, name, required,
	                                                                  identifiable_by}
	{
	}

	auto Integer_Argument_Parser_Builder::description(const string &description)
			-> Integer_Argument_Parser_Builder &
	{
		return static_cast<Integer_Argument_Parser_Builder &>(
				Abstract_Argument_Parser_Builder::description(description));
	}

	auto Integer_Argument_Parser_Builder::min(const int min_value) -> Integer_Argument_Parser_Builder &
	{
		m_min = min_value;
		return *this;
	}

	auto Integer_Argument_Parser_Builder::max(const int max_value) -> Integer_Argument_Parser_Builder &
	{
		m_max = max_value;
		return *this;
	}

	auto Integer_Argument_Parser_Builder::build_parser_with_requiredness_ensured()
			-> unique_ptr<Abstract_Argument_Parser>
	{
		return make_unique<Integer_Argument_Parser>(m_parameter_name, m_description, m_identifiable_by, m_min,
		                                            m_max, m_default_value);
	}

} // namespace yacl
