/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/parser/param_requiredness.hpp"
#include <junkbox/assertion.hpp>
#include <junkbox/convenient_algorithms.hpp>
#include <junkbox/text.hpp>

namespace yacl
{

	using junkbox::text::contains_whitespace;
	using junkbox::text::single_quoted;

	Command_Parser_Builder_Default_Impl::Command_Parser_Builder_Default_Impl(
			const Cmd_Parser_Registration_Fun &register_cmd_parser_fun)
			: m_register_command_parser{register_cmd_parser_fun}
	{
	}

	Command_Parser_Builder_Default_Impl::Command_Parser_Builder_Default_Impl(
			const Cmd_Parser_Registration_Fun &register_cmd_parser_fun, const string &cmd_name)
			: m_register_command_parser{register_cmd_parser_fun}, m_name{cmd_name}
	{
		if ("" == cmd_name)
		{
			throw Command_Parser_Builder_Error{"Command name must not be empty."};
		}
		else if (contains_whitespace(cmd_name))
		{
			throw Command_Parser_Builder_Error{"Command name " + single_quoted(cmd_name) +
			                                   " must not contain white space."};
		}
	}

	auto Command_Parser_Builder_Default_Impl::name() const noexcept -> const string &
	{
		return m_name;
	}

	auto Command_Parser_Builder_Default_Impl::is_anonymous_command() const noexcept -> bool
	{
		return m_name == "";
	}

	auto Command_Parser_Builder_Default_Impl::description(const string &description) -> Command_Parser_Builder &
	{
		ensure_valid_description(description);
		m_description = description;
		return *this;
	}

	auto Command_Parser_Builder_Default_Impl::optional() -> Argument_Parser_Builder
	{
		return Argument_Parser_Builder{*this, param::Requiredness::OPTIONAL};
	}

	auto Command_Parser_Builder_Default_Impl::mandatory() -> Argument_Parser_Builder
	{
		return Argument_Parser_Builder{*this, param::Requiredness::MANDATORY};
	}

	auto Command_Parser_Builder_Default_Impl::action(const Command_Action &action) -> void
	{
		if (!m_description.empty())
		{
			if (is_anonymous_command())
			{
				m_register_command_parser("", Command_Parser{m_description, action,
				                                             move(m_named_parameters),
				                                             move(m_positional_parameters)});
			}
			else
			{
				m_register_command_parser(m_name, Command_Parser{m_name, m_description, action,
				                                                 move(m_named_parameters),
				                                                 move(m_positional_parameters)});
			}
		}
		else
		{
			throw Command_Parser_Builder_Error{"Description missing for " + descriptive_command_name() +
			                                   "."};
		}
	}

	auto Command_Parser_Builder_Default_Impl::register_named_parameter(unique_ptr<Abstract_Argument_Parser> param)
			-> void
	{
		DEV_ASSERT(!param->is_positional(), "Trying to register a positional parameter as a named parameter.");
		ensure_named_parameter_not_registered_yet(*param);
		m_named_parameters.try_emplace(param->name(), move(param));
	}

	auto
	Command_Parser_Builder_Default_Impl::register_positional_parameter(unique_ptr<Abstract_Argument_Parser> param)
			-> void
	{
		DEV_ASSERT(param->is_positional(), "Trying to register a named parameter as a positional parameter.");
		ensure_positional_parameter_not_registered_yet(*param);
		ensure_no_optional_positional_parameters_yet(*param);
		m_positional_parameters.emplace_back(move(param));
	}

	auto Command_Parser_Builder_Default_Impl::descriptive_command_name() const -> string
	{
		return is_anonymous_command() ? "anonymous command" : "command " + single_quoted(m_name);
	}

	auto Command_Parser_Builder_Default_Impl::ensure_valid_description(const string &description) const -> void
	{
		if ("" == description)
		{
			throw Command_Parser_Builder_Error{"Description of " + descriptive_command_name() +
			                                   " must not be empty."};
		}
		else if ("" != m_description)
		{
			throw Command_Parser_Builder_Error{"Description of " + descriptive_command_name() +
			                                   " already set. Trying to override " +
			                                   single_quoted(m_description) + " with " +
			                                   single_quoted(description) + "."};
		}
	}

	auto Command_Parser_Builder_Default_Impl::ensure_named_parameter_not_registered_yet(
			const Abstract_Argument_Parser &param) const -> void
	{
		// Should use contains here. However, not yet supported by compiler.
		if (m_named_parameters.find(param.name()) != m_named_parameters.end())
		{
			throw Command_Parser_Builder_Error{"Parameter with name " + single_quoted(param.name()) +
			                                   " already defined for " + descriptive_command_name() + "."};
		}
	}

	auto Command_Parser_Builder_Default_Impl::ensure_positional_parameter_not_registered_yet(
			const Abstract_Argument_Parser &param) const -> void
	{
		using junkbox::algo::for_each;
		for_each(m_positional_parameters,
		         [&](const auto &other)
		         {
				 if (param.name() == other->name())
				 {
					 throw Command_Parser_Builder_Error{"Positional parameter with name " +
				                                            single_quoted(param.name()) +
				                                            " already defined for " +
				                                            descriptive_command_name() + "."};
				 }
			 });
	}

	auto Command_Parser_Builder_Default_Impl::ensure_no_optional_positional_parameters_yet(
			const Abstract_Argument_Parser &new_param) const -> void
	{
		using junkbox::algo::for_each;
		for_each(m_positional_parameters,
		         [&](const auto &old_param)
		         {
				 if (!old_param->is_mandatory())
				 {
					 throw Command_Parser_Builder_Error{
							 "There is already a positional, optional parameter " +
							 single_quoted(old_param->name()) + " for " +
							 descriptive_command_name() +
							 ". Therefore you cannot define positional, mandatory "
							 "parameter " +
							 single_quoted(new_param.name() +
				                                       ". However, you may add furter positional, "
				                                       "optional parameters though.")};
				 }
			 });
	}

} // namespace yacl
