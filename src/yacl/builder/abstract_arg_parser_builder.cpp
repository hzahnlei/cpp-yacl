/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/abstract_arg_parser_builder.hpp"
#include "yacl/builder/arg_parser_builder.hpp"
#include "yacl/builder/cmd_parser_builder_defimpl.hpp"
#include <junkbox/text.hpp>

namespace yacl
{

	using namespace junkbox;

	Abstract_Argument_Parser_Builder::Abstract_Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder,
	                                                                   const string &param_name,
	                                                                   const param::Requiredness required,
	                                                                   const param::Identifiability identifiable_by)
			: m_command_parser_builder{cmd_parser_builder}, m_parameter_name{param_name},
			  m_requiredness{required}, m_identifiable_by{identifiable_by}
	{
		if (param_name.empty())
		{
			throw Argument_Parser_Builder_Error{"Parameter name for command " +
			                                    text::single_quoted(m_command_parser_builder.name()) +
			                                    " must not be empty."};
		}
	}

	auto Abstract_Argument_Parser_Builder::description(const string &description)
			-> Abstract_Argument_Parser_Builder &
	{
		if (!description.empty())
		{
			m_description = description;
			return *this;
		}
		else
		{
			throw Argument_Parser_Builder_Error{"Description of parameter " +
			                                    text::single_quoted(m_parameter_name) + " of command " +
			                                    text::single_quoted(m_command_parser_builder.name()) +
			                                    " must not be empty."};
		}
	}

	auto Abstract_Argument_Parser_Builder::optional() -> Argument_Parser_Builder
	{
		return build().optional();
	}

	auto Abstract_Argument_Parser_Builder::mandatory() -> Argument_Parser_Builder
	{
		return build().mandatory();
	}

	auto Abstract_Argument_Parser_Builder::action(const Command_Action &action) -> void
	{
		build().action(action);
	}

	auto Abstract_Argument_Parser_Builder::build() -> Command_Parser_Builder &
	{
		if (is_positional())
		{
			m_command_parser_builder.register_positional_parameter(build_parser());
		}
		else
		{
			m_command_parser_builder.register_named_parameter(build_parser());
		}
		return m_command_parser_builder;
	}

	auto Abstract_Argument_Parser_Builder::is_positional() const noexcept -> bool
	{
		return m_identifiable_by == param::Identifiability::BY_POSITION;
	}

	auto Abstract_Argument_Parser_Builder::is_supposed_to_be_mandatory() const noexcept -> bool
	{
		return param::Requiredness::MANDATORY == m_requiredness;
	}

	auto Abstract_Argument_Parser_Builder::build_parser() -> unique_ptr<Abstract_Argument_Parser>
	{
		ensure_requiredness();
		return build_parser_with_requiredness_ensured();
	}

	auto Abstract_Argument_Parser_Builder::ensure_requiredness() const -> void
	{
		if (is_supposed_to_be_mandatory() && has_default_value())
		{
			throw Argument_Parser_Builder_Error{"Mandatory parameter " +
			                                    text::single_quoted(m_parameter_name) + " of command " +
			                                    text::single_quoted(m_command_parser_builder.name()) +
			                                    " must not have a default value."};
		}
		else if (!is_supposed_to_be_mandatory() && !has_default_value())
		{
			throw Argument_Parser_Builder_Error{"Optional parameter " +
			                                    text::single_quoted(m_parameter_name) + " of command " +
			                                    text::single_quoted(m_command_parser_builder.name()) +
			                                    " is missing a default value."};
		}
		else if (m_description.empty())
		{
			throw Argument_Parser_Builder_Error{"Description for parameter " +
			                                    text::single_quoted(m_parameter_name) + " of command " +
			                                    text::single_quoted(m_command_parser_builder.name()) +
			                                    " missing."};
		}
	}

} // namespace yacl
