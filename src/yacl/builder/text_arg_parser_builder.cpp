/*
 * C++ YACL - Yet another C++ library for handling command line arguments.
 *
 * (c) 2020, 2022, 2023 Holger Zahnleiter, All rights reserved
 */

#include "yacl/builder/text_arg_parser_builder.hpp"
#include "yacl/parser/text_arg_parser.hpp"
#include <junkbox/convenient_algorithms.hpp>

namespace yacl
{

	using junkbox::algo::contains;
	using junkbox::text::contains_whitespace;
	using junkbox::text::single_quoted;
	using std::make_unique;

	Text_Argument_Parser_Builder::Text_Argument_Parser_Builder(Command_Parser_Builder &cmd_parser_builder,
	                                                           const string &name,
	                                                           const param::Requiredness required,
	                                                           const param::Identifiability identifiable_by)
			: Abstract_Reference_Value_Argument_Parser_Builder{cmd_parser_builder, name, required,
	                                                                   identifiable_by}
	{
	}

	auto Text_Argument_Parser_Builder::description(const string &description) -> Text_Argument_Parser_Builder &
	{
		return static_cast<Text_Argument_Parser_Builder &>(
				Abstract_Argument_Parser_Builder::description(description));
	}

	auto Text_Argument_Parser_Builder::allow_empty_text() -> Text_Argument_Parser_Builder &
	{
		if (!m_is_empty_text_allowed)
		{
			m_is_empty_text_allowed = true;
			return *this;
		}
		else
		{
			throw Argument_Parser_Builder_Error{"Trying to override allow-empty property of parameter " +
			                                    single_quoted(m_parameter_name) + "."};
		}
	}

	auto Text_Argument_Parser_Builder::allow_white_space() -> Text_Argument_Parser_Builder &
	{
		if (!m_is_white_space_allowed)
		{
			m_is_white_space_allowed = true;
			return *this;
		}
		else
		{
			throw Argument_Parser_Builder_Error{
					"Trying to override allow-white-space property of parameter " +
					single_quoted(m_parameter_name) + "."};
		}
	}

	auto Text_Argument_Parser_Builder::build_parser_with_requiredness_ensured()
			-> unique_ptr<Abstract_Argument_Parser>
	{
		return make_unique<Text_Argument_Parser>(m_parameter_name, m_description, m_identifiable_by,
		                                         m_is_empty_text_allowed, m_is_white_space_allowed,
		                                         move(m_allowed_values), m_default_value);
	}

	auto Text_Argument_Parser_Builder::enforce_constraints_on_choice(const string &choice) const -> void
	{
		if (!m_is_empty_text_allowed && choice.empty())
		{
			throw Argument_Parser_Builder_Error{
					"Choice for parameter " + single_quoted(m_parameter_name) + " of command " +
					single_quoted(m_command_parser_builder.name()) + " is empty."};
		}
		else if (!m_is_white_space_allowed && contains_whitespace(choice))
		{
			throw Argument_Parser_Builder_Error{"Choice " + single_quoted(choice) + " for parameter " +
			                                    single_quoted(m_parameter_name) + " of command " +
			                                    single_quoted(m_command_parser_builder.name()) +
			                                    " contains whitespace."};
		}
		else if (contains(m_allowed_values, choice))
		{
			throw Argument_Parser_Builder_Error{"Duplicate choice " + single_quoted(choice) +
			                                    " for parameter " + single_quoted(m_parameter_name) +
			                                    " of command " +
			                                    single_quoted(m_command_parser_builder.name()) + "."};
		}
	}

} // namespace yacl
